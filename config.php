<?php
return array(
	'DB_TYPE' => 'mysql',
	'DB_HOST' => 'localhost',
	'DB_NAME' => 'ishwnet_hsnc',
	'DB_USER' => 'ishwnet_hsnc',
	'DB_PWD' => 'microsoft',
	'DB_PORT' => '3306',
	'DB_PREFIX' => 'hs_',
	
	'URL_MODEL' => 2,
	
	'TMPL_PARSE_STRING' => array(
		'__ROOT__' => SAKURA_URI,
		'__PUBLIC__' => SAKURA_URI.'/Public',
	),
);