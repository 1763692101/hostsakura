<?php
class pay_hostker {
	public $key;
	public $iv;
	/**
	 * 构造函数
	 * @param string $key
	 * @param string $iv
	 */
	public function __construct($key,$iv){
		$this->key = $key;
		$this->iv = $iv;
	}
	/**
	 * 支付网址构造
	 * @param string $returnurl
	 * @param string $orderid
	 * @param string $rmb
	 * @return string
	 */
	public function urlGet($returnurl,$orderid,$rmb){
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		$resellerid = $this->iv;
		$userip = $_SERVER["REMOTE_ADDR"];
		$key = $this->key;
		$sign = md5($userip.$key);
		$url = urlencode($returnurl);
		$url = 'http://i.hostker.com/rapi/pay?resellerid='.$resellerid.'&orderid='.$orderid.'&rmb='.$rmb.'&noticeurl='.$url.'&sign='.$sign;
		return $url;
	}
	/**
	 * 订单状态检测
	 * @param string $invoice
	 * @param string $orderid
	 * @return array
	 */
	public function statusCheck($invoice,$orderid){
		$resellerid = $this->iv;
		$status = file_get_contents('http://i.hostker.com/rapi/checkOrder?invoice='.$invoice.'&orderid='.$orderid.'&resellerid='.$resellerid);
		if($status == '1') return array('status'=>true,'res'=>'1');
		else return array('status'=>false,'res'=>'0');
	}
}