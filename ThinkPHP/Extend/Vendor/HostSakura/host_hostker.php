<?php
/**
 * HostKer主机操作类
 * @author kiddel
 * @date 2013-07-01
 * @time 20:52
 */
class host_hostker extends host_apibase {
	public $key;
	public $iv;
	public $des;
	const URL='http://i.hostker.com/rapi/doAction';
	/**
	 * 构造函数
	 * @param string $key
	 * @param string $iv
	 * @param object $des
	 */
	public function __construct($key,$iv,$des){
		$this->key = $key;
		$this->iv = $iv;
		$this->des = $des;
	}
	/**
	 * 返回代码处理
	 * @param string $type
	 * @param string/int $response
	 * @return array
	 */
	public function msgProcess($type,$response){
		if($type == 'addHost'){
			switch($response){
				case '0':return array('status'=>false,'res'=>$response,'text'=>'开通失败，请立即与技术联系');
				case '1':return array('status'=>false,'res'=>$response,'text'=>'用户名不符合规定');
				case '2':return array('status'=>false,'res'=>$response,'text'=>'密码不科学');
				case '3':return array('status'=>false,'res'=>$response,'text'=>'用户名存在');
				case '4':return array('status'=>false,'res'=>$response,'text'=>'型号不存在');
				case '5':return array('status'=>false,'res'=>$response,'text'=>'付款周期不正确');
				case '6':return array('status'=>false,'res'=>$response,'text'=>'代理商账号余额不足');
				case '50':return array('status'=>false,'res'=>$response,'text'=>'sign签名验证失败');
				case '51':return array('status'=>false,'res'=>$response,'text'=>'代理商不存在');
				default:return array('status'=>true,'res'=>$response,'text'=>'开通成功');
			}
		}elseif($type == 'renewHost'){
			switch($response){
				case '0':return array('status'=>false,'res'=>$response,'text'=>'开通失败，请立即与技术联系');
				case '1':return array('status'=>false,'res'=>$response,'text'=>'用户名不符合规定');
				case '5':return array('status'=>false,'res'=>$response,'text'=>'付款周期不正确');
				case '6':return array('status'=>false,'res'=>$response,'text'=>'代理商账号余额不足');
				case '50':return array('status'=>false,'res'=>$response,'text'=>'sign签名验证失败');
				case '51':return array('status'=>false,'res'=>$response,'text'=>'代理商不存在');
				default:return array('status'=>true,'res'=>$response,'text'=>'续费成功');
			}
		}elseif($type == 'changePw'){
			switch($response){
				case '0':return array('status'=>true,'res'=>$response,'text'=>'修改成功');
				case '1':return array('status'=>false,'res'=>$response,'text'=>'修改失败');
				case '50':return array('status'=>false,'res'=>$response,'text'=>'sign签名验证失败');
				case '51':return array('status'=>false,'res'=>$response,'text'=>'代理商不存在');
			}
		}elseif($type == 'upgradeHost'){
			switch($response){
				case '0':return array('status'=>true,'res'=>$response,'text'=>'升级成功');
				case '1':return array('status'=>false,'res'=>$response,'text'=>'用户名不存在');
				case '2':return array('status'=>false,'res'=>$response,'text'=>'只能升级不能降级');
				case '3':return array('status'=>false,'res'=>$response,'text'=>'代理商账号余额不足');
			}
		}else{
			return false;
		}
	}
	/**
	 * 主机开通方法
	 * @param string $username
	 * @param string $password
	 * @param array $pdata
	 * @return array
	*/
	public function addHost($username,$password,$pdata){
		$data['panelusername'] = $username;
		$data['panelpassword'] = $password;
		$data['action'] = 'createHost';
		$data['time'] = $pdata['time'];
		$data['type'] = $pdata['type'];
		ksort($data);
		$json = json_encode($data);
		$postdata = $this->des->encrypt($json);
		$post['resellerid'] = $this->iv;
		$post['data'] = $postdata;
		$post['sign'] = md5($json.$this->key);
		$response = $this->curlPost(self::URL,$post);
		$result = $this->msgProcess('addHost',$response);
		return $result;
	}
	/**
	 * 主机续费方法
	 * @param string $username
	 * @param string $time
	 * @return array
	*/
	public function renewHost($username,$time){
		$data['panelusername'] = $username;
		$data['action'] = 'renewHost';
		$data['time'] = $time;
		ksort($data);
		$json=json_encode($data);
		$postdata=$this->des->encrypt($json);
		$post['resellerid'] = $this->iv;
		$post['data'] = $postdata;
		$post['sign'] = md5($json.$this->key);
		$response = $this->curlPost(self::URL,$post);
		$result = $this->msgProcess('renewHost',$response);
		return $result;
	}
	/**
	 * 主机密码修改方法
	 * @param string $username
	 * @param string $password
	 * @return array
	*/
	public function changePw($username,$password){
		$data['action'] = 'changePw';
		$data['panelusername'] = $username;
		$data['panelpassword'] = $password;
		ksort($data);
		$json=json_encode($data);
		$postdata=$this->des->encrypt($json);
		$post['resellerid']=$this->iv;
		$post['data']=$postdata;
		$post['sign']=md5($json.$this->key);
		$response = $this->curlPost(self::URL,$post);
		$result = $this->msgProcess('changePw',$response);
		return $result;
	}
	/**
	 * 主机升级差价计算
	 * @param int $oldfee
	 * @param int $newfee
	 * @param string $duedate
	 * @return int
	 */
	public function priceCount($oldfee,$newfee,$duedate){
		$fee = $newfee-$oldfee;
		$today = date('Ymd',time());
		if($today == $duedate){
			$days=1;
		}else{
			$unixtime=strtotime($duedate);
			$unixtime=$unixtime-time();
			$days=$unixtime / 86400;
		}
		$fee = $fee/10; 
		$fee = $fee*$days;
		$fee = $fee/30;
		$fee = round($fee,2);
		return $fee;
	}
	/**
	 * 主机升级
	 * @param string $username
	 * @param string $new
	 * @return array
	 */
	public function upgradeHost($username,$new){
		$data['panelusername'] = $username;
		$data['action'] = 'upgradeHost';
		$data['type'] = $new;
		ksort($data);
		$json = json_encode($data);
		$postdata = $this->des->encrypt($json);
		$post['resellerid']=$this->iv;
		$post['data']=$postdata;
		$post['sign']=md5($json.$this->key);
		$response = $this->curlPost(self::URL,$post);
		$result = $this->msgProcess('upgradeHost',$response);
		return $result;
	}
	/**
	 * 主机删除方法
	 * @param string $username
	 * @return array
	*/
	public function delHost($username){
		return array('status'=>false,'res'=>'0','text'=>'暂时不支持删除');
	}
	/**
	 * 跳转到控制面板
	 * @param string $username
	 * @param string $email
	 */
	public function toPanel($username,$email){
		$panelusername = $username;
		$etime = time()+1800;
		$resellerid = $this->iv;
		$key = $this->key;
		$sign=md5($panelusername.$etime.$email.$key);
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header('Location: http://panel.idc.cx/cp/doLogin?resellerid='.$resellerid.'&etime='.$etime.'&panelusername='.$panelusername.'&email='.urlencode($email).'&sign='.$sign);
	}
}