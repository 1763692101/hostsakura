<?php
/**
 * 主机相关功能API基类
 * @author kiddel
 *
 */
abstract class host_apibase {
	/**
	 * curl数据提交方法
	 * @param string $url
	 * @param array $post
	 * @return string
	 */
	public function curlPost($url,$post=array()){
    	$ch=curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_HEADER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	$result=curl_exec($ch);
    	curl_close($ch);
    	return $result;
    }
    /**
     * 返回代码处理
     * @param string $type
     * @param string/int $response
     * @return array
     */
    abstract function msgProcess($type,$response);
    /**
     * 主机开通方法
     * @param string $username
     * @param string $password
     * @param array $data
     * @return array
     */
	abstract function addHost($username,$password,$data);
	/**
	 * 主机续费方法
	 * @param string $username
	 * @param string $time
	 * @return array
	 */
	abstract function renewHost($username,$time);
	/**
	 * 主机密码修改方法
	 * @param string $username
	 * @param string $password
	 * @return array
	 */
	abstract function changePw($username,$password);
	/**
	 * 主机升级
	 * @param string $username
	 * @param string $new
	 */
	abstract function upgradeHost($username,$new);
	/**
	 * 主机删除方法
	 * @param string $username
	 * @return array
	 */
	abstract function delHost($username);
}