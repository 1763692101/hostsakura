<?php
class HostkerAction extends SakuraAction {
	public $obj;
	public $des;
	public function __construct(){
		parent::__construct();
		$this->loadSetting();
		$this->apiInit();
	}
	public function index(){
		$this->showError('未定义操作');
	}
	public function loadSetting(){
		$data = M('Hostker_setting')->select();
		$settings = array();
		foreach($data as $val)
			$settings[$val['key']] = $val['value'];
		C('SAKURA_HOSTKER_SETTING',$settings);
	}
	public function apiInit(){
		$this->loadHCC('hostker');
		$settings = C('SAKURA_HOSTKER_SETTING');
		$this->des = new Des($settings['apfkey'],$settings['apfid']);
		$this->obj = new host_hostker($settings['apfkey'],$settings['apfid'],$this->des);
	}
	public function buy(){
		A('Member')->checkLogin();
		$pid = I('get.pid');
		if(!$pid) $this->showError('参数不完整');
		$dbData = M('Hostker_scheme')->where(array('id'=>$pid))->select();
		if(!$dbData) $this->showError('未找到此计划');
		$this->assign('plan',$dbData['0']);
		$this->display();
	}
	public function dobuy(){
		A('Member')->checkLogin();
		$pwd = md5(md5(I('post.password')));
		if($pwd != C('SAKURA_MEMBER.password')) $this->showError('密码校验失败');
		$pid = I('post.pid');
		$time = I('post.time');
		$username = I('post.panelusername');
		$password = I('post.panelpassword');
		if(!$username || !$password) $this->showError('参数不完整');
		$dbData = M('Hostker_scheme')->where(array('id'=>$pid))->select();
		if(!$dbData) $this->showError('未知的型号');
		$dbData = $dbData['0'];
		$fee = ($time=='month') ? ($dbData['fee']/10) : $dbData['fee'];
		if($fee > C('SAKURA_MEMBER.money')) $this->showError('您的余额不足');
		$newmoney = C('SAKURA_MEMBER.money') - $fee;
		$res = $this->obj->addHost($username,$password,array('time'=>$time,'type'=>$pid));
		if($res['status']){
			M('User')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->save(array('money'=>$newmoney));
			$data = array();
			$data['pid'] = $pid;
			$data['uid'] = C('SAKURA_MEMBER.uid');
			$data['panelusername'] = $username;
			$data['exptime'] = $res['res'];
			M('Cloudhost')->add($data);
			$this->showSuccess($res['text'],U('Member/host_mycloud'));
		}else $this->showError('['.$res['res'].']'.$res['text']);
	}
	public function panel(){
		A('Member')->checkLogin();
		$hid = I('get.hid');
		$dbData = M('Cloudhost')->where(array('id'=>$hid))->select();
		if(!$dbData) $this->showError('没有这个主机');
		$dbData = $dbData['0'];
		if($dbData['uid'] != C('SAKURA_MEMBER.uid')) $this->showError('你没有这台主机的使用权');
		$this->obj->toPanel($dbData['panelusername'],C('SAKURA_MEMBER.email'));
	}
	public function renew(){
		A('Member')->checkLogin();
		$hid = I('get.hid');
		if(!$hid) $this->showError('参数不完整');
		$dbData = M('Cloudhost')->where(array('id'=>$hid))->select();
		if(!$dbData) $this->showError('没有这个主机');
		$dbData = $dbData['0'];
		$planData = M('Hostker_scheme')->where(array('id'=>$dbData['pid']))->select();
		if(!$planData) $this->showError('找不到对应型号');
		$planData = $planData['0'];
		$this->assign('hostData',$dbData);
		$this->assign('planData',$planData);
		$this->display();
	}
	public function dorenew(){
		A('Member')->checkLogin();
	}
	public function update(){
		A('Member')->checkLogin();
	}
	
	
	public function getPlanInfo($id,$get='all'){
		$data = M('Hostker_scheme')->where(array('id'=>$id))->select();
		if(!$data) return false;
		$data = $data['0'];
		if($get=='all') return $data;
		else return $data[$get];
	}
}