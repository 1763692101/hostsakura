<?php
class ForumAction extends SakuraAction {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$where = array();
		$where['rid'] = array('eq','0');
		$count = M('Threads')->where($where)->count();
		$Page = new Page($count,10);
		$Page->url = 'Forum/index/p';
		$Page->setConfig('header','篇帖子');
		$Page->setConfig('prev','<i class="icon-chevron-left"></i>');
		$Page->setConfig('next','<i class="icon-chevron-right"></i>');
		$Page->setConfig('theme','%totalRow%&nbsp;%header%&nbsp;%nowPage%/%totalPage%&nbsp;页&nbsp;%upPage%&nbsp;%first%&nbsp;%prePage%&nbsp;%linkPage%&nbsp;%nextPage%&nbsp;%downPage%&nbsp;%end%');
		$page_nav = urldecode($Page->show());
		$dbData = M('Threads')->where($where)->order('timestamp desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('threads',$dbData);
		$this->assign('page_nav',$page_nav);
		$this->display();
	}
	public function newthread(){
		A('Member')->checkLogin();
		$this->display();
	}
	public function postnew(){
		A('Member')->checkLogin();
		$title = I('post.title');
		$context = I('post.context');
		if(!$title || !$context) $this->showError('参数不完整');
		if(strlen($title) < 3) $this->showError('标题长度不足');
		if(strlen($context) < 10) $this->showError('内容太少');
		$data = array();
		$data['rid'] = '0';
		$data['uid'] = C('SAKURA_MEMBER.uid');
		$data['title'] = $title;
		$data['context'] = $context;
		$data['timestamp'] = mktime();
		$query = M('Threads')->add($data);
		if($query) $this->showSuccess('帖子发表成功',U('Forum/view',array('tid'=>$query)));
		else $this->showError('帖子发表失败，请重试');
	}
	 function view(){
		$tid = I('get.tid');
		$where = array();
		$where['id'] = array('eq',$tid);
		$where['rid'] = array('eq','0');
		$threadData = M('Threads')->where($where)->select();
		if(!$threadData) $this->showError('没有此贴或者此贴已经被删除');
		$threadData = $threadData['0'];
		if($threadData['rid']!='0') $this->showError('这是一篇回复');
		$where = array();
		$where['rid'] = array('eq',$tid);
		$count = M('Threads')->where($where)->count();
		$Page = new Page($count,10);
		$Page->url = 'Forum/view/tid/'.$tid.'/p';
		$Page->setConfig('header','篇回复');
		$Page->setConfig('prev','<i class="icon-chevron-left"></i>');
		$Page->setConfig('next','<i class="icon-chevron-right"></i>');
		$Page->setConfig('theme','%totalRow%&nbsp;%header%&nbsp;%nowPage%/%totalPage%&nbsp;页&nbsp;%upPage%&nbsp;%first%&nbsp;%prePage%&nbsp;%linkPage%&nbsp;%nextPage%&nbsp;%downPage%&nbsp;%end%');
		$page_nav = urldecode($Page->show());
		$replyData = M('Threads')->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('threadData',$threadData);
		$this->assign('replyData',$replyData);
		$this->assign('page_nav',$page_nav);
		$this->display();
	}
	public function reply(){
		A('Member')->checkLogin();
		$tid = I('get.tid');
		$dbData = M('Threads')->where(array('id'=>$tid))->select();
		if(!$dbData) $this->showError('没有这篇帖子');
		$dbData = $dbData['0'];
		$this->assign('threadData',$dbData);
		$this->display();
	}
	public function doreply(){
		A('Member')->checkLogin();
		$tid = I('post.tid');
		$title = I('post.title');
		$context = I('post.context');
		if(!$tid || !$title || !$context) $this->showError('参数不完整');
		$dbData = M('Threads')->where(array('id'=>$tid))->select();
		if(!$dbData) $this->showError('没有这篇帖子');
		$dbData = $dbData['0'];
		$toid = $dbData['rid'] ? $dbData['rid'] : $dbData['id'];
		$data = array();
		$data['rid'] = $tid;
		$data['uid'] = C('SAKURA_MEMBER.uid');
		$data['title'] = $title;
		$data['context'] = $context;
		$data['timestamp'] = mktime();
		$query = M('Threads')->add($data);
		if($query) $this->showSuccess('回复成功',U('Forum/view',array('tid'=>$toid)));
		else $this->showError('回复失败，请重试');
	}
	public function del(){
		A('Member')->checkLogin();
		if(C('SAKURA_MEMBER.admin') != '1') $this->showError('权限验证失败');
		$tid = I('get.tid');
		$dbData = M('Threads')->where(array('id'=>$tid))->select();
		if(!$dbData) $this->showError('没有这篇帖子');
		$dbData = $dbData['0'];
		if($dbData['rid']!='0') $tourl = U('Forum/view',array('tid'=>$dbData['rid']));
		else $tourl = U('Forum/index');
		M('Threads')->where(array('id'=>$tid))->delete();
		M('Threads')->where(array('rid'=>$tid))->delete();
		$this->showError('帖子删除成功',$tourl);
	}
	public function edit(){
		A('Member')->checkLogin();
		$tid = I('get.tid');
		$dbData = M('Threads')->where(array('id'=>$tid))->select();
		if(!$dbData) $this->showError('没有这篇帖子');
		$dbData = $dbData['0'];
		if(C('SAKURA_MEMBER.admin') != '1' && C('SAKURA_MEMBER.uid') != $dbData['uid'])
			$this->showError('权限验证失败');
		$this->assign('threadData',$dbData);
		$this->display();
	}
	public function doedit(){
		A('Member')->checkLogin();
		$tid = I('post.tid');
		$title = I('post.title');
		$context = I('post.context');
		$dbData = M('Threads')->where(array('id'=>$tid))->select();
		if(!$dbData) $this->showError('没有这篇帖子');
		$dbData = $dbData['0'];
		if(C('SAKURA_MEMBER.admin') != '1' && C('SAKURA_MEMBER.uid') != $dbData['uid'])
			$this->showError('权限验证失败');
		$data = array();
		$data['title'] = $title;
		$data['context'] = $context;
		$query = M('Threads')->where(array('id'=>$tid))->save($data);
		if($dbData['rid']!='0') $tourl = U('Forum/view',array('tid'=>$dbData['rid']));
		else $tourl = U('Forum/view',array('tid'=>$tid));
		if($query) $this->showSuccess('帖子编辑成功',$tourl);
		else $this->showError('帖子编辑失败，请重试');
	}
	
	public function getReplyNum($tid){
		$count = M('Threads')->where(array('rid'=>$tid))->count();
		return $count;
	}
	public function getReply($tid){
		return M('Threads')->where(array('rid'=>$tid))->select();
	}
	public function getLastReply($tid){
		$dbData = M('Threads')->where(array('rid'=>$tid))->order('timestamp desc')->select();
		if(!$dbData) return false;
		$dbData = $dbData['0'];
		return $dbData;
	}
}