<?php
class IndexAction extends SakuraAction {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		$this->to(U('Member/index'));
	}
}