<?php
class SakuraAction extends Action {
	public function __construct(){
		parent::__construct();
		$this->Initialize();
		if(C('SAKURA_SETTING.open')=='0')
			$this->showError('网站维护中......');
	}
	public function Initialize(){
		$this->InitSetting();
		$this->InitMember();
		$this->InitHCBC();
	}
	public function InitSetting(){
		$dbData = M('Setting')->select();
		$settings = array();
		foreach($dbData as $val)
			$settings[$val['key']] = $val['value'];
		C('SAKURA_SETTING',$settings);
	}
	public function InitMember(){
		C('SAKURA_MEMBER',$this->verifyMember());
	}
	public function verifyMember(){
		$sessionData = unserialize(session('SAKURA_MEMBER'));
		if(!$sessionData) return false;
		$dbData = M('User')->where(array('uid'=>$sessionData['uid']))->select();
		if(!$dbData) return false;
		$dbData = $dbData['0'];
		$dbData['lastip'] = $sessionData['lastip'];
		$dbData['lasttime'] = $sessionData['lasttime'];
		if($sessionData['email'] != $dbData['email'] || $sessionData['password'] != $dbData['password'])
			return false;
		else
			return $dbData;
	}
	public function InitHCBC(){
		vendor('HostSakura.host_apibase');
	}
	public function loadHCC($api){
		vendor('HostSakura.host_'.$api);
	}
	public function to($url){
		header("Location:".$url);
	}
	public function showSuccess($message,$url=''){
		$this->success($message,$url);
		exit;
	}
	public function showError($message,$url=''){
		$this->error($message,$url);
		exit;
	}
}