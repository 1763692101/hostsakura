<?php
class MemberAction extends SakuraAction {
	public function __construct(){
		parent::__construct();
	}
	public function checkLogin($return=false){
		$login = C('SAKURA_MEMBER');
		$login = $login ? true : false;
		if($return) return $login;
		else if(!$login) $this->to(U('Member/login'));
	}
	public function index(){
		$this->checkLogin();
		$count = array();
		$count['cloudhost'] = M('Chost')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->count();
		$count['order'] = M('Order')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->count();
		$notices = M('Notice')->order('timestamp desc')->select();
		$this->assign('notices',$notices);
		$this->assign('count',$count);
		$this->display();
	}
	public function login(){
		if(C('SAKURA_MEMBER')) $this->to(U('Member/index'));
		$this->display();
	}
	public function dologin(){
		if(C('SAKURA_MEMBER')) $this->to(U('Member/index'));
		$email = I('post.email');
		$password = md5(md5(I('post.password')));
		if(!$email || !$password) $this->showError('参数不完整');
		$dbData = M('User')->where(array('email'=>$email))->select();
		if(!$dbData) $this->showError('E-Mail输入错误');
		$dbData = $dbData['0'];
		if($dbData['password'] != $password) $this->showError('密码输入错误');
		session('SAKURA_MEMBER',serialize($dbData));
		$data = array();
		$data['lastip'] = $_SERVER["REMOTE_ADDR"];
		$data['lasttime'] = mktime();
		M('User')->where(array('email'=>$email))->save($data);
		$this->to(U('Member/index'));
	}
	public function register(){
		if(C('SAKURA_MEMBER')) $this->to(U('Member/index'));
		if(C('SAKURA_SETTING.open')=='3') $this->showError('网站暂时关闭注册');
		$this->display();
	}
	public function doregister(){
		if(C('SAKURA_MEMBER')) $this->to(U('Member/index'));
		if(C('SAKURA_SETTING.open')=='3') $this->showError('网站暂时关闭注册');
		$email = I('post.email');
		$pwd = I('post.password');
		$pwd2 = I('post.password2');
		$name = I('post.name');
		$phone = I('post.phone');
		$invite = I('post.invite');
		if(!$email || !$pwd || !$pwd2 || !$name || !$phone) $this->showError('参数不完整');
		if($pwd != $pwd2) $this->showError('两次密码输入不一致');
		if(!$invite && C('SAKURA_SETTING.open')=='2') $this->showError('您未填写邀请码');
		if($invite){
			$dbData = M('Invite')->where(array('code'=>$invite))->select();
			if(!$dbData) $this->showError('没有这个邀请码');
			$dbData = $dbData['0'];
			$now = mktime();
			if($dbData['exptime'] < $now) $this->showError('邀请码已过期');
			M('Invite')->where(array('code'=>$invite))->save(array('used'=>'1','usetime'=>mktime()));
		}
		$pwd = md5(md5($pwd));
		$data = array();
		$data['email'] = $email;
		$data['password'] = $pwd;
		$data['name'] = $name;
		$data['phone'] = $phone;
		$data['lastip'] = $_SERVER["REMOTE_ADDR"];
		$data['lasttime'] = mktime();
		$data['admin'] = '0';
		M('User')->add($data);
		$this->to(U('Member/login'));
	}
	public function logout(){
		$this->checkLogin();
		session('SAKURA_MEMBER',null);
		$this->to(U('Index/index'));
	}
	public function basic_info(){
		$this->checkLogin();
		$this->display();
	}
	public function post_basic_info(){
		$this->checkLogin();
		$pwd = md5(md5(I('post.password')));
		$name = I('post.name');
		$phone = I('post.phone');
		if($pwd != C('SAKURA_MEMBER.password')) $this->showError('密码校验失败');
		if(!$name || !$phone) $this->showError('参数不完整');
		$data = array();
		$data['name'] = $name;
		$data['phone'] = $phone;
		M('User')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->save($data);
		$this->showSuccess('基本资料修改成功',U('Member/basic_info'));
	}
	public function basic_pwd(){
		$this->checkLogin();
		$this->display();
	}
	public function post_basic_pwd(){
		$this->checkLogin();
		$pwd = md5(md5(I('post.password')));
		if($pwd != C('SAKURA_MEMBER.password')) $this->showError('密码校验失败');
		$pwd1 = I('post.password1');
		$pwd2 = I('post.password2');
		if(!$pwd1 || !$pwd2) $this->showError('参数不完整');
		$pwd1 = md5(md5($pwd1));
		$pwd2 = md5(md5($pwd2));
		if($pwd1 != $pwd2) $this->showError('两次密码输入不一致');
		$data = array();
		$data['password'] = $pwd1;
		M('User')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->save($data);
		session('SAKURA_MEMBER',null);
		$this->showSuccess('密码修改成功，请重新登录',U('Member/login'));
	}
	public function basic_email(){
		$this->checkLogin();
		$this->display();
	}
	public function post_basic_email(){
		$this->checkLogin();
		$pwd = md5(md5(I('post.password')));
		if($pwd != C('SAKURA_MEMBER.password')) $this->showError('密码校验失败');
		$email = I('post.email');
		if(!$email) $this->showError('参数不完整');
		$data = array();
		$data['email'] = $email;
		M('User')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->save($data);
		session('SAKURA_MEMBER',null);
		$this->showSuccess('邮箱修改成功，请重新登录',U('Member/login'));
	}
	public function host_mycloud(){
		$this->checkLogin();
		$dbData = M('Cloudhost')->where(array('uid'=>C('SAKURA_MEMBER.uid')))->select();
		$this->assign('myHosts',$dbData);
		$this->display();
	}
	public function host_my(){
		$this->checkLogin();
		$this->showError('暂未开放');
	}
	public function mail_list(){
		$this->checkLogin();
		$dbData = M('Mail')->where(array('receiver'=>C('SAKURA_MEMBER.uid')))->order('timestamp desc')->select();
		$this->assign('myMails',$dbData);
		$this->display();
	}
	public function mail_view(){
		$this->checkLogin();
		$mid = I('get.mid');
		if(!$mid) $this->showError('参数不完整');
		$dbData = M('Mail')->where(array('id'=>$mid))->select();
		if(!$dbData) $this->showError('没有这封邮件');
		M('Mail')->where(array('id'=>$mid))->save(array('status'=>'1'));
		$dbData = $dbData['0'];
		$this->assign('dbData',$dbData);
		$this->display();
	}
	public function mail_send(){
		$this->checkLogin();
		$this->display();
	}
	public function post_mail_send(){
		$this->checkLogin();
		$email = I('post.receiver');
		$title = I('post.title');
		$context = I('post.context');
		if(!$email || !$title || !$context) $this->showError('参数不完整');
		$userInfo = M('User')->where(array('email'=>$email))->select();
		if(!$userInfo) $this->showError('没有找到收件人');
		$receiver = $userInfo['0']['uid'];
		$data = array();
		$data['timestamp'] = mktime();
		$data['type'] = '1';
		$data['title'] = $title;
		$data['context'] = $context;
		$data['sender'] = C('SAKURA_MEMBER.uid');
		$data['receiver'] = $receiver;
		$data['status'] = '0';
		M('Mail')->add($data);
		$this->showSuccess('邮件发送成功',U('Member/mail_list'));
	}
	public function mail_reply(){
		$this->checkLogin();
		$mid = I('get.mid');
		if(!$mid) $this->showError('参数不完整');
		$dbData = M('Mail')->where(array('id'=>$mid))->select();
		if(!$dbData) $this->showError('没有这封邮件');
		$dbData = $dbData['0'];
		$this->assign('dbData',$dbData);
		$this->display();
	}
	public function post_mail_reply(){
		$this->checkLogin();
		$email = I('post.receiver');
		$title = I('post.title');
		$context = I('post.context');
		if(!$email || !$title || !$context) $this->showError('参数不完整');
		$userInfo = M('User')->where(array('email'=>$email))->select();
		if(!$userInfo) $this->showError('没有找到收件人');
		$receiver = $userInfo['0']['uid'];
		$data = array();
		$data['timestamp'] = mktime();
		$data['type'] = '1';
		$data['title'] = $title;
		$data['context'] = $context;
		$data['sender'] = C('SAKURA_MEMBER.uid');
		$data['receiver'] = $receiver;
		$data['status'] = '0';
		M('Mail')->add($data);
		$this->showSuccess('邮件回复成功',U('Member/mail_list'));
	}
	
	public function getInfoByUID($uid,$get='all'){
		$data = M('User')->where(array('uid'=>$uid))->select();
		if(!$data) return false;
		$data = $data['0'];
		if($get=='all') return $data;
		else return $data[$get];
	}
	public function getAvatar($email,$size='18'){
		$md5 = md5($email);
		$file = $md5.'_'.$size.'.png';
		$dir = THINK_PATH.'../Cache/'.$file;
		$url = 'https://www.gravatar.com/avatar/'.$md5.'?s='.$size;
		if(!file_exists($dir)){
			/*
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			$result=curl_exec($ch);
			curl_close($ch);
			file_put_contents($dir,$result);
			*/
		}
		//return C('SAKURA_SETTING.siteurl').'/Cache/'.$md5.'_'.$size.'.png';
		return $url;
	}
}