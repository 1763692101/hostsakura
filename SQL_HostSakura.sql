CREATE TABLE IF NOT EXISTS `hs_setting` (
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `hs_setting` (`key`, `value`) VALUES
('sitename', 'HostSakura'),
('siteurl', 'http://127.0.0.1'),
('keywords', 'HostSakura,HostKer,虚拟主机'),
('description', 'HostSakura是一款高效的虚拟主机控制系统。'),
('open', '2'),
('foot', '<script src="http://s25.cnzz.com/stat.php?id=5493115&web_id=5493115&show=pic" language="JavaScript"></script>');

CREATE TABLE IF NOT EXISTS `hs_user` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(64) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `money` varchar(16) NOT NULL,
  `lastip` varchar(32) NOT NULL,
  `lasttime` varchar(16) NOT NULL,
  `admin` varchar(8) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

INSERT INTO `hs_user` (`uid`, `email`, `password`, `name`, `phone`, `money`, `lastip`, `lasttime`, `admin`) VALUES
(1, 'kiddel@qq.com', '07aa90e4974e6e8915a4ba2c5eeb283f', '日向悠', '18660130298', '0', '0', '0', '1'),
(2, 'newcxs@qq.com', '07aa90e4974e6e8915a4ba2c5eeb283f', '孙洪武', '18660130298', '0', '0', '0', '1');

CREATE TABLE IF NOT EXISTS `hs_invite` (
  `code` varchar(32) NOT NULL,
  `uid` varchar(16) NOT NULL,
  `createtime` varchar(16) NOT NULL,
  `exptime` varchar(16) NOT NULL,
  `used` varchar(8) NOT NULL,
  `usetime` varchar(16) NOT NULL,
  UNIQUE KEY `key` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hs_cloudhost` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `panelusername` varchar(32) NOT NULL,
  `exptime` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hs_order` (
  `id` varchar(32) NOT NULL,
  `title` varchar(64) NOT NULL,
  `uid` varchar(16) NOT NULL,
  `money` varchar(32) NOT NULL,
  `create_time` varchar(16) NOT NULL,
  `pay_time` varchar(16) NOT NULL,
  `hkid` varchar(64) NOT NULL COMMENT 'HostKer专用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hs_da_server` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `ip` varchar(32) NOT NULL,
  `param` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hs_da_plan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` varchar(16) NOT NULL,
  `name` varchar(32) NOT NULL,
  `param` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hs_hostker_setting` (
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `hs_hostker_setting` (`key`, `value`) VALUES
('apfid', '90000003'),
('apfkey', '54044068');

CREATE TABLE IF NOT EXISTS `hs_hostker_scheme` (
  `id` int(3)  NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '计划名称',
  `quota` int(6) NOT NULL COMMENT '空间大小[M]',
  `mysql` int(5) NOT NULL COMMENT 'MySQL大小[M]',
  `hkbw` int(6) NOT NULL COMMENT '香港/亚洲流量[G]',
  `cnbw` int(6) NOT NULL COMMENT '大陆/欧美流量[G]',
  `cputime` int(8) NOT NULL COMMENT 'CPU时间[时]',
  `querytime` int(8) NOT NULL COMMENT '查询时间[时]',
  `fee` int(5) NOT NULL COMMENT '价格[元/年]',
  `pname` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `hs_hostker_scheme` (`id`, `name`, `quota`, `mysql`, `hkbw`, `cnbw`, `cputime`, `querytime`, `fee`, `pname`) VALUES
(1, 'Plan_A', 500, 50, 1, 10, 10, 1, 50, '1'),
(2, 'Plan_B', 1024, 100, 5, 30, 20, 3, 150, '2'),
(3, 'Plan_C', 3072, 300, 15, 90, 90, 9, 450, '3'),
(4, 'Plan_D', 5120, 500, 30, 200, 150, 15, 750, '4'),
(5, 'Plan_E', 10240, 1024, 60, 500, 350, 35, 1500, '5');

CREATE TABLE IF NOT EXISTS `hs_threads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rid` varchar(16) NOT NULL,
  `uid` varchar(16) NOT NULL,
  `title` varchar(64) NOT NULL,
  `context` text NOT NULL,
  `timestamp` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hs_notice` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` varchar(16) NOT NULL,
  `context` text NOT NULL,
  `type` varchar(32) NOT NULL COMMENT '蓝-alert-info,红-alert-error,黄-null,绿-alert-success',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `hs_mail` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `timestamp` varchar(16) NOT NULL,
  `type` varchar(8) NOT NULL COMMENT '1-普通邮件,2-管理员邮件,3-系统邮件',
  `title` varchar(64) NOT NULL,
  `context` text NOT NULL,
  `sender` varchar(16) NOT NULL,
  `receiver` varchar(16) NOT NULL,
  `status` varchar(8) NOT NULL COMMENT '0-未读,1-已读',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;