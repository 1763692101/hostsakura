<?php
function gravatar($email,$url,$size="32"){
	$avatar = 'http://0.gravatar.com/avatar/'.md5($email).'?s='.$size;
	$file = './avatar/'.md5($email).'_'.$size.'.jpg';
	$url = $url.'/avatar/'.md5($email).'_'.$size.'.jpg';
	$t = 1209600;
	if(!is_file($file) || (time() - filemtime($file)) > $t){
		@unlink($file);
		$cnt = file_get_contents($avatar);
		file_put_contents($file,$cnt);
	}
	return $url;
}