<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_forum").className="active";</script>
    <style>.pagenav a{margin:0 2px 0 2px;}</style>
    <style>
    .threadHeader{border:1px gray;border-style:none none dashed none;overflow:hidden;}
    .threadBody{margin:15px 20px 30px 20px;}
    .threadFooter{border:1px gray;border-style:dashed none none none;}
    </style>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum">社区</a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum/view/tid/{$threadData['id']}">{$threadData['title']}</a> <span class="divider">/</span></li>
        <li class="active">第 {:(I('get.p')?I('get.p'):'1')} 页</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <h4></h4>
          <div class="span12">
            <div class="threadata inline">
              <div class="pull-left span2 text-center">
                <img src="{:A('Member')->getAvatar(A('Member')->getInfoByUID($threadData['uid'],'email'),'128')}" width="128" height="128">
              </div>
              <div class="pull-right span9" style="margin-right:50px;">
                <div class="threadHeader inline">
                  <div class="pull-left"><strong>{$threadData['title']}</strong></div>
                  <div class="pull-right"><strong>楼主</strong></div>
                </div>
                <div class="threadBody">{$threadData['context']|nl2br}</div>
                <div class="threadFooter inline">
                  <div class="pull-left"><small>{$threadData['timestamp']|date="Y-m-d H:i",###}</small></div>
                  <div class="pull-right"><small>
                    <a href="__APP__/Forum/reply/tid/{$threadData['id']}">回复</a>
                    <php>if(C('SAKURA_MEMBER.admin') == '1' || C('SAKURA_MEMBER.uid') == $threadData['uid']){</php>
                      <a href="__APP__/Forum/edit/tid/{$threadData['id']}">编辑</a>
                    <php>}</php>
                    <php>if(C('SAKURA_MEMBER.admin') == '1'){</php>
                      <a href="__APP__/Forum/del/tid/{$threadData['id']}">删除</a>
                    <php>}</php>
                  </small></div>
                </div>
              </div>
            </div>
          </div>
          <h1>&nbsp;</h1>
          <php>$page = I('get.p') ? I('get.p') : '1';</php>
          <php>$p = $page - 1;</php>
          <php>if($replyData){</php>
            <php>$i = 1;</php>
            <foreach name="replyData" item="reply">
              <div class="span12">
                <div class="threadata inline">
                  <div class="pull-left span2 text-center">
                    <img src="{:A('Member')->getAvatar(A('Member')->getInfoByUID($reply['uid'],'email'),'128')}" width="128" height="128">
                  </div>
                  <div class="pull-right span9" style="margin-right:50px;">
                    <div class="threadHeader inline">
                      <div class="pull-left"><strong>{$reply['title']}</strong></div>
                      <div class="pull-right"><strong>{:$i+$p*10}#</strong></div>
                    </div>
                    <div class="threadBody">
                      {$reply['context']|nl2br}
                      <php>$pReply = A('Forum')->getReply($reply['id']);</php>
                      <php>if($pReply){</php>
                        <div class="well" style="padding:10px!important;margin-top:10px;margin-left:20px;">
                          <foreach name="pReply" item="pr">
                            <div class="inline" style="margin:3px 5px 5px 10px!important"><nobr>
                              <img src="{:A('Member')->getAvatar(A('Member')->getInfoByUID($pr['uid'],'email'),'24')}" width="24" height="24">
                              <small><strong>{:A('Member')->getInfoByUID($pr['uid'],'name')}: </strong>{$pr['context']}</small>
                              <div class="text-right"><small>{$pr['timestamp']|date="Y-m-d H:i",###}</small></div>
                            </nobr></div>
                            <hr style="margin:0!important">
                          </foreach>
                        </div>
                      <php>}</php>
                    </div>
                    <div class="threadFooter inline">
                      <div class="pull-left"><small>{$reply['timestamp']|date="Y-m-d H:i",###}</small></div>
                      <div class="pull-right"><small>
                          <a href="__APP__/Forum/reply/tid/{$reply['id']}">回复</a>
                        <php>if(C('SAKURA_MEMBER.admin') == '1' || C('SAKURA_MEMBER.uid') == $threadData['uid']){</php>
                          <a href="__APP__/Forum/edit/tid/{$reply['id']}">编辑</a>
                        <php>}</php>
                        <php>if(C('SAKURA_MEMBER.admin') == '1'){</php>
                          <a href="__APP__/Forum/del/tid/{$reply['id']}">删除</a>
                        <php>}</php>
                      </small></div>
                    </div>
                  </div>
                </div>
              </div>
              <h1>&nbsp;</h1>
              <php>$i++;</php>
            </foreach>
          <php>}else{</php>
            <p>无回复</p>
          <php>}</php>
          <div class="text-right pagenav"><small>{$page_nav}</small></div>
          <h1>&nbsp;</h1>
          <hr>
          <h4>发表回复</h4>
          <php>if(C('SAKURA_MEMBER')){</php>
            <form class="form-horizontal" action="__APP__/Forum/doreply" method="post">
              <input type="hidden" name="tid" value="{$threadData['id']}">
              <div class="control-group">
                <label class="control-label" for="inputTitle">标题</label>
                <div class="controls">
                  <input class="span8" type="text" id="inputTitle" name="title" value="Re:{$threadData['title']}">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputContext">内容</label>
                <div class="controls">
                  <textarea class="span8" rows="10" id="inputContext" name="context" placeholder="请输入回复内容"></textarea>
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn btn-primary btn-large">回复</button>
                </div>
              </div>
            </form>
          <php>}else{</php>
            <p class="lead">您还没有登录，不能发表回复</p>
          <php>}</php>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />