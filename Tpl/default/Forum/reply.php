<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_forum").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum">社区</a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum/view/tid/{:($threadData['rid']?$threadData['rid']:$threadData['id'])}">{$threadData['title']}</a> <span class="divider">/</span></li>
        <li class="active">回复帖子</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <div class="inline" style="padding-bottom:25px;">
            <div class="pull-left"><h4>回复帖子：{$threadData['title']}</h4></div>
            <div class="pull-right"><h4>#</h4></div>
          </div>
          <hr>
          <form class="form-horizontal" action="__APP__/Forum/doreply" method="post">
            <input type="hidden" name="tid" value="{$threadData['id']}">
            <div class="control-group">
              <label class="control-label" for="inputTitle">帖子标题</label>
              <div class="controls">
                <input class="span8" type="text" id="inputTitle" name="title" value="Re:{$threadData['title']}">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputContext">帖子内容</label>
              <div class="controls">
                <textarea class="span8" rows="10" id="inputContext" name="context" placeholder="请输入帖子内容"></textarea>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn btn-primary btn-large">回复</button>
              </div>
            </div>
          </form>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />