<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_forum").className="active";</script>
    <style>.pagenav a{margin:0 2px 0 2px;}</style>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum">社区</a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum">帖子列表</a> <span class="divider">/</span></li>
        <li class="active">第 {:(I('get.p')?I('get.p'):'1')} 页</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <div class="inline" style="padding-bottom:25px;">
            <div class="pull-left"><h4>帖子列表</h4></div>
            <div class="pull-right">
              <a class="btn btn-primary" href="__APP__/Forum/newthread">发表新帖</a>
            </div>
          </div>
          <hr>
          <table class="table table-hover table-condensed">
            <thead>
              <tr>
                <th width="80%">标题</th><!-- 标题 -->
                <th width="10%">作者</th><!-- 作者+时间 -->
                <th width="10%">回复</th><!-- 回复数+回复者 -->
              </tr>
            </thead>
            <tbody>
              <php>if($threads){</php>
                <foreach name="threads" item="thread">
                  <php>$authorInfo = A('Member')->getInfoByUID($thread['uid']);</php>
                  <php>$lastReply = A('Forum')->getLastReply($thread['id']);</php>
                  <php>$lastReply = A('Member')->getInfoByUID($lastReply['uid'],'name');</php>
                  <php>$replyNum = intval(A('Forum')->getReplyNum($thread['id']));</php>
                  <tr>
                    <td>
                      <p style="margin:10px!important;"><a href="__APP__/Forum/view/tid/{$thread.id}">{$thread.title}</a></p>
                    </td>
                    <td>
                      <small>
                        <img src="{:A('Member')->getAvatar($authorInfo['email'])}">
                        <strong>{$authorInfo.name}</strong>
                      </small><br>
                      <small class="muted">{$thread.timestamp|date="Y-m-d",###}</small>
                    </td>
                    <td>
                      <small><strong>{$replyNum}</strong></small><br>
                      <small class="muted">{:($lastReply ? $lastReply : '暂无')}</small>
                    </td>
                  </tr>
                </foreach>
              <php>}else{</php>
                <tr><td colspan="3"></td></tr>
              <php>}</php>
            </tbody>
          </table>
          <div class="text-right pagenav"><small>{$page_nav}</small></div>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />