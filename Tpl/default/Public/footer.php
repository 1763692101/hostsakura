	<footer>
      <div class="form-actions" id="footer">
        <div class="container">
          <div class="pull-left text-left">
            <p>
              <small>Powered by <strong><a href="http://www.hostsakura.tk" target="_blank">HostSakura</a></strong> {:SAKURA_VERSION}</small><br>
              <small>&copy; {:date("Y")} <a href="{:C('SAKURA_SETTING.siteurl')}">{:C('SAKURA_SETTING.sitename')}</a></small>
            </p>
          </div>
          <div class="pull-right text-right">
            <p>
              <small>{:C('SAKURA_SETTING.foot')}</small><br>
              <small>GMT+8, {:date("Y-m-d H:i")}</small>
            </p>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="__ROOT__/Public/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>