<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>{:C('SAKURA_SETTING.sitename')} - Powered by HostSakura</title>
    <link href="__ROOT__/Public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="keywords" content="{:C('SAKURA_SETTING.keywords')}" />
    <meta name="description" content="{:C('SAKURA_SETTING.descripton')}" />
    <meta name="generator" content="HostSakura {:SAKURA_VERSION} Release {:SAKURA_RELEASE}">
    <meta name="author" content="kiddel[kiddel@qq.com]">
    <meta name="copyright" content="{:date('Y')} {:C('SAKURA_SETTING.sitename')}.">
    <script src="__ROOT__/Public/jquery/jquery-1.10.1.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="navbar navbar-static-top"><!-- navbar-inverse -->
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="{:C('SAKURA_SETTING.siteurl')}">{:C('SAKURA_SETTING.sitename')}</a>
          <ul class="nav">
            <li id="nav_index"><a href="__ROOT__/index.php">首页</a></li>
            <li id="nav_member"><a href="__APP__/Member">会员中心</a></li>
            <li id="nav_product" class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">产品中心<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li id="productnav_cloudhost"><a href="__APP__/Product/cloudhost">云虚拟主机</a></li>
                <li id="productnav_hosting"><a href="__APP__/Product/hosting">普通虚拟主机</a></li>
              </ul>
            </li>
            <li id="nav_forum"><a href="__APP__/Forum">社区</a></li>
          </ul>
          <ul class="nav pull-right">
            <php>if(C('SAKURA_MEMBER')){</php>
              <li id="nav_username"><a><img src="{:A('Member')->getAvatar(C('SAKURA_MEMBER.email'))}">&nbsp;{:C('SAKURA_MEMBER.name')}</a></li>
              <php>if(C('SAKURA_MEMBER.admin')=='1'){</php>
                <li id="nav_admin"><a href="__APP__/Admin"><i class="icon-cog"></i></a></li>
              <php>}</php>
              <li id="nav_logout"><a href="__APP__/Member/logout"><i class="icon-off"></i></a></li>
            <php>}else{</php>
              <li id="nav_login"><a href="__APP__/Member/login">登录</a></li>
              <li id="nav_register"><a href="__APP__/Member/register">注册</a></li>
            <php>}</php>
          </ul>
        </div>
      </div>
    </div><br>