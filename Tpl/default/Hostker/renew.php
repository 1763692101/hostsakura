<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_product").className="active dropdown";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Product">产品中心</a> <span class="divider">/</span></li>
        <li class="active">续费云虚拟主机 {$plan.name}</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <h4>产品中心 - 云虚拟主机 - 续费 {$plan.name}</h4>
          <hr>
          <form class="form-horizontal" action="__APP__/Hostker/dorenew" method="post">
            <div class="control-group">
              <label class="control-label" for="inputPassword">确认登录密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">购买型号</label>
              <div class="controls">
                {$plan.name}&nbsp;[<a href="__APP__/Product/cloudhost">更换</a>]
                <input type="hidden" name="pid" value="{$plan.id}">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">购买时间</label>
              <div class="controls">
                <label class="radio inline">
                  <input type="radio" name="time" id="time_radio_year" value="year" onclick="time_year();" checked>1 年
                </label>
                <label class="radio inline">
                  <input type="radio" name="time" id="time_radio_month" onclick="time_month();" value="month">1 月
                </label>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">所需金额</label>
              <div class="controls">
                <div id="money_need">{$plan.fee} 元</div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">您的余额</label>
              <div class="controls">
                <div id="money_need">{:C('SAKURA_MEMBER.money')} 元</div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPanelusername">控制台用户名</label>
              <div class="controls">
                <input type="text" id="inputPanelusername" name="panelusername" placeholder="请输入控制台用户名">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPanelpassword">控制台密码</label>
              <div class="controls">
                <input type="text" id="inputPanelpassword" name="panelpassword" placeholder="请输入控制台密码">
              </div>
            </div>
            <div class="control-group">
              <div class="controls" id="post_button">
                <php>if(C('SAKURA_MEMBER.money') >= $plan['fee']){</php>
                  <button type="submit" class="btn btn-primary">购买</button>
                <php>}else{</php>
                  <button type="button" class="btn btn-danger">您的余额不足</button>
                <php>}</php>
              </div>
            </div>
          </form>
          <script>
          var year = {$plan.fee};
          var month = {$plan['fee']/10};
          var my = {:C('SAKURA_MEMBER.money')};
		  function time_year(){
		      document.getElementById('money_need').innerHTML = '{$plan.fee} 元';
		      if(my >= year){
			      document.getElementById('post_button').innerHTML = '<button type="submit" class="btn btn-primary">购买</button>';
		      }else{
		    	  document.getElementById('post_button').innerHTML = '<button type="button" class="btn btn-danger">您的余额不足</button>';
		      }
		  }
		  function time_month(){
		      document.getElementById('money_need').innerHTML = '{$plan["fee"]/10} 元';
		      if(my >= month){
			      document.getElementById('post_button').innerHTML = '<button type="submit" class="btn btn-primary">购买</button>';
		      }else{
		    	  document.getElementById('post_button').innerHTML = '<button type="button" class="btn btn-danger">您的余额不足</button>';
		      }
		  }
          </script>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />