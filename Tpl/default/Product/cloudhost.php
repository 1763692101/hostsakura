<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_product").className="active dropdown";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Product">产品中心</a> <span class="divider">/</span></li>
        <li class="active">云虚拟主机</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <h4>产品中心 - 云虚拟主机</h4>
          <hr>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <foreach name="data" item="val">
                  <th>{$val.name}</th>
                </foreach>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>空间大小</th>
                <foreach name="data" item="val">
                  <td>{$val.quota} MB</td>
                </foreach>
              </tr>
              <tr>
                <th>MySQL大小</th>
                <foreach name="data" item="val">
                  <td>{$val.mysql} MB</td>
                </foreach>
              </tr>
              <tr>
                <th>香港/亚洲流量</th>
                <foreach name="data" item="val">
                  <td>{$val.hkbw} GB</td>
                </foreach>
              </tr>
              <tr>
                <th>大陆/欧美流量</th>
                <foreach name="data" item="val">
                  <td>{$val.cnbw} GB</td>
                </foreach>
              </tr>
              <tr>
                <th>CPU时间</th>
                <foreach name="data" item="val">
                  <td>{$val.cputime} 小时</td>
                </foreach>
              </tr>
              <tr>
                <th>查询时间</th>
                <foreach name="data" item="val">
                  <td>{$val.querytime} 小时</td>
                </foreach>
              </tr>
              <tr>
                <th>价格</th>
                <foreach name="data" item="val">
                  <td>{$val.fee} 元/年<br>{:($val['fee']/10)} 元/月</td>
                </foreach>
              </tr>
              <tr>
                <th>购买</th>
                <foreach name="data" item="val">
                  <td><a href="__APP__/Hostker/buy/pid/{$val.id}" class="btn btn-mini btn-primary">购买</a></td>
                </foreach>
              </tr>
            </tbody>
          </table>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />