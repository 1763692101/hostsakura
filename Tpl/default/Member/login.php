<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_login").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">登录</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span12">
  	      <h4>用户登录</h4>
  	      <hr>
  	      <form class="form-horizontal" action="__APP__/Member/dologin" method="post">
            <div class="control-group">
              <label class="control-label" for="inputEmail">E-Mail</label>
              <div class="controls">
                <input type="text" id="inputEmail" name="email" placeholder="请输入E-Mail">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword">密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">登录</button>
              </div>
            </div>
          </form>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />