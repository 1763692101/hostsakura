<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">首页</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_index").className="active";</script>
        </div>
        <div class="span9">
          <h4>系统通知</h4>
          <php>if($notices){</php>
            <foreach name="notices" item="notice">
              <div class="alert {$notice['type']}">
                <strong>{$notice['timestamp']|date="Y-m-d",###}：</strong>{$notice['context']}
              </div>
            </foreach>
          <php>}else{</php>
            <div class="alert alert-info">
              <strong>#</strong>暂无系统通知
            </div>
          <php>}</php>
          <hr>
          <h4>我的资料</h4>
          <table class="table table-bordered">
            <tr>
              <th width="20%">UID</th>
              <td width="30%">{:C('SAKURA_MEMBER.uid')}</td>
              <th width="20%">E-Mail</th>
              <td width="30%">{:C('SAKURA_MEMBER.email')}</td>
            </tr>
            <tr>
              <th>姓名</th>
              <td>{:C('SAKURA_MEMBER.name')}</td>
              <th>手机</th>
              <td>{:C('SAKURA_MEMBER.phone')}</td>
            </tr>
            <tr>
              <th>上次登录IP</th>
              <td>{:C('SAKURA_MEMBER.lastip')}</td>
              <th>上次登录时间</th>
              <td>{:date("Y-m-d H:i:s",C('SAKURA_MEMBER.lasttime'))}</td>
            </tr>
            <tr>
              <th>账号等级</th>
              <td>
                <php>if(C('SAKURA_MEMBER.admin')=='1'){</php>管理员
                <php>}else{</php>普通用户
                <php>}</php>
              </td>
              <th>账号余额</th>
              <td>{:C('SAKURA_MEMBER.money')} 元</td>
            </tr>
          </table>
          <hr>
          <h4>统计信息</h4>
          <table class="table table-bordered">
            <tr>
              <th width="20%">云虚拟主机数</th>
              <td width="30%">{$count.cloudhost}</td>
              <th width="20%">订单数</th>
              <td width="30%">{$count.order}</td>
            </tr>
          </table>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />