<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_register").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">注册</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span12">
  	      <h4>用户注册</h4>
  	      <hr>
  	      <form class="form-horizontal" action="__APP__/Member/doregister" method="post">
            <div class="control-group">
              <label class="control-label" for="inputEmail">E-Mail</label>
              <div class="controls">
                <input type="text" id="inputEmail" name="email" placeholder="请输入E-Mail">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword">密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword2">确认密码</label>
              <div class="controls">
                <input type="password" id="inputPassword2" name="password2" placeholder="请再次输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputName">姓名</label>
              <div class="controls">
                <input type="text" id="inputName" name="name" placeholder="请输入姓名">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPhone">手机</label>
              <div class="controls">
                <input type="text" id="inputPhone" name="phone" placeholder="请输入手机">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputInvite">邀请码</label>
              <div class="controls">
                <php>if(C('SAKURA_SETTING.open')=='1'){</php>
                  <input type="text" id="inputInvite" name="invite" placeholder="请输入邀请码，没有可以不填">
                <php>}elseif(C('SAKURA_SETTING.open')=='2'){</php>
                  <input type="text" id="inputInvite" name="invite" placeholder="请输入邀请码">
                <php>}</php>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">注册</button>
              </div>
            </div>
          </form>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />