<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">我的云虚拟主机</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_host_mycloud").className="active";</script>
        </div>
        <div class="span9">
          <h4>我的云虚拟主机</h4>
          <hr>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>控制台用户名</th>
                <th>产品型号</th>
                <th>过期时间</th>
                <th>管理</th>
              </tr>
            </thead>
            <tbody>
              <php>if($myHosts){</php>
                <foreach name="myHosts" item="myHost">
                  <tr>
                    <td>{$myHost['id']}</td>
                    <td>{$myHost['panelusername']}</td>
                    <td>{:A('Hostker')->getPlanInfo($myHost['pid'],'name')}</td>
                    <td>{$myHost['exptime']}</td>
                    <td>
                      <a class="btn btn-mini btn-primary" href="__APP__/Hostker/panel/hid/{$myHost['id']}">面板</a>
                      <a class="btn btn-mini" href="__APP__/Hostker/renew/hid/{$myHost['id']}">续费</a>
                      <a class="btn btn-mini" href="__APP__/Hostker/update/hid/{$myHost['id']}">升级</a>
                    </td>
                  </tr>
                </foreach>
              <php>}else{</php>
                <tr><td colspan="5">没有查询到数据</td></tr>
              <php>}</php>
            </tbody>
          </table>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />