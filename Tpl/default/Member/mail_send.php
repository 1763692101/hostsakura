<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">发送邮件</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_mail_send").className="active";</script>
        </div>
        <div class="span9">
          <h4>发送邮件</h4>
          <hr>
          <form class="form-horizontal" action="__APP__/Member/post_mail_send" method="post">
            <div class="control-group">
              <label class="control-label" for="inputReceiver">收件人</label>
              <div class="controls">
                <input type="text" id="inputReceiver" class="span8" name="receiver" placeholder="请输入收件人E-Mail">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputTitle">标题</label>
              <div class="controls">
                <input type="text" id="inputTitle" name="title" class="span8" placeholder="请输入邮件标题">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputContext">内容</label>
              <div class="controls">
                <textarea id="inputContext" name="context" class="span8" rows="10" placeholder="请输入邮件内容"></textarea>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary">发送</button>
              <button type="button" class="btn" onclick="reset();">重置</button>
            </div>
          </form>
          <script>
          function reset(){
              document.getElementById('inputReceiver').value="";
              document.getElementById('inputTitle').value="";
              document.getElementById('inputContext').value="";
          }
          </script>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />