<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member/mail_list">邮件</a> <span class="divider">/</span></li>
        <li class="active">{$dbData['title']}</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_mail_list").className="active";</script>
        </div>
        <div class="span9">
          <h4>
          <switch name="dbData.type" >
  	          <case value="1"><span class="badge badge-info">普通</span></case>
              <case value="2"><span class="badge badge-success">管理</span></case>
              <case value="3"><span class="badge badge-warning">系统</span></case>
              <default /><span class="badge badge-inverse">未知</span>
            </switch>
            {$dbData['title']}
          </h4>
          <hr>
          <div style="margin:10px;">{$dbData['context']|nl2br}</div>
          <hr>
          <div class="text-right"><small>
            {:A('Member')->getInfoByUID($dbData['sender'],'name')}
            [{:A('Member')->getInfoByUID($dbData['sender'],'email')}]
          </small></div>
          <div class="text-right"><small class="muted">{$dbData['timestamp']|date="Y-m-d H:i:s",###}</small></div>
          <div class="text-center">
            <a class="btn btn-large btn-primary" href="__APP__/Member/mail_reply/mid/{$dbData['id']}">回复</a>
          </div>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />