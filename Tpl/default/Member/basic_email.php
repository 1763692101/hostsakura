<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">用户邮箱修改</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_basic_email").className="active";</script>
        </div>
        <div class="span9">
          <h4>用户邮箱修改</h4>
          <hr>
          <form class="form-horizontal" action="__APP__/Member/post_basic_email" method="post">
            <div class="control-group">
              <label class="control-label" for="inputPassword">确认密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">原E-Mail</label>
              <div class="controls">
                <p>{:C('SAKURA_MEMBER.email')}</p>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputEmail">新E-Mail</label>
              <div class="controls">
                <input type="text" id="inputEmail" name="email" placeholder="请输入新E-Mail">
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary">提交</button>
              <button type="button" class="btn" onclick="reset();">重置</button>
            </div>
          </form>
          <script>
          function reset(){
              document.getElementById('inputEmail').value="";
          }
          </script>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />