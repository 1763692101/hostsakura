<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">登录密码修改</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_basic_pwd").className="active";</script>
        </div>
        <div class="span9">
          <h4>登录密码修改</h4>
          <hr>
          <form class="form-horizontal" action="__APP__/Member/post_basic_pwd" method="post">
            <div class="control-group">
              <label class="control-label" for="inputPassword">原密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入原密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword1">新密码</label>
              <div class="controls">
                <input type="password" id="inputPassword1" name="password1" placeholder="请输入新密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword2">确认新密码</label>
              <div class="controls">
                <input type="password" id="inputPassword2" name="password2" placeholder="请再次输入新密码">
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary">提交</button>
              <button type="button" class="btn" onclick="reset();">重置</button>
            </div>
          </form>
          <script>
          function reset(){
              document.getElementById('inputPassword').value="";
              document.getElementById('inputPassword1').value="";
              document.getElementById('inputPassword2').value="";
          }
          </script>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />