<ul class="nav nav-tabs nav-stacked">
            <li class="nav-header">系统</li>
            <li id="sidenav_index"><a href="__APP__/Member">会员中心首页</a></li>
            <li class="nav-header">用户</li>
            <li id="sidenav_basic_info"><a href="__APP__/Member/basic_info">基本资料修改</a></li>
            <li id="sidenav_basic_pwd"><a href="__APP__/Member/basic_pwd">登录密码修改</a></li>
            <li id="sidenav_basic_email"><a href="__APP__/Member/basic_email">用户邮箱修改</a></li>
            <li class="nav-header">主机</li>
            <li id="sidenav_host_mycloud"><a href="__APP__/Member/host_mycloud">我的云虚拟主机</a></li>
            <li id="sidenav_host_my"><a href="__APP__/Member/host_my">我的普通虚拟主机</a></li>
            <li><a href="__APP__/Product">购买新主机</a></li>
            <li class="nav-header">财务</li>
            <li id="sidenav_money_info"><a href="__APP__/Member/money_info">我的财务信息</a></li>
            <li id="sidenav_money_log"><a href="__APP__/Member/money_log">交易记录</a></li>
            <li id="sidenav_money_recharge"><a href="__APP__/Member/money_recharge">在线充值</a></li>
            <li class="nav-header">邮件</li>
            <li id="sidenav_mail_list"><a href="__APP__/Member/mail_list">邮件列表</a></li>
            <li id="sidenav_mail_send"><a href="__APP__/Member/mail_send">发送邮件</a></li>
          </ul>