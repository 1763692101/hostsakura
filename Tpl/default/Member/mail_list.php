<include file="Public:header" />
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">邮件列表</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <include file="Member:sidenav" />
  	      <script type="text/javascript">document.getElementById("sidenav_mail_list").className="active";</script>
        </div>
        <div class="span9">
          <h4>邮件列表</h4>
          <hr>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>类型</th>
                <th>标题</th>
                <th>发送者</th>
                <th>发送时间</th>
                <th>状态</th>
              </tr>
            </thead>
            <tbody>
              <php>if($myMails){</php>
                <foreach name="myMails" item="myMail">
                  <tr>
                    <td><small>{$myMail['id']}</small></td>
                    <td>
                      <switch name="myMail.type" >
  	                    <case value="1"><span class="badge badge-info">普通</span></case>
                        <case value="2"><span class="badge badge-success">管理</span></case>
                        <case value="3"><span class="badge badge-warning">系统</span></case>
                        <default /><span class="badge badge-inverse">未知</span>
                      </switch>
                    </td>
                    <td><a href="__APP__/Member/mail_view/mid/{$myMail['id']}">{$myMail['title']}</a></td>
                    <td><small>
                      {:A('Member')->getInfoByUID($myMail['sender'],'name')}
                      [{:A('Member')->getInfoByUID($myMail['sender'],'email')}]
                    </small></td>
                    <td><small>{$myMail['timestamp']|date="Y-m-d H:i:s",###}</small></td>
                    <td>
                      <switch name="myMail.status" >
                        <case value="1"><span class="badge badge-success">已读</span></case>
                        <default /><span class="badge badge-important">未读</span>
                      </switch>
                    </td>
                  </tr>
                </foreach>
              <php>}else{</php>
                <tr><td colspan="6">没有查询到数据</td></tr>
              <php>}</php>
            </tbody>
          </table>
        </div>
	  </div>
	</div>
	
<include file="Public:footer" />