<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo (C("SAKURA_CONFIG.sitename")); ?> - Powered by HostSakura</title>
	<meta name="keywords" content="<?php echo (C("SAKURA_CONFIG.keywords")); ?>">
    <meta name="description" content="<?php echo (C("SAKURA_CONFIG.description")); ?>">
    <meta name="generator" content="HostSakura Ver.<?php echo (C("SAKURA_VERSION")); ?> Rel.<?php echo (C("SAKURA_RELEASE")); ?> Dev.<?php echo (SAKURA_DEVREL); ?>" />
	<meta name="author" content="Kiddel[kiddel.mail@gmail.com]">
    <link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/main.css" rel="stylesheet">
	<link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/bootstrap.css" rel="stylesheet">
	<link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/bootstrap-responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div id="is">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
   <div class="container">
	  <a class="brand" href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>
		<ul class="nav">
		  <li><a href="__ROOT__/index.php">主页</a></li>
		  <li><a href="__APP__/Member/index">用户中心</a></li>
		  <li><a href="__APP__/Page/about">关于我们</a></li>
		  <li><a href="__APP__/Forum">讨论区</a></li>
		</ul>
        <ul class="nav pull-right">
        	<?php if(!C('SAKURA_MEMBER')){ ?>
 				<li><a href="__APP__/Member/login">登录</a></li>
          		<li><a href="__APP__/Member/reg">注册</a></li>
          	<?php }else{ ?>
 				<li><a href="javascript:void(0);">欢迎您，<?php echo (C("SAKURA_MEMBER.name")); ?></a></li>
          		<li><a href="__APP__/Member/info">资料</a></li>
          		<li><a href="__APP__/Member/logout">退出</a></li>
 			<?php } ?>
        </ul>
	 </div>
	</div>
</div>
<?php if(C('SAKURA_MEMBER')){ ?>
<?php if(A('Mail')->CheckWaitMail() == true){ ?>
<div class="alert alert-info"><b>您有新邮件，点击[<a href="__APP__/Mail">这里</a>]查看</b></div>
<?php } ?>
<?php } ?>


<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Host">我的主机列表</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <ul class="nav nav-tabs nav-stacked">
<li class="nav-header">用户中心</li>
<li><a href="__APP__/Member">用户中心首页</a></li>
<li><a href="__APP__/Member/logout">退出登录</a></li>

<li class="nav-header">资料管理</li>
<li><a href="__APP__/Member/info">基础资料修改</a></li>
<li><a href="__APP__/Member/changepwd">用户密码修改</a></li>
<li><a href="__APP__/Member/changemail">用户E-Mail修改</a></li>

<li class="nav-header">财务管理</li>
<li><a href="__APP__/Member/paylogs">交易记录</a></li>
<li><a href="__APP__/Member/recharge">在线充值</a></li>

<li class="nav-header">产品管理</li>
<li><a href="__APP__/Host">我的主机列表</a></li>
<li><a href="__APP__/Host/buy">购买主机</a></li>

<li class="nav-header">站内邮件管理</li>
<li><a href="__APP__/Mail">邮件列表</a></li>
<li><a href="__APP__/Mail/waitoread">未读邮件</a></li>
</ul>
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>我的主机列表</h3></div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>控制面板用户名</th>
      <th>主机类型</th>
      <th>到期时间</th>
      <th>操作</th>
    </tr>
  </thead>
  <tbody>
  <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><tr>
      <td><?php echo ($val["id"]); ?></td>
      <td><?php echo ($val["panelusername"]); ?></td>
      <td><?php echo ($val["type"]); ?></td>
      <td><?php echo ($val["exptime"]); ?></td>
      <td>
        <a class="btn btn-mini" href="javascript:void(0);" onclick="window.open('__APP__/Host/panel/hid/<?php echo ($val["id"]); ?>');">控制面板</a>
        <a class="btn btn-mini" href="__APP__/Host/renew/hid/<?php echo ($val["id"]); ?>">续费</a>
      </td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  </tbody>
</table>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<hr>
<div align="center">
	 &copy; <?php echo date("Y");?> <a href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>. 
	 Powered by <a href="http://blog.iharuka.tk" target="_blank">HostSakura</a>
</div>
<p>&nbsp;</p>
</div> 
</body>
</html>