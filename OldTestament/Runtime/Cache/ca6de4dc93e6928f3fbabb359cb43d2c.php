<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo (C("SAKURA_CONFIG.sitename")); ?> - Powered by HostSakura</title>
	<meta name="keywords" content="<?php echo (C("SAKURA_CONFIG.keywords")); ?>">
    <meta name="description" content="<?php echo (C("SAKURA_CONFIG.description")); ?>">
    <meta name="generator" content="HostSakura Ver.<?php echo (C("SAKURA_VERSION")); ?> Rel.<?php echo (C("SAKURA_RELEASE")); ?> Dev.<?php echo (SAKURA_DEVREL); ?>" />
	<meta name="author" content="Kiddel[kiddel.mail@gmail.com]">
    <link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/main.css" rel="stylesheet">
	<link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/bootstrap.css" rel="stylesheet">
	<link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/bootstrap-responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div id="is">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
   <div class="container">
	  <a class="brand" href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>
		<ul class="nav">
		  <li><a href="__ROOT__/index.php">主页</a></li>
		  <li><a href="__APP__/Member/index">用户中心</a></li>
		  <li><a href="__APP__/Page/about">关于我们</a></li>
		  <li><a href="__APP__/Forum">讨论区</a></li>
		</ul>
        <ul class="nav pull-right">
        	<?php if(!C('SAKURA_MEMBER')){ ?>
 				<li><a href="__APP__/Member/login">登录</a></li>
          		<li><a href="__APP__/Member/reg">注册</a></li>
          	<?php }else{ ?>
 				<li><a href="javascript:void(0);">欢迎您，<?php echo (C("SAKURA_MEMBER.name")); ?></a></li>
          		<li><a href="__APP__/Member/info">资料</a></li>
          		<li><a href="__APP__/Member/logout">退出</a></li>
 			<?php } ?>
        </ul>
	 </div>
	</div>
</div>
<?php if(C('SAKURA_MEMBER')){ ?>
<?php if(A('Mail')->CheckWaitMail() == true){ ?>
<div class="alert alert-info"><b>您有新邮件，点击[<a href="__APP__/Mail">这里</a>]查看</b></div>
<?php } ?>
<?php } ?>


<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Host/buy">购买主机</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <ul class="nav nav-tabs nav-stacked">
<li class="nav-header">用户中心</li>
<li><a href="__APP__/Member">用户中心首页</a></li>
<li><a href="__APP__/Member/logout">退出登录</a></li>

<li class="nav-header">资料管理</li>
<li><a href="__APP__/Member/info">基础资料修改</a></li>
<li><a href="__APP__/Member/changepwd">用户密码修改</a></li>
<li><a href="__APP__/Member/changemail">用户E-Mail修改</a></li>

<li class="nav-header">财务管理</li>
<li><a href="__APP__/Member/paylogs">交易记录</a></li>
<li><a href="__APP__/Member/recharge">在线充值</a></li>

<li class="nav-header">产品管理</li>
<li><a href="__APP__/Host">我的主机列表</a></li>
<li><a href="__APP__/Host/buy">购买主机</a></li>

<li class="nav-header">站内邮件管理</li>
<li><a href="__APP__/Mail">邮件列表</a></li>
<li><a href="__APP__/Mail/waitoread">未读邮件</a></li>
</ul>
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>购买主机</h3></div>
<div class="alert alert-info">注：所有空间均支持月付，月付价格为<b>年付价格 / 10</b></div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>计划<br>名称</th>
      <th>文件硬盘<br>(MB)</th>
      <th>数据库硬盘<br>(MB)</th>
      <th>香港/亚洲CDN<br>流量(MB)</th>
      <th>大陆/欧美CDN<br>流量(MB)</th>
      <th>CPU时间<br>(min)</th>
      <th>数据库查询<br>时间(min)</th>
      <th>价格<br>(元/年)</th>
      <th>购买</th>
    </tr>
  </thead>
  <tbody>
  <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><tr>
      <td><?php echo ($val["name"]); ?></td>
      <td><?php echo ($val["quota"]); ?></td>
      <td><?php echo ($val["mysql"]); ?></td>
      <td><?php echo ($val["hkbw"]); ?></td>
      <td><?php echo ($val["cnbw"]); ?></td>
      <td><?php echo ($val["cputime"]); ?></td>
      <td><?php echo ($val["querytime"]); ?></td>
      <td><?php echo ($val["fee"]); ?></td>
      <td><a class="btn btn-mini" href="__APP__/Host/buyguide/pid/<?php echo ($val["id"]); ?>">购买</a></td>
    </tr><?php endforeach; endif; else: echo "" ;endif; ?>
  </tbody>
</table>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<hr>
<div align="center">
	 &copy; <?php echo date("Y");?> <a href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>. 
	 Powered by <a href="http://blog.iharuka.tk" target="_blank">HostSakura</a>
</div>
<p>&nbsp;</p>
</div> 
</body>
</html>