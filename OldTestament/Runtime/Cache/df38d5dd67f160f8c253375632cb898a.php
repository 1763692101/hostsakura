<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php echo (C("SAKURA_CONFIG.sitename")); ?> - Powered by HostSakura</title>
	<meta name="keywords" content="<?php echo (C("SAKURA_CONFIG.keywords")); ?>">
    <meta name="description" content="<?php echo (C("SAKURA_CONFIG.description")); ?>">
    <meta name="generator" content="HostSakura <?php echo (C("SAKURA_VERSION")); ?>.<?php echo (C("SAKURA_RELEASE")); ?>" />
	<meta name="author" content="Kiddel[kiddel.mail@gmail.com]">
    <link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/main.css" rel="stylesheet">
	<link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/bootstrap.css" rel="stylesheet">
	<link href="__ROOT__<?php echo (C("PUBLIC_URL")); ?>/style/bootstrap-responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div id="is">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
   <div class="container">
	  <a class="brand" href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>
		<ul class="nav">
		  <li><a href="__ROOT__/index.php">主页</a></li>
		  <li><a href="__APP__/Member/index">用户中心</a></li>
		  <li><a href="__APP__/Page/about">关于我们</a></li>
		  <li><a href="__APP__/Forum">讨论区</a></li>
		</ul>
        <ul class="nav pull-right">
        	<?php if(!C('SAKURA_MEMBER')){ ?>
 				<li><a href="__APP__/Member/login">登录</a></li>
          		<li><a href="__APP__/Member/reg">注册</a></li>
          	<?php }else{ ?>
 				<li><a href="javascript:void(0);">欢迎您，<?php echo (C("SAKURA_MEMBER.name")); ?></a></li>
          		<li><a href="__APP__/Member/info">资料</a></li>
          		<li><a href="__APP__/Member/logout">退出</a></li>
 			<?php } ?>
        </ul>
	 </div>
	</div>
</div>
<?php if(C('SAKURA_MEMBER')){ ?>
<?php if(A('Mail')->CheckWaitMail() == true){ ?>
<div class="alert alert-info"><b>您有新邮件，点击[<a href="__APP__/Mail">这里</a>]查看</b></div>
<?php } ?>
<?php } ?>


<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Page/about">关于我们</a>
	</li>
</ul>

<div class="row-fluid">
	<div class="span12">
		<div class="hero-unit">
			<h2 align="center">关于我们</h2>
			<?php echo ($context); ?>
		</div>
	</div>
</div>
	
  </div> <!-- is end -->

<hr>
<div align="center">
	 &copy; <?php echo date("Y");?> <a href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>. 
	 Powered by <a href="http://blog.iharuka.tk" target="_blank">HostSakura</a>
</div>
<p>&nbsp;</p>
</div> 
</body>
</html>