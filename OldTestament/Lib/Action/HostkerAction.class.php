<?php if(!defined('THINK_PATH')) exit('Access Denied');
/**
 * @name HostKer Control API For HostSakura
 * @author kiddel
 * @version 1.0
 */
class HostkerAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    }
    /**
     * @name 提交数据
     * 
     * @param data 提交的数据
     * @param url 提交到的API地址[默认为http://i.hostker.com/rapi/doAction]
     * 
     * @return API返回的结果
     */
    public function postData($data,$url='http://i.hostker.com/rapi/doAction'){
    	$cfg = C('SAKURA_CONFIG');
    	$DES=new Des($cfg['apf_key'],$cfg['apf_id']);
    	ksort($data);
    	$json = json_encode($data);
    	$postdata = $DES->encrypt($json);
    	$post['resellerid'] = $cfg['apf_id'];
    	$post['data'] = $postdata;
    	$post['sign'] = md5($json.$cfg['apf_key']);
    	$ch=curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_HEADER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	$result=curl_exec($ch);
    	curl_close($ch);
    	return $result;
    }
    /**
     * @name 开通主机
     * 
     * @param panelusername 控制面板账号，主机名也是这个开头
     * @param panelpassword 控制面板密码
     * @param type 主机型号，现在只能传入数字1-5，具体请参考代理商成本价格表
     * @param time 付款周期，只能传入字符串month(表示月付)或者year(表示年付)
     * @param action createHost
     * 
     * @return 0 开通失败，请立即与Hostker技术联系
     * @return 1 用户名不符合规定（只能字母或者数字，4-12位）
     * @return 2 密码不科学（忘记传了或者别的特殊情况）
     * @return 3 用户名存在（先检查再开通就不会有这情况）
     * @return 4 型号不存在（数字1-5不能传别的）
     * @return 5 付款周期不正确（只能传入字符串month或者year或者test）
     * @return 6 代理商账号余额不足
     * @return 50 sign签名验证失败
     * @return 51 代理商不存在
     * @return 201xxxxx 到期年月日，月和日都是两位数，表示开通成功
     */
    public function createHost($panelusername,$panelpassword,$type,$time='month'){
    	$data = array();
    	$data['panelusername'] = $panelusername;
    	$data['panelpassword'] = $panelpassword;
    	$data['type'] = $type;
    	$data['time'] = $time;
    	$data['action'] = 'createHost';
    	$response = $this->postData($data);
    	return $response;
    }
    /**
     * @name 检查用户名是否被占用
     * 
     * @param panelusername 控制面板用户名，只能是字母或者数字，4-12位
     * @param action checkHostname
     * 
     * @return 0 用户名被占用或者不符合规定，总之就是不能用！
     * @return 1 用户名可用
     * @return 50 sign签名验证失败
     * @return 51 代理商不存在
     */
    public function checkHostname($panelusername){
    	$data = array();
    	$data['panelusername'] = $panelusername;
    	$data['action'] = 'checkHostname';
    	$response = $this->postData($data);
    	return $response;
    }
    /**
     * @name 检查代理商余额
     * 
     * @param action balance
     * 
     * @return 50 sign签名验证失败
     * @return 51 代理商不存在
     * @return float 浮点数(精确到小数点后两位)表示代理商余额
     */
    public function balance(){
    	$data = array();
    	$data['action'] = 'balance';
    	$response = $this->postData($data);
    	return $response;
    }
    /**
     * @name 修改控制面板登录密码
     * 
     * @param action changePw
     * @param panelusername 控制面板用户名
     * @param panelpassword 新的登录密码
     * 
     * @return 0 修改失败
     * @return 1 修改成功
     * @return 50 sign签名验证失败
     * @return 51 代理商不存在
     */
    public function changePw($panelusername,$panelpassword){
    	$data = array();
    	$data['panelusername'] = $panelusername;
    	$data['panelpassword'] = $panelpassword;
    	$data['action'] = 'changePw';
    	$response = $this->postData($data);
    	return $response;
    }
    /**
     * @name 续费
     * 
     * @param panelusername 控制面板账号，主机名也是这个开头
     * @param time 付款周期，只能传入字符串month(表示月付)或者year(表示年付)
     * @param action renewHost
     * 
     * @return 0 主机不存在
     * @return 1 用户名不符合规定（只能字母或者数字，4-12位）
     * @return 5 付款周期不正确（只能传入字符串month或者year或者test）
     * @return 6 代理商账号余额不足
     * @return 50 sign签名验证失败
     * @return 51 代理商不存在
     * @return 201xxxxx 到期年月日，月和日都是两位数，表示续费成功
     */
    public function renewHost($panelusername,$time='month'){
    	$data = array();
    	$data['panelusername'] = $panelusername;
    	$data['time'] = $time;
    	$data['action'] = 'renewHost';
    	$response = $this->postData($data);
    	return $response;
    }
    /**
     * @name 升级
     * 
     * @param panelusername 控制面板账号，主机名也是这个开头
     * @param type 新型号
     * @param action upgradeHost
     * 
     * @return 0 续费成功
     * @return 1 用户名不存在
     * @return 2 新型号小于等于老型号，只能升级不能降级
     * @return 3 代理商账号余额不足
     */
    public function upgradeHost($panelusername,$type){
    	$data = array();
    	$data['panelusername'] = $panelusername;
    	$data['type'] = $type;
    	$data['actoin'] = 'upgradeHost';
    	$response = $this->postData($data);
    	return $response;
    }
    /**
     * @name 计算升级差价
     * 
     * @param oldfee 原价格
     * @param newfee 新价格
     * @param duedate 原到期时间
     * 
     * @return number
     */
    public function upgradeFee($oldfee,$newfee,$duedate){
    	$fee=$newfee-$oldfee;
    	$today=date('Ymd',time());
    	if($today == $duedate){
    		$days=1;
    	}else{
    		$unixtime=strtotime($duedate);
    		$unixtime=$unixtime-time();
    		$days=$unixtime / 86400;
    	}
    	$fee=$fee/10;
    	$fee=$fee*$days;
    	$fee=$fee/30;
    	$fee=round($fee,2);
    	return $fee;
    }
}