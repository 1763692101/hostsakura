<?php if(!defined('THINK_PATH')) exit('Access Denied');
class MailAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    	A('Cron')->InitCron();
    }
    public function index(){
    	A('Member')->checklogin();
    	$Model = M('Mail');
    	$member = C('SAKURA_MEMBER');
    	$count = $Model->where("`email`='".$member['email']."'")->count();
    	$Page = new Page($count,20);
    	$Page->url = 'Mail/index/p';
    	$show = urldecode($Page->show());
    	$list = $Model->where("`email`='".$member['email']."'")->order('timestamp desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->assign('page_nav',$show);
    	$this->assign('list',$list);
    	$this->display();
    }
    public function waitoread(){
    	A('Member')->checklogin();
    	$Model = M('Mail');
    	$member = C('SAKURA_MEMBER');
    	$count = $Model->where("`email`='".$member['email']."'")->count();
    	$Page = new Page($count,20);
    	$Page->url = 'Mail/index/p';
    	$show = urldecode($Page->show());
    	$list = $Model->where("`email`='".$member['email']."' AND `status`='0'")->order('timestamp desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->assign('page_nav',$show);
    	$this->assign('list',$list);
    	$this->display();
    }
    public function read(){
    	A('Member')->checklogin();
    	$mid = I('get.mid');
    	if(!$mid) $this->showmessage('未定义操作','','error');
    	$Model = M('Mail');
    	$member = C('SAKURA_MEMBER');
    	$data = $Model->where("`id`='".$mid."'")->select();
    	if($data['0']['email']!=$member['email']) $this->showmessage('看别人的邮件是不礼貌的哦~','','error');
    	$Model->where("`id`='".$mid."'")->save(array('status'=>'1'));
    	$this->assign('data',$data['0']);
    	$this->display();
    }
    
    
    public function CheckWaitMail(){
    	$Model = M('Mail');
    	$member = C('SAKURA_MEMBER');
    	if(!$member['email']) return false;
    	$count = $Model->where("`status`='0' AND `email`='".$member['email']."'")->count();
    	if($count == '0') return false;
    	else return true;
    }
    public function SendMail($email,$title,$context){
    	$context = self::BuildMail($context);
    	$data = array();
    	$data['email'] = $email;
    	$data['title'] = $title;
    	$data['context'] = $context;
    	$data['timestamp'] = mktime();
    	$data['status'] = '0';
    	self::SendEMail($email, $title, $context);
    	M('Mail')->add($data);
    }
    public function SendEMail($email,$title,$context){
    	$config = C('SAKURA_CONFIG');
    	$member = M('User')->where("`email`='".$email."'")->select();
    	$member = $member['0'];
    	vendor('PHPMailer.class#phpmailer');
    	$mail = new PHPMailer();
    	$mail->CharSet = 'UTF-8';
    	$mail->IsSMTP();
    	$mail->SMTPDebug = 0;
    	$mail->SMTPAuth = true;
    	$mail->SMTPSecure = '';
    	$mail->Host = $config['smtp_server'];
    	$mail->Port = $config['smtp_port'];
    	$mail->Username = $config['smtp_username'];
    	$mail->Password = $config['smtp_password'];
    	$mail->SetFrom($config['smtp_email'], $config['sitename']);
    	$replyEmail = $config['smtp_email'];
    	$replyName = $config['sitename'];
    	$mail->AddReplyTo($replyEmail, $replyName);
    	$mail->Subject = $title;
    	$mail->MsgHTML($context);
    	$mail->AddAddress($email, $member['name']);
    	return $mail->Send() ? true : $mail->ErrorInfo;
    }
    public function BuildMail($content){
    	$cfg = C('SAKURA_CONFIG');
    	$response  = "<h1>「".$cfg['sitename']."」</h1>";
    	$response .= "<p>&nbsp;</p>";
    	$response .= "<p>".$content."</p>";
    	$response .= "<p>&nbsp;</p>";
    	$response .= "<p>&copy; ".date("Y")." <a href='http://".$_SERVER['HTTP_HOST'].U('Index/index')."' target='_blank'>".$cfg['sitename']."</a></p>";
    	$response .= "<br>";
    	return $response;
    }
}