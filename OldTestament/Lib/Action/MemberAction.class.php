<?php if(!defined('THINK_PATH')) exit('Access Denied');
class MemberAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    	A('Cron')->InitCron();
    }
    public function index(){
    	$this->checklogin();
    	$this->display();
    }
    public function checklogin(){
    	if(C('SAKURA_MEMBER')==false) $this->showmessage('您还没有登录',U('Member/login'));
    }
    public function login(){
    	if(C('SAKURA_MEMBER')!=false) $this->showmessage('您已经登录，下面跳转到用户中心',U('Member/index'));
    	$this->display();
    }
    public function reg(){
    	if(C('SAKURA_MEMBER')!=false) $this->showmessage('您已经登录，下面跳转到用户中心',U('Member/index'));
    	$this->display();
    }
    public function logout(){
    	session('sakura_member',null);
    	$this->showmessage('退出成功',U('Index/index'));
    }
    public function do_login(){
    	if(C('SAKURA_MEMBER')!=false) $this->showmessage('您已经登录，下面跳转到用户中心',U('Member/index'));
    	$Model = M('User');
    	$email = I('post.email');
    	$pwd = I('post.password');
    	$dbInfo = $Model->where("`email`='".$email."'")->select();
    	$dbInfo = $dbInfo['0'];
    	if($dbInfo['password'] == md5(md5($pwd))){
    		$sessData = serialize($dbInfo);
    		session('sakura_member',$sessData);
    		$Model->where("`email`='".$email."'")->save(array('lastip'=>$_SERVER['REMOTE_ADDR'],'lasttime'=>mktime()));
    		$this->showmessage('登录成功，下面跳转到用户中心',U('Member/index'));
    	}else{
    		$this->showmessage('用户名或密码不正确','','error');
    	}
    }
    public function do_reg(){
    	if(C('SAKURA_MEMBER')!=false) $this->showmessage('您已经登录，下面跳转到用户中心',U('Member/index'));
    	$Model = M('User');
    	$cfg = C('SAKURA_CONFIG');
    	if(I('post.pwd1') == I('post.pwd2')){
    		$data = array();
    		$data['email'] = I('post.email');
    		$data['password'] = md5(md5(I('post.pwd1')));
    		$data['money'] = '0';
    		$data['name'] = I('post.name');
    		$data['phone'] = I('post.phone');
    		$data['regip'] = $_SERVER['REMOTE_ADDR'];
    		$data['regtime'] = mktime();
    		$data['lastip'] = $_SERVER['REMOTE_ADDR'];
    		$data['lasttime'] = mktime();
    		$res = $Model->where("`email`='".$data['email']."'")->select();
    		if(!$res){
    			$Model->add($data);
    			$context = @str_replace('{sitename}',$cfg['sitename'],$cfg['welcomemail']);
    			A('Mail')->SendMail($data['email'],'欢迎注册「'.$cfg['sitename'].'」',$context);
    			$this->showmessage('注册成功，请登录',U('Member/login'));
    		}else{
    			$this->showmessage('E-Mail已被使用','','error');
    		}
    	}else{
    		$this->showmessage('两次密码输入不一致','','error');
    	}
    }
    public function changemail(){
    	$this->checklogin();
    	$this->display();
    }
    public function post_changemail(){
    	$this->checklogin();
    	$neweml = I('post.neweml');
    	if(!$neweml) $this->showmessage('您没有输入新邮箱','','error');
    	$sessInfo = C('SAKURA_MEMBER');
    	if($sessInfo['password'] != md5(md5(I('post.pwd')))) $this->showmessage('密码输入错误','','error');
    	$response = M('User')->where("`uid`='".$sessInfo['uid']."'")->save(array('email'=>$neweml));
    	if($response>0){
    		session('sakura_member',null);
    		$this->showmessage('修改成功，请重新登录',U('Member/login'));
    	}else{
    		$this->showmessage('修改失败，请重试','','error');
    	}
    }
    public function info(){
    	$this->checklogin();
    	$this->display();
    }
    public function post_info(){
    	$this->checklogin();
    	$sessInfo = C('SAKURA_MEMBER');
    	if($sessInfo['password'] != md5(md5(I('post.password')))) $this->showmessage('密码输入错误','','error');
    	$data = array();
    	$data['name'] = I('post.name');
    	$data['phone'] = I('post.phone');
    	$Model = M('User');
    	$response = $Model->where("`email`='".$sessInfo['email']."'")->save($data);
    	$sessInfo['name'] = $data['name'];
    	$sessInfo['phone'] = $data['phone'];
    	if($response>0){
    		session('sakura_member',serialize($sessInfo));
    		$this->showmessage('修改成功',U('Member/info'));
    	}else{
    		$this->showmessage('修改失败，请重试','','error');
    	}
    }
    public function changepwd(){
    	$this->checklogin();
    	$this->display();
    }
    public function post_changepwd(){
    	$this->checklogin();
    	$sessInfo = C('SAKURA_MEMBER');
    	if($sessInfo['password'] != md5(md5(I('post.password')))) $this->showmessage('密码输入错误','','error');
    	$data = array();
    	$data['password'] = md5(md5(I('post.newpwd1')));
    	$Model = M('User');
    	$response = $Model->where("`email`='".$sessInfo['email']."'")->save($data);
    	if($response>0){
    		session('sakura_member',null);
    		$this->showmessage('修改成功',U('Member/login'));
    	}else{
    		$this->showmessage('修改失败，请重试','','error');
    	}
    }
    public function recharge(){
    	$this->checklogin();
    	$this->display();
    }
    public function do_recharge(){
    	$this->checklogin();
    	$udata = C('SAKURA_MEMBER');
    	if(md5(md5(I('post.password'))) != $udata['password']) $this->showmessage('密码验证失败','','error');
    	if(I('post.money') < 1) $this->showmessage('必须填写大于0的整数');
    	header("Cache-Control: no-cache, must-revalidate");
    	header("Pragma: no-cache");
    	$cfg = C('SAKURA_CONFIG');
    	$resellerid=$cfg['apf_id'];
    	$userip=$_SERVER["REMOTE_ADDR"];
    	$key=$cfg['apf_key'];
    	$sign=md5($userip.$key);
    	$url=urlencode('http://'.$_SERVER['HTTP_HOST'].U('Member/recharge_return'));
    	$orderid=mktime().rand(100,999);
    	$rmb=I('post.money');
    	$data = array();
    	$data['id'] = $orderid;
    	$data['email'] = $udata['email'];
    	$data['title'] = '充值到账户'.$udata['email'];
    	$data['money'] = $rmb;
    	$data['timestamp'] = mktime();
    	$data['status'] = '0';
    	M('Orders')->add($data);
    	$payurl = 'http://i.hostker.com/rapi/pay?resellerid='.$resellerid.'&orderid='.$orderid.'&rmb='.$rmb.'&noticeurl='.$url.'&sign='.$sign;
    	$this->showmessage('正在跳转到支付接口...',$payurl);
    }
    public function recharge_return(){
    	$invoice=I('get.invoice');
    	$orderid=I('get.orderid');
    	$cfg = C('SAKURA_CONFIG');
    	$resellerid=$cfg['apf_id'];
    	$status=file_get_contents('http://i.hostker.com/rapi/checkOrder?invoice='.$invoice.'&orderid='.$orderid.'&resellerid='.$resellerid);
    	if($status=='1'){
    		$udata = C('SAKURA_MEMBER');
    		$rechargemoney = M('Orders')->where("`id`='".$orderid."'")->select();
    		$rechargemoney = intval($rechargemoney['0']['money']);
    		$oldmoney = M('User')->where("`email`='".$udata['email']."'")->select();
    		$oldmoney = intval($oldmoney['0']['money']);
    		$newmoney = $oldmoney + $rechargemoney;
    		M('User')->where("`email`='".$udata['email']."'")->save(array('money'=>$newmoney));
    		M('Orders')->where("`id`='".$orderid."'")->save(array('hkid'=>$invoice,'status'=>'1'));
    	}
    }
    public function paylogs(){
    	$Model = M('Orders');
    	$member = C('SAKURA_MEMBER');
    	$count = $Model->where("`email`='".$member['email']."'")->count();
		$Page = new Page($count,20);
		$Page->url = 'Member/paylogs/p';
		$show = urldecode($Page->show());
		$list = $Model->where("`email`='".$member['email']."'")->order('timestamp desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->assign('page_nav',$show);
    	$this->assign('list',$list);
    	$this->display();
    }
    
    
    public function GetName($email){
    	$data = M('User')->where("`email`='".$email."'")->select();
    	return $data['0']['name'];
    }
}