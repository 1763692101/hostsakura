<?php if(!defined('THINK_PATH')) exit('Access Denied');
class IndexAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    	A('Cron')->InitCron();
    }
    public function index(){
    	if(!C('SAKURA_MEMBER')) header("Location:".U('Member/login'));
    	else header("Location:".U('Member/index'));
    }
    public function test(){
    	$post = array();
    	$post['type'] = '1';
    	$post['context'] = '';
		$result = A('Sakura')->CurlPost('http://api.iharuka.tk/getags/index.php',$post);
		$result = explode(',',$result);
		$this->show('CONTEXT:<br>','utf-8');
		var_dump($result);
    }
    public function crondatest(){
    	$res = A('Mail')->SendEMail('newcxs@qq.com','test',mktime().'测试一下了额。');
    	echo $res;
    }
    public function ctest(){
    	$content = A('Mail')->BuildMail(mktime().'测试一下了额。');
    	$res = A('Mail')->SendEMail('13905416466@139.com','test'.mktime(),$content);
    	echo $res;
    }
    public function crontest(){
    	ignore_user_abort();
    	set_time_limit(0);
    	//$total=3600;
    	$interval=60*30;
    	$i=0;
    	do{
    		A('Mail')->SendEMail('newcxs@qq.com','test',mktime().'测试一下了额。');
    		sleep($interval);
    		$i++;
    	}while($i<2);
    	ob_flush();
    	flush();
    }
}