<?php if(!defined('THINK_PATH')) exit('Access Denied');
class PageAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    	A('Cron')->InitCron();
    }
    public function about(){
    	$data = M('Page')->where("`page`='about'")->select();
    	$this->assign('context',$data['0']['context']);
    	$this->display();
    }
}