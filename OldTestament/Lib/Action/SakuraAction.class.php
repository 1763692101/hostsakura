<?php if(!defined('THINK_PATH')) exit('Access Denied');
class SakuraAction extends Action {
    public function __construct(){
    	parent::__construct();
    	$this->InitSystem();
    }
    public function InitSystem(){
    	C('SAKURA_MEMBER',array());
    	C('SAKURA_CONFIG',array());
    	C('SAKURA_DES',null);
    	$this->InitConfig();
    	$this->InitUser();
    	$cfg = C('SAKURA_CONFIG');
    	$this->InitDESEngine($cfg['apf_key'],$cfg['apf_id']);
    	unset($cfg);
    }
    public function InitConfig(){
    	$Setting = M('Setting');
    	$configs = $Setting->select();
    	$lists = array();
    	foreach($configs as $val) $lists[$val['key']] = $val['value'];
    	C('SAKURA_CONFIG',$lists);
    }
    public function InitUser(){
    	$Model = M('User');
    	$sessUserInfo = @unserialize(session('sakura_member'));
    	if(!$sessUserInfo) return false;
    	$dbUserInfo = $Model->where("`uid`='".$sessUserInfo['uid']."'")->select();
    	$dbUserInfo = $dbUserInfo['0'];
    	if($dbUserInfo['password'] != $sessUserInfo['password']) C('SAKURA_MEMBER',false);
    	else C('SAKURA_MEMBER',$sessUserInfo);
    }
    public function InitDESEngine($key,$iv){
    	$des = new Des($key,$iv);
    	C('SAKURA_DES',$des);
    }
    
    public function showmessage($msg,$url='',$type='success'){
    	switch($type){
    		case 'success':
    			$this->success($msg,$url);
    			break;
    		case 'error':
    			$this->error($msg,$url);
    			break;
    		default:
    			$this->success($msg,$url);
    			break;
    	}
    	exit;
    }
    
    
    public function friendlyDate($sTime,$type = 'normal',$alt = 'false') {
    	$cTime        =    time();
    	$dTime        =    $cTime - $sTime;
    	$dDay        =    intval(date("z",$cTime)) - intval(date("z",$sTime));
    	$dYear        =    intval(date("Y",$cTime)) - intval(date("Y",$sTime));
    	if($type=='normal'){
    		if( $dTime < 60 ){
    			return $dTime."秒前";
    		}elseif( $dTime < 3600 ){
    			return intval($dTime/60)."分钟前";
    		}elseif( $dYear==0 && $dDay == 0  ){
    			return '今天'.date('H:i',$sTime);
    		}elseif($dYear==0){
    			return date("m月d日 H:i",$sTime);
    		}else{
    			return date("Y-m-d H:i",$sTime);
    		}
    	}elseif($type=='mohu'){
    		if( $dTime < 60 ){
    			return $dTime."秒前";
    		}elseif( $dTime < 3600 ){
    			return intval($dTime/60)."分钟前";
    		}elseif( $dTime >= 3600 && $dDay == 0  ){
    			return intval($dTime/3600)."小时前";
    		}elseif( $dDay > 0 && $dDay<=7 ){
    			return intval($dDay)."天前";
    		}elseif( $dDay > 7 &&  $dDay <= 30 ){
    			return intval($dDay/7) . '周前';
    		}elseif( $dDay > 30 ){
    			return intval($dDay/30) . '个月前';
    		}
    		//full: Y-m-d , H:i:s
    	}elseif($type=='full'){
    		return date("Y-m-d , H:i:s",$sTime);
    	}elseif($type=='ymd'){
    		return date("Y-m-d",$sTime);
    	}else{
    		if( $dTime < 60 ){
    			return $dTime."秒前";
    		}elseif( $dTime < 3600 ){
    			return intval($dTime/60)."分钟前";
    		}elseif( $dTime >= 3600 && $dDay == 0  ){
    			return intval($dTime/3600)."小时前";
    		}elseif($dYear==0){
    			return date("Y-m-d H:i:s",$sTime);
    		}else{
    			return date("Y-m-d H:i:s",$sTime);
    		}
    	}
    }
    
    
    public function CurlPost($url,$post=array()){
    	$ch=curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_HEADER, 0);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	$result=curl_exec($ch);
    	curl_close($ch);
    	return $result;
    }
}