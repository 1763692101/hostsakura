<?php if(!defined('THINK_PATH')) exit('Access Denied');
class ForumAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    	A('Cron')->InitCron();
    }
    public function index(){
    	A('Member')->checklogin();
    	$Model = M('Thread');
    	$count = $Model->where('rid=0')->count();
		$Page = new Page($count,20);
		$Page->url = 'Forum/index/p';
		$show = urldecode($Page->show());
		$list = $Model->where('rid=0')->order('timestamp desc')->limit($Page->firstRow.','.$Page->listRows)->select();
    	$this->assign('page_nav',$show);
    	$this->assign('thread_list',$list);
    	$this->display();
    }
    public function newpost(){
    	A('Member')->checklogin();
    	$uinfo = C('SAKURA_MEMBER');
    	$this->NewThread($this->_post('title'),$this->_post('context'),$uinfo['email']);
    }
    public function thread(){
    	A('Member')->checklogin();
    	$Model = M('Thread');
    	$tid = I('get.tid');
    	$data = $Model->where("`tid`='".$tid."'")->select();
    	if(!$data) $this->showmessage('没有此贴或此贴已经被删除','','error');
    	$thread_data = $data['0'];
    	
    	$count = $Model->where('rid='.$tid)->count();
		$Page = new Page($count,10);
		$Page->url = 'Forum/thread/tid/'.$tid.'/p';
		$show = urldecode($Page->show());
		$replys = $Model->where('rid='.$tid)->order('timestamp')->limit($Page->firstRow.','.$Page->listRows)->select();
		
    	$this->assign('thread_data',$thread_data);
    	$this->assign('replys',$replys);
    	$this->assign('page_nav',$show);
    	$this->assign('page',I('get.p'));
    	$this->display();
    }
    public function newreply(){
    	A('Member')->checklogin();
    	$rid = I('post.tid');
    	$title = I('post.title');
    	$context = I('post.context');
    	$author = C('SAKURA_MEMBER');
    	$author = $author['email'];
    	$this->ReplyThread($rid, $title, $context, $author);
    }
    
    
    public function GetReplyNum($tid){
    	if(!$tid) $this->showmessage('没有此贴','','error');
    	$Model = M('Thread');
    	$count = $Model->where("`rid`='".$tid."'")->count();
    	return $count;
    }
    public function GetLastReplyName($tid){
    	if(!$tid) $this->showmessage('没有此贴','','error');
    	$Model = M('Thread');
    	$data = $Model->where("`rid`='".$tid."'")->order("tid desc")->find();
    	if(!$data) return '暂无';
    	else return A('Member')->GetName($data['author']);
    }
    public function NewThread($title,$context,$author){
    	$Model = M('Thread');
    	$data = array();
    	$data['rid'] = '0';
    	$data['title'] = $title;
    	$data['context'] = $context;
    	$data['author'] = $author;
    	$data['timestamp'] = mktime();
    	$response = $Model->add($data);
    	if($response>0) $this->showmessage('发帖成功，下面跳转到帖子页面',U('Forum/thread',array('tid'=>$response)));
    	else $this->showmessage('发帖失败，请重试','','error');
    }
    public function ReplyThread($rid,$title,$context,$author){
    	$Model = M('Thread');
    	$data = array();
    	$data['rid'] = $rid;
    	$data['title'] = $title;
    	$data['context'] = $context;
    	$data['author'] = $author;
    	$data['timestamp'] = mktime();
    	if(!$Model->where("`tid`='".$rid."'")->select()) $this->showmessage('不存在此贴','','error');
    	$response = $Model->add($data);
    	if($response>0) $this->showmessage('回复成功，下面跳转到帖子页面',U('Forum/thread',array('tid'=>$rid)));
    	else $this->showmessage('回复失败，请重试','','error');
    }
}