<?php if(!defined('THINK_PATH')) exit('Access Denied');
class HostAction extends SakuraAction {
    public function __construct(){
    	parent::__construct();
    	A('Cron')->InitCron();
    }
    public function index(){
    	A('Member')->checklogin();
    	$member = C('SAKURA_MEMBER');
    	$list = M('Hostings')->where("`email`='".$member['email']."'")->select();
    	$this->assign('list',$list);
    	$this->display();
    }
    public function buy(){
    	A('Member')->checklogin();
    	$Model = M('Plan');
    	$list = $Model->select();
    	$this->assign('list',$list);
    	$this->display();
    }
    public function buyguide(){
    	A('Member')->checklogin();
    	$pid = I('get.pid');
    	$Model = M('Plan');
    	$plandata = $Model->where("`id`='".$pid."'")->select();
    	if(!$plandata) $this->showmessage('未定义操作','','error');
    	$plandata = $plandata['0'];
    	$this->assign('plandata',$plandata);
    	$this->display();
    }
    public function do_buy(){
    	A('Member')->checklogin();
    	$pid = I('post.pid');
    	$member = C('SAKURA_MEMBER');
    	$panelusername = I('post.panelusername');
    	$panelpassword = I('post.panelpassword');
    	$time = I('post.time');
    	$type = $pid;
    	$plandata = M('Plan')->where("`id`='".$pid."'")->select();
    	if(!$plandata) $this->showmessage('未定义操作','','error');
    	$plandata = $plandata['0'];
    	$planprice = ($time=='month') ? ($plandata['fee']/10) : $plandata['fee'];
    	if($member['money']<$planprice) $this->showmessage('余额不足','','error');
    	$HOSTKER = A('Hostker');
    	$result = $HOSTKER->checkHostname($panelusername);
    	if($result == '1'){
    		$result = $HOSTKER->createHost($panelusername,$panelpassword,$type,$time);
    		switch($result){
    			case '0':
    				$this->showmessage('开通失败，错误代码[0]','','error');
    				break;
    			case '1':
    				$this->showmessage('用户名不符合规定（只能字母或者数字，4-12位）','','error');
    				break;
    			case '2':
    				$this->showmessage('密码不科学','','error');
    				break;
    			case '3':
    				$this->showmessage('用户名存在','','error');
    				break;
    			case '4':
    				$this->showmessage('型号不存在','','error');
    				break;
    			case '5':
    				$this->showmessage('付款周期不正确','','error');
    				break;
    			case '6':
    				$this->showmessage('开通失败，错误代码[6]','','error');
    				break;
    			case '50':
    				$this->showmessage('开通失败，错误代码[50]','','error');
    				break;
    			case '51':
    				$this->showmessage('开通失败，错误代码[51]','','error');
    				break;
    			default:
    				$exptime = $result;
    				$data = array();
    				$data['id'] = mktime().rand(100,999);
    				$data['email'] = $member['email'];
    				$data['panelusername'] = $panelusername;
    				$data['type'] = $type;
    				$data['exptime'] = $exptime;
    				M('Hostings')->add($data);
    				$oldmoney = $member['money'];
    				$newmoney = $oldmoney - $planprice;
    				M('User')->where("`email`='".$member['email']."'")->save(array('money'=>$newmoney));
    				$data = array();
    				$data['email'] = $member['email'];
    				$data['title'] = '购买主机';
    				$data['money'] = intval('-'.$planprice);
    				$data['timestamp'] = mktime();
    				$data['status'] = '1';
    				$data['hkid'] = '0';
    				M('Orders')->add($data);
    				$this->showmessage('开通成功！到期时间：'.$exptime,U('Host/index'));
    				break;
    		}
    	}else{
    		switch($result){
    			case '0':
    				$this->showmessage('用户名被占用或者不符合规定','','error');
    				break;
    			case '50':
    				$this->showmessage('请联系管理员，错误代码[50]','','error');
    				break;
    			case '51':
    				$this->showmessage('请联系管理员，错误代码[51]','','error');
    				break;
    			default:
    				$this->showmessage('未知错误','','error');
    				break;
    		}
    	}
    }
    public function renew(){
    	A('Member')->checklogin();
    	$member = C('SAKURA_MEMBER');
    	$hid = I('get.hid');
    	$hostinfo = M('Hostings')->where("`id`='".$hid."'")->select();
    	if(!$hostinfo) $this->showmessage('未定义操作','','error');
    	$hostinfo = $hostinfo['0'];
    	if($hostinfo['email']!=$member['email']) $this->showmessage('未定义操作','','error');
    	$plandata = M('Plan')->where("`id`='".$hostinfo['type']."'")->select();
    	$plandata = $plandata['0'];
    	$this->assign('hostinfo',$hostinfo);
    	$this->assign('plandata',$plandata);
    	$this->display();
    }
    public function do_renew(){
    	A('Member')->checklogin();
    	$member = C('SAKURA_MEMBER');
    	$hid = I('post.hid');
    	$panelusername = I('post.panelusername');
    	$time = I('post.time');
    	$pid = $hid['type'];
    	$plandata = M('Plan')->where("`id`='".$pid."'")->select();
    	if(!$plandata) $this->showmessage('未定义操作','','error');
    	$plandata = $plandata['0'];
    	$planprice = ($time=='month') ? ($plandata['fee']/10) : $plandata['fee'];
    	if($member['money']<$planprice) $this->showmessage('余额不足','','error');
    	$HOSTKER = A('Hostker');
    	$result = $HOSTKER->renewHost($panelusername,$time);
    	switch($result){
    		case '0':
    			$this->showmessage('主机不存在','','error');
    			break;
    		case '1':
    			$this->showmessage('用户名不符合规定','','error');
    			break;
    		case '5':
    			$this->showmessage('付款周期不正确','','error');
    			break;
    		case '6':
    			$this->showmessage('续费失败，错误代码[6]','','error');
    			break;
    		case '50':
    			$this->showmessage('续费失败，错误代码[50]','','error');
    			break;
    		case '51':
    			$this->showmessage('续费失败，错误代码[51]','','error');
    			break;
    		default:
    			$exptime = $result;
    			M('Hostings')->where("`id`='".$hid."'")->save(array('exptime'=>$exptime));
    			$oldmoney = $member['money'];
    			$newmoney = $oldmoney - $planprice;
    			M('User')->where("`email`='".$member['email']."'")->save(array('money'=>$newmoney));
    			$data = array();
    			$data['id'] = mktime().rand(100,999);
    			$data['email'] = $member['email'];
    			$data['title'] = '续费主机';
    			$data['money'] = intval('-'.$planprice);
    			$data['timestamp'] = mktime();
    			$data['status'] = '1';
    			$data['hkid'] = '0';
    			M('Orders')->add($data);
    			$this->showmessage('续费成功，到期时间：'.$exptime,U('Host/index'));
    			break;
    	}
    }
    public function panel(){
    	A('Member')->checklogin();
    	$member = C('SAKURA_MEMBER');
    	$cfg = C('SAKURA_CONFIG');
    	$hid = I('get.hid');
    	$hostinfo = M('Hostings')->where("`id`='".$hid."'")->select();
    	if(!$hostinfo) $this->showmessage('未定义操作','','error');
    	$hostinfo = $hostinfo['0'];
    	if($hostinfo['email']!=$member['email']) $this->showmessage('未定义操作','','error');
    	$panelusername=$hostinfo['panelusername'];
    	$etime=time()+1800;
    	$resellerid=$cfg['apf_id'];
    	$key=$cfg['apf_key'];
    	$email=$member['email'];
    	$sign=md5($panelusername.$etime.$email.$key);
    	header("Cache-Control: no-cache, must-revalidate");
    	header("Pragma: no-cache");
    	header('Location: http://panel.idc.cx/cp/doLogin?resellerid='.$resellerid.'&etime='.$etime.'&panelusername='.$panelusername.'&email='.urlencode($email).'&sign='.$sign);
    }
}