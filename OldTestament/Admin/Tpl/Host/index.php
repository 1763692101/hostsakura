<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>主机管理</li><li>/</li>
            <li>主机列表</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>主机列表</h2>
            </div>
            <div class="contentbox">
            	<b>说明：</b><br/><br/>
	   			过期时间格式为YYYYMMDD；删除主机只会删除在本站的记录[因为HostKer没有提供删除接口]
	   			<div class="spacer"></div>
                <table width="100%">
                	<tr>
                		<th>ID</th>
                		<th>用户E-Mail</th>
                		<th>控制台用户名</th>
                		<th>主机类型</th>
                		<th>过期时间</th>
                		<th>管理</th>
                	</tr>
                	<volist name="list" id="val">
                		<tr>
                			<td>{$val.id}</td>
                			<td>{$val.email}</td>
                			<td>{$val.panelusername}</td>
                			<td>{$val.type}</td>
                			<td>{$val.exptime}</td>
                			<td>
                				<a href="__APP__/Host/detail/hid/{$val.id}">管理</a>&nbsp;|&nbsp;
                				<a href="__APP__/Host/hostdel/hid/{$val.id}">删除</a>
                			</td>
                		</tr>
    				</volist>
                </table><br>
                <div align="right">{$page_nav}</div>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />