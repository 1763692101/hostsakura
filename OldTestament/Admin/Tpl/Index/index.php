<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>首页</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>资料栏</h2>
            </div>
            <div class="contentbox">
                <div class="noticebox">
                    <div class="innernotice">
                        <h4>统计/数据</h4>
                        <p>用户总数：{$count.user}</p>
                        <p>主机总数：{$count.host}</p>
                        <p>上次登录IP：{$Think.config.SAKURA_ADMIN.lastip}</p>
                        <p>上次登录时间：{$Think.config.SAKURA_ADMIN.lasttime|date="Y-m-d H:i:s",###}</p>
                    </div>
                </div>
                <div class="noticeboxalt">
                    <div class="innernotice">
                        <h4>HostKer代理商客服</h4>
                        <p>主机/CDN技术客服QQ：5882121</p>
                        <p>业务/财务/APF技术QQ：8332121</p>
                        <p>请勿向下级客户提供HostKer的QQ号</p>
                    </div>
                </div>
                <div class="noticebox">
                    <div class="innernotice">
                        <h4>API信息</h4>
                        <p>代理商编号：{$Think.config.SAKURA_CONFIG.apf_id}</p>
                        <p>代理接口Key：{$Think.config.SAKURA_CONFIG.apf_key}</p>
                        <p>请妥善保管切勿泄露API信息</p>
                        <p>如不慎泄露请立即联系HostKer客服人员修改Key</p>
                    </div>
                </div>
                <div class="noticeboxalt">
                    <div class="innernotice">
                        <h4>开发者留言</h4>
                        <p>各位使用者大家好</p>
                        <p>我是HostSakura的开发者</p>
                        <p>群发邮件因为效率问题</p>
                        <p>已经被我关闭了</p>
                        <p>下个版本将解决这个问题</p>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="contentcontainer">
            <div class="headings">
                <h2>版权信息</h2>
            </div>
            <div class="contentbox">
                <table width="100%">
                	<tr>
                		<td><b>程序版本</b></td>
                		<td>HostSakura Ver.{$Think.config.SAKURA_VERSION} Rel.{$Think.config.SAKURA_RELEASE} Dev.{$Think.const.SAKURA_DEVREL}</td>
                	</tr>
                	<tr>
                		<td><b>作者</b></td>
                		<td>kiddel</td>
                	</tr>
                	<tr>
                		<td><b>作者博客</b></td>
                		<td><a href="http://blog.iharuka.tk" target="_blank">blog.iharuka.tk</a></td>
                	</tr>
                	<tr>
                		<td><b>版权信息</b></td>
                		<td>Code by kiddel. Framework by <a href="http://thinkphp.cn" target="_blank">TopThink</a>. Theme by <a href="http://www.hostker.com" target="_blank">HostKer</a></td>
                	</tr>
                	<tr>
                		<td><b>遵循协议</b></td>
                		<td><a href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.zh" target="_blank"><img src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"></a></td>
                	</tr>
                	<tr>
                		<td><b>还有一件事...</b></td>
                		<td>感谢所有使用本程序的用户！谢谢你们的支持！</td>
                	</tr>
                </table>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />