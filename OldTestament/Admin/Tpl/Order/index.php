<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>订单管理</li><li>/</li>
            <li>订单列表</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>订单列表</h2>
            </div>
            <div class="contentbox">
            	<b>说明：</b><br/><br/>
	   			状态：0[未支付],1[已支付]
	   			<div class="spacer"></div>
                <table width="100%">
                	<tr>
                		<th>ID</th>
                		<th>用户E-Mail</th>
                		<th>订单标题</th>
                		<th>订单金额</th>
                		<th>时间</th>
                		<th>状态</th>
                		<th>HostKer中ID</th>
                		<th>管理</th>
                	</tr>
                	<volist name="list" id="val">
                		<tr>
                			<td>{$val.id}</td>
                			<td>{$val.email}</td>
                			<td>{$val.title}</td>
                			<td>{$val.money}</td>
                			<td>{$val.timestamp|date="Y-m-d H:i:s",###}</td>
                			<td>{$val.status}</td>
                			<td>{$val.hkid}</td>
                			<td>
                				<a href="__APP__/Order/orderdel/oid/{$val.id}">删除</a>
                			</td>
                		</tr>
    				</volist>
                </table><br>
                <div align="right">{$page_nav}</div>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />