<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>用户管理</li><li>/</li>
            <li>用户列表</li><li>/</li>
            <li>UID:{$userinfo.uid} 的用户详情</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>UID:{$userinfo.uid} 的用户详情&nbsp;&nbsp;<a href="__APP__/User">[返回]</a></h2>
            </div>
            <div class="contentbox">
                <table width="100%">
                	<tr>
                		<td width="20%"><b>UID</b></td>
                		<td>{$userinfo.uid}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>E-Mail</b></td>
                		<td>{$userinfo.email}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>余额</b></td>
                		<td>{$userinfo.money}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>姓名</b></td>
                		<td>{$userinfo.name}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>电话</b></td>
                		<td>{$userinfo.phone}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>注册IP</b></td>
                		<td>{$userinfo.regip}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>注册时间</b></td>
                		<td>{$userinfo.regtime|date="Y-m-d H:i:s",###}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>上次登录IP</b></td>
                		<td>{$userinfo.lastip}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>上次登录时间</b></td>
                		<td>{$userinfo.lasttime|date="Y-m-d H:i:s",###}</td>
                	</tr>
                	<tr>
                		<td width="20%"><b>管理操作</b></td>
                		<td>
                			<a href="__APP__/User/userdel/uid/{$userinfo.uid}">删除</a>&nbsp;|&nbsp;
                			<a href="__APP__/User/sendmail/uid/{$userinfo.uid}">发邮件</a>
                		</td>
                	</tr>
                </table>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />