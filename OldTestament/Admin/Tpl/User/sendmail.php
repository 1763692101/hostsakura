<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>用户管理</li><li>/</li>
            <li>发送邮件</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>发送邮件</h2>
            </div>
            <div class="contentbox">
                <form method='post' action="__APP__/User/dosendmail">
             		<p>
                        <label for="domain"><strong>用户UID: </strong></label>
                        <input type="text" name="uid"  class="inputbox" value="{$userinfo.uid}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>邮件标题: </strong></label>
                        <input type="text" name="title"  class="inputbox" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>邮件内容: </strong></label>
                        <textarea name="content" class="inputbox"></textarea> <br />
                    </p>
					<input type="submit" value="提交" class="btn" />
				</form>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />