<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>用户管理</li><li>/</li>
            <li>用户列表</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>用户列表</h2>
            </div>
            <div class="contentbox">
            	<b>说明：</b><br/><br/>
	   			删除用户操作会直接删除，请谨慎操作
	   			<div class="spacer"></div>
                <table width="100%">
                	<tr>
                		<th>UID</th>
                		<th>E-Mail</th>
                		<th>余额</th>
                		<th>姓名</th>
                		<th>电话</th>
                		<th>管理</th>
                	</tr>
                	<volist name="userlist" id="val">
                		<tr>
                			<td>{$val.uid}</td>
                			<td>{$val.email}</td>
                			<td>{$val.money} 元</td>
                			<td>{$val.name}</td>
                			<td>{$val.phone}</td>
                			<td>
                				<a href="__APP__/User/detail/uid/{$val.uid}">详情</a>&nbsp;|&nbsp;
                				<a href="__APP__/User/sendmail/uid/{$val.uid}">发邮件</a>&nbsp;|&nbsp;
                				<a href="__APP__/User/userdel/uid/{$val.uid}">删除</a>
                			</td>
                		</tr>
    				</volist>
                </table><br>
                <div align="right">{$page_nav}</div>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />