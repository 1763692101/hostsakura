<div id="leftside">
    	<div class="user">
        	<img src="https://www.gravatar.com/avatar/{$Think.config.SAKURA_ADMIN.email|md5}.png?s=44" width="44" height="44" class="hoverimg" alt="Avatar" />
                        <p>欢迎光临</p>
            <p class="username">{$Think.config.SAKURA_ADMIN.name}</p>
            <p class="userbtn"><a href="__APP__/Index" title="">后台首页</a></p>
            <p class="userbtn"><a href="__APP__/Index/logout" title="">退出登录</a></p>
        </div>
        
        <ul id="nav">
        	<li>
                <ul class="navigation">
                    <li class="heading selected" onclick="javascript:location.href='__ROOT__/index.php'">返回网站首页</li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">系统管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Setting/basic">基本参数设置</a></li>
                    <li><a href="__APP__/Setting/hostker">HostKer参数设置</a></li>
                    <li><a href="__APP__/Setting/mailsend">邮件参数设置</a></li>
                    <li><a href="__APP__/Setting/systemupdate">系统升级</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">用户管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/User">用户列表</a></li>
                    <li><a href="__APP__/User/sendmail">发送邮件</a></li>
                    <!-- <li><a href="__APP__/User/sendmails">群发邮件</a></li> -->
                </ul>
            </li>
            <li>
                <a class="expanded heading">主机管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Host">主机列表</a></li>
                    <li><a href="__APP__/Host/buyhost">添加主机</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">订单管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Order">订单列表</a></li>
                </ul>
            </li>
                </ul>
            </li>
        </ul>
    </div>