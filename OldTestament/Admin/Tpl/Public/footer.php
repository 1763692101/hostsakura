        <div style="clear:both;"></div>

        <div id="footer">
        	&copy; Copyright {:date("Y")} <a href="{$Think.config.SAKURA_CONFIG.siteurl}" target="_blank">{$Think.config.SAKURA_CONFIG.sitename}</a>. All rights reserved.
        	&nbsp; Powered by HostSakura Ver.{$Think.config.SAKURA_VERSION} Rel.{$Think.config.SAKURA_RELEASE}
        </div>
    </div>
    
    <include file="Public:menu" />
</body>
</html>