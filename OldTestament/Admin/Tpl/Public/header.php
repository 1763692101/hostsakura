<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理中心 - Powered by HostSakura</title>
<link href="__ROOT__/Admin/Public/style/layout.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/wysiwyg.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/styles.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.min.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery-ui.min.js'></script>
<script type="text/javascript" src="__ROOT__/Admin/Public/js/enhance.js"></script>	
<script type='text/javascript' src='__ROOT__/Admin/Public/js/excanvas.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.wysiwyg.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/visualize.jQuery.js'></script>
<script type="text/javascript" src='__ROOT__/Admin/Public/js/functions.js'></script>
</head>
<body id="homepage">
	<div id="header">
    	<a href="__ROOT__/admin.php"><img src="__ROOT__/Public/image/logo.png" alt="" class="logo" /></a>
    	<div id="searcharea">
        </div>
    </div>
    