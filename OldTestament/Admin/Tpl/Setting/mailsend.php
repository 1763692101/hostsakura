<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>系统管理</li><li>/</li>
            <li>邮件参数设置</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>提示信息</h2>
            </div>
            <div class="contentbox">
                <p>请在下面的表单中修改您需要的配置。</p>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="contentcontainer">
            <div class="headings">
                <h2>邮件参数设置</h2>
            </div>
            <div class="contentbox">
                <form method='post' action="__APP__/Setting/domailsend">
             		<p>
                        <label for="domain"><strong>SMTP服务器: </strong></label>
                        <input type="text" name="smtp_server"  class="inputbox" value="{$setlist.smtp_server}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>SMTP端口: </strong></label>
                        <input type="text" name="smtp_port"  class="inputbox" value="{$setlist.smtp_port}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>SMTP邮箱: </strong></label>
                        <input type="text" name="smtp_email"  class="inputbox" value="{$setlist.smtp_email}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>SMTP用户名: </strong></label>
                        <input type="text" name="smtp_username"  class="inputbox" value="{$setlist.smtp_username}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>SMTP密码: </strong></label>
                        <input type="text" name="smtp_password"  class="inputbox" value="{$setlist.smtp_password}" /> <br />
                    </p>
					<input type="submit" value="提交" class="btn" />
				</form>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />