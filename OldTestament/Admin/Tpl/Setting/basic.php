<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>系统管理</li><li>/</li>
            <li>基本参数设置</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>提示信息</h2>
            </div>
            <div class="contentbox">
                <p>请在下面的表单中修改您需要的配置。</p>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="contentcontainer">
            <div class="headings">
                <h2>基本参数设置</h2>
            </div>
            <div class="contentbox">
                <form method='post' action="__APP__/Setting/dobasic">
             		<p>
                        <label for="domain"><strong>网站名称: </strong></label>
                        <input type="text" name="sitename"  class="inputbox" value="{$setlist.sitename}" /> <br />
                    </p>
             		<p>
                        <label for="domain"><strong>网站地址: </strong></label>
                        <input type="text" name="siteurl"  class="inputbox" value="{$setlist.siteurl}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>网站关键字: </strong></label>
                        <input type="text" name="keywords"  class="inputbox" value="{$setlist.keywords}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>网站描述: </strong></label>
                        <input type="text" name="description"  class="inputbox" value="{$setlist.description}" /> <br />
                    </p>
                    <p>
                        <label for="domain"><strong>新注册欢迎信息: </strong></label>
                        <textarea name="welcomemail" class="inputbox">{$setlist.welcomemail}</textarea> <br />
                        PS:网站名称可用{sitename}代替
                    </p>
					<input type="submit" value="提交" class="btn" />
				</form>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />