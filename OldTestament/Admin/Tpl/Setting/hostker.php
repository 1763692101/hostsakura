<include file="Public:header" />
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>系统管理</li><li>/</li>
            <li>HostKer参数设置</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>提示信息</h2>
            </div>
            <div class="contentbox">
                <p>请在下面的表单中修改您需要的配置。</p>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="contentcontainer">
            <div class="headings">
                <h2>HostKer参数设置</h2>
            </div>
            <div class="contentbox">
                <form method='post' action="__APP__/Setting/dohostker">
             		<p>
                        <label for="domain"><strong>代理商编号: </strong></label>
                        <input type="text" name="apfid"  class="inputbox" value="{$setlist.apf_id}" /> <br />
                    </p>
             		<p>
                        <label for="domain"><strong>代理接口Key: </strong></label>
                        <input type="text" name="apfkey"  class="inputbox" value="{$setlist.apf_key}" /> <br />
                    </p>
					<input type="submit" value="提交" class="btn" />
				</form>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
<include file="Public:footer" />