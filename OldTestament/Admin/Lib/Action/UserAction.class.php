<?php if(!defined('THINK_PATH')) exit('Access Denied');
class UserAction extends SaAction {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		A('Index')->chklogin();
		$Model = M('User');
		$count = $Model->count();
		$Page = new Page($count,20);
		$Page->url = 'User/index/p';
		$show = urldecode($Page->show());
		$list = $Model->order('uid asc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('page_nav',$show);
		$this->assign('userlist',$list);
		$this->display();
	}
	public function detail(){
		A('Index')->chklogin();
		$uid = I('get.uid');
		$dbInfo = M('User')->where(array('uid'=>$uid))->select();
		if(!$dbInfo) $this->showmessage('没有此用户','','error');
		$dbInfo = $dbInfo['0'];
		$this->assign('userinfo',$dbInfo);
		$this->display();
	}
	public function userdel(){
		A('Index')->chklogin();
		$uid = I('get.uid');
		if(!$uid) $this->showmessage('未定义操作','','error');
		$dbInfo = M('User')->where(array('uid'=>$uid))->select();
		if(!$dbInfo) $this->showmessage('没有此用户','','error');
		M('User')->where(array('uid'=>$uid))->delete();
		$this->showmessage('用户删除成功',U('User/index'));
	}
	public function sendmail(){
		A('Index')->chklogin();
		$uid = I('get.uid');
		if($uid){
			$dbInfo = M('User')->where(array('uid'=>$uid))->select();
			$dbInfo = $dbInfo['0'];
		}else{
			$dbInfo = false;
		}
		$this->assign('userinfo',$dbInfo);
		$this->display();
	}
	public function dosendmail(){
		A('Index')->chklogin();
		$uid = I('post.uid');
		$title = I('post.title');
		$content = I('post.content');
		$userinfo = M('User')->where(array('uid'=>$uid))->select();
		if(!$userinfo) $this->showmessage('没有此用户','','error');
		$content = $this->BuildMail($content);
		$this->SendAMail($userinfo['0']['email'],$title,$content);
		$this->SendEMail($userinfo['0']['email'],$title,$content);
		$this->showmessage('邮件发送成功');
	}
	public function sendmails(){
		A('Index')->chklogin();
		$this->display();
	}
	public function dosendmails(){
		set_time_limit(0);
		A('Index')->chklogin();
		$title = I('post.title');
		$content = I('post.content');
		$content = $this->BuildMail($content);
		$Model = M('User');
		$count = $Model->count();
		$cnt = $count / 20;
		$cnt = $cnt + 1;
		for($i=0;$i<$cnt;$i++){
			$firstRow = $i * 20;
			$list = $Model->order('uid asc')->limit($firstRow.',20')->select();
			foreach($list as $val){
				$this->SendAMail($val['email'],$title,$content);
				$this->SendEMail($val['email'],$title,$content);
			}
		}
		$this->showmessage('群发邮件成功',U('User/sendmails'));
	}
	
	
	
	public function SendAMail($email,$title,$context){
		$data = array();
		$data['email'] = $email;
		$data['title'] = $title;
		$data['context'] = $context;
		$data['timestamp'] = mktime();
		$data['status'] = '0';
		M('Mail')->add($data);
	}
	public function SendEMail($email,$title,$context){
		$config = C('SAKURA_CONFIG');
		$member = M('User')->where("`email`='".$email."'")->select();
		$member = $member['0'];
		vendor('PHPMailer.class#phpmailer');
		$mail = new PHPMailer();
		$mail->CharSet = 'UTF-8';
		$mail->IsSMTP();
		$mail->SMTPDebug = 0;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = '';
		$mail->Host = $config['smtp_server'];
		$mail->Port = $config['smtp_port'];
		$mail->Username = $config['smtp_username'];
		$mail->Password = $config['smtp_password'];
		$mail->SetFrom($config['smtp_email'], $config['sitename']);
		$replyEmail = $config['smtp_email'];
		$replyName = $config['sitename'];
		$mail->AddReplyTo($replyEmail, $replyName);
		$mail->Subject = $title;
		$mail->MsgHTML($context);
		$mail->AddAddress($email, $member['name']);
		return $mail->Send() ? true : $mail->ErrorInfo;
	}
	public function BuildMail($content){
		$cfg = C('SAKURA_CONFIG');
		$response  = "<h1>「".$cfg['sitename']."」</h1>";
		$response .= "<p>&nbsp;</p>";
		$response .= "<p>".$content."</p>";
		$response .= "<p>&nbsp;</p>";
		$response .= "<p>&copy; ".date("Y")." <a href='http://".$_SERVER['HTTP_HOST'].U('Index/index')."' target='_blank'>".$cfg['sitename']."</a></p>";
		$response .= "<br>";
		return $response;
	}
}