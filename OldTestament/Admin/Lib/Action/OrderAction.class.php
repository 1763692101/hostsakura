<?php if(!defined('THINK_PATH')) exit('Access Denied');
class OrderAction extends SaAction {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		A('Index')->chklogin();
		$Model = M('Orders');
		$count = $Model->count();
		$Page = new Page($count,20);
		$Page->url = 'Order/index/p';
		$show = urldecode($Page->show());
		$list = $Model->order('id asc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('page_nav',$show);
		$this->assign('list',$list);
		$this->display();
	}
	public function orderdel(){
		A('Index')->chklogin();
		$oid = I('get.oid');
		if(!$oid) $this->showmessage('未定义操作','','error');
		$dbData = M('Orders')->where(array('id'=>$oid))->select();
		if(!$dbData) $this->showmessage('没有此订单','','error');
		M('Orders')->where(array('id'=>$oid))->delete();
		$this->showmessage('订单删除成功',U('Order/index'));
	}
}