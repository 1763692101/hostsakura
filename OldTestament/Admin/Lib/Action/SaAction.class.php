<?php if(!defined('THINK_PATH')) exit('Access Denied');
class SaAction extends Action {
	public function __construct(){
		parent::__construct();
		self::SystemInit();
	}
	public function SystemInit(){
		self::UserInit();
		self::SettingInit();
	}
	public function UserInit(){
		$email = @session('sakura_admin_email');
		$pwd = @session('sakura_admin_pwd');
		$data = M('Admin')->where(array('email'=>$email))->select();
		if(!$data){
			C('SAKURA_ADMIN',array('hasLogin'=>false));
			return null;
		}
		$data = $data['0'];
		if($data['password'] != $pwd){
			C('SAKURA_ADMIN',array('hasLogin'=>false));
		}else{
			C('SAKURA_ADMIN',array('hasLogin'=>true,'email'=>$email,'name'=>$data['name'],'lastip'=>session('sakura_admin_lastip'),'lasttime'=>session('sakura_admin_lasttime')));
		}
	}
	public function SettingInit(){
		$settings = M('Setting')->select();
		$sets = array();
		foreach($settings as $val) $sets[$val['key']] = $val['value'];
		C('SAKURA_CONFIG',$sets);
	}
	public function showmessage($msg,$url='',$type='success'){
		switch($type){
			case 'success':
				$this->success($msg,$url);
				break;
			case 'error':
				$this->error($msg,$url);
				break;
			default:
				$this->success($msg,$url);
				break;
		}
		exit;
	}
}