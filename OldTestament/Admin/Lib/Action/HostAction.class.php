<?php if(!defined('THINK_PATH')) exit('Access Denied');
class HostAction extends SaAction {
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		A('Index')->chklogin();
		$Model = M('Hostings');
		$count = $Model->count();
		$Page = new Page($count,20);
		$Page->url = 'Host/index/p';
		$show = urldecode($Page->show());
		$list = $Model->order('id asc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('page_nav',$show);
		$this->assign('list',$list);
		$this->display();
	}
	public function detail(){
		A('Index')->chklogin();
		$cfg = C('SAKURA_CONFIG');
		$hid = I('get.hid');
		if(!$hid) $this->showmessage('未定义操作','','error');
		$host = M('Hostings')->where(array('id'=>$hid))->select();
		if(!$host) $this->showmessage('没有此主机','','error');
		$host = $host['0'];
		$panelusername=$host['panelusername'];
		$etime=time()+1800;
		$resellerid=$cfg['apf_id'];
		$key=$cfg['apf_key'];
		$email=$host['email'];
		$sign=md5($panelusername.$etime.$email.$key);
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header('Location: http://panel.idc.cx/cp/doLogin?resellerid='.$resellerid.'&etime='.$etime.'&panelusername='.$panelusername.'&email='.urlencode($email).'&sign='.$sign);
	}
	public function hostdel(){
		A('Index')->chklogin();
		$hid = I('get.hid');
		if(!$hid) $this->showmessage('未定义操作','','error');
		$host = M('Hostings')->where(array('id'=>$hid))->select();
		if(!$host) $this->showmessage('没有此主机','','error');
		M('Hostings')->where(array('id'=>$hid))->delete();
		$this->showmessage('主机删除成功',U('Host/index'));
	}
	public function buyhost(){
		A('Index')->chklogin();
		$this->display();
	}
	public function dobuyhost(){
		A('Index')->chklogin();
		$data = array();
		$data['email'] = I('post.email');
		$data['panelusername'] = I('post.panelusername');
		$data['type'] = I('post.type');
		$data['exptime'] = I('post.exptime');
		$dbData = M('Hostings')->where(array('panelusername'=>$data['panelusername']))->select();
		if($dbData) $this->showmessage('已有此主机','','error');
		M('Hostings')->add($data);
		$this->showmessage('添加主机成功',U('Host/index'));
	}
}