<?php if(!defined('THINK_PATH')) exit('Access Denied');
class IndexAction extends SaAction {
	public function __construct(){
		parent::__construct();
	}
	public function chklogin(){
		$cfg = C('SAKURA_ADMIN');
		if($cfg['hasLogin']==false) $this->showmessage('您还没有登录',U('Index/login'));
	}
	public function index(){
		$this->chklogin();
		$count = array();
		$count['user'] = M('User')->count();
		$count['host'] = M('Hostings')->count();
		$this->assign('count',$count);
		$this->display();
	}
	public function login(){
		$cfg = C('SAKURA_ADMIN');
		if($cfg['hasLogin']==true) $this->showmessage('您已经登录',U('Index/index'));
		$this->display();
	}
	public function dologin(){
		$cfg = C('SAKURA_ADMIN');
		if($cfg['hasLogin']==true) $this->showmessage('您已经登录',U('Index/index'));
		$email = I('post.email');
		$pwd = md5(md5(I('post.password')));
		$dbInfo = M('Admin')->where(array('email'=>$email))->select();
		if(!$dbInfo) $this->showmessage('没有此管理员','','error');
		$dbInfo = $dbInfo['0'];
		if($pwd == $dbInfo['password']){
			session('sakura_admin_email',$email);
			session('sakura_admin_pwd',$pwd);
			session('sakura_admin_lastip',$dbInfo['lastip']);
			session('sakura_admin_lasttime',$dbInfo['lasttime']);
			M('Admin')->where(array('email'=>$email))->save(array('lastip'=>$_SERVER['REMOTE_ADDR'],'lasttime'=>mktime()));
			$this->showmessage('登录成功',U('Index/index'));
		}else{
			$this->showmessage('密码输入错误','','error');
		}
	}
	public function logout(){
		$this->chklogin();
		session('sakura_admin_email',null);
		session('sakura_admin_pwd',null);
		session('sakura_admin_lastip',null);
		session('sakura_admin_lasttime',null);
		$this->showmessage('退出成功',U('Index/login'));
	}
}