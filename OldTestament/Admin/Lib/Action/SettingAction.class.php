<?php if(!defined('THINK_PATH')) exit('Access Denied');
class SettingAction extends SaAction {
	public function __construct(){
		parent::__construct();
	}
	public function basic(){
		A('Index')->chklogin();
		$settings = M('Setting')->select();
		$setlist = array();
		foreach($settings as $val) $setlist[$val['key']] = $val['value'];
		$this->assign('setlist',$setlist);
		$this->display();
	}
	public function dobasic(){
		A('Index')->chklogin();
		$data = array();
		$data['sitename'] = I('post.sitename');
		$data['siteurl'] = I('post.siteurl');
		$data['keywords'] = I('post.keywords');
		$data['description'] = I('post.description');
		$data['welcomemail'] = I('post.welcomemail');
		foreach($data as $key=>$val) M('Setting')->where(array('key'=>$key))->save(array('value'=>$val));
		$this->showmessage('修改成功',U('Setting/basic'));
	}
	public function hostker(){
		A('Index')->chklogin();
		$settings = M('Setting')->select();
		$setlist = array();
		foreach($settings as $val) $setlist[$val['key']] = $val['value'];
		$this->assign('setlist',$setlist);
		$this->display();
	}
	public function dohostker(){
		A('Index')->chklogin();
		$data = array();
		$data['apf_id'] = I('post.apfid');
		$data['apf_key'] = I('post.apfkey');
		foreach($data as $key=>$val) M('Setting')->where(array('key'=>$key))->save(array('value'=>$val));
		$this->showmessage('修改成功',U('Setting/hostker'));
	}
	public function mailsend(){
		A('Index')->chklogin();
		$settings = M('Setting')->select();
		$setlist = array();
		foreach($settings as $val) $setlist[$val['key']] = $val['value'];
		$this->assign('setlist',$setlist);
		$this->display();
	}
	public function domailsend(){
		A('Index')->chklogin();
		$data = array();
		$data['smtp_server'] = I('post.smtp_server');
		$data['smtp_port'] = I('post.smtp_port');
		$data['smtp_email'] = I('post.smtp_email');
		$data['smtp_username'] = I('post.smtp_username');
		$data['smtp_password'] = I('post.smtp_password');
		foreach($data as $key=>$val) M('Setting')->where(array('key'=>$key))->save(array('value'=>$val));
		$this->showmessage('修改成功',U('Setting/mailsend'));
	}
	public function systemupdate(){
		A('Index')->chklogin();
		$this->display();
	}
}