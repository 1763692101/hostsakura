<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理中心 - Powered by HostSakura</title>
<link href="__ROOT__/Admin/Public/style/layout.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/wysiwyg.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/styles.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.min.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery-ui.min.js'></script>
<script type="text/javascript" src="__ROOT__/Admin/Public/js/enhance.js"></script>	
<script type='text/javascript' src='__ROOT__/Admin/Public/js/excanvas.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.wysiwyg.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/visualize.jQuery.js'></script>
<script type="text/javascript" src='__ROOT__/Admin/Public/js/functions.js'></script>
</head>
<body id="homepage">
	<div id="header">
    	<a href="__ROOT__/admin.php"><img src="__ROOT__/Public/image/logo.png" alt="" class="logo" /></a>
    	<div id="searcharea">
        </div>
    </div>
    
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>首页</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>资料栏</h2>
            </div>
            <div class="contentbox">
                <div class="noticebox">
                    <div class="innernotice">
                        <h4>统计/数据</h4>
                        <p>用户总数：<?php echo ($count["user"]); ?></p>
                        <p>主机总数：<?php echo ($count["host"]); ?></p>
                        <p>上次登录IP：<?php echo (C("SAKURA_ADMIN.lastip")); ?></p>
                        <p>上次登录时间：<?php echo (date("Y-m-d H:i:s",C("SAKURA_ADMIN.lasttime"))); ?></p>
                    </div>
                </div>
                <div class="noticeboxalt">
                    <div class="innernotice">
                        <h4>HostKer代理商客服</h4>
                        <p>主机/CDN技术客服QQ：5882121</p>
                        <p>业务/财务/APF技术QQ：8332121</p>
                        <p>请勿向下级客户提供HostKer的QQ号</p>
                    </div>
                </div>
                <div class="noticebox">
                    <div class="innernotice">
                        <h4>API信息</h4>
                        <p>代理商编号：<?php echo (C("SAKURA_CONFIG.apf_id")); ?></p>
                        <p>代理接口Key：<?php echo (C("SAKURA_CONFIG.apf_key")); ?></p>
                        <p>请妥善保管切勿泄露API信息</p>
                        <p>如不慎泄露请立即联系HostKer客服人员修改Key</p>
                    </div>
                </div>
                <div class="noticeboxalt">
                    <div class="innernotice">
                        <h4>开发者留言</h4>
                        <p>各位使用者大家好</p>
                        <p>我是HostSakura的开发者</p>
                        <p>群发邮件因为效率问题</p>
                        <p>已经被我关闭了</p>
                        <p>下个版本将解决这个问题</p>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="contentcontainer">
            <div class="headings">
                <h2>版权信息</h2>
            </div>
            <div class="contentbox">
                <table width="100%">
                	<tr>
                		<td><b>程序版本</b></td>
                		<td>HostSakura Ver.<?php echo (C("SAKURA_VERSION")); ?> Rel.<?php echo (C("SAKURA_RELEASE")); ?> Dev.<?php echo (SAKURA_DEVREL); ?></td>
                	</tr>
                	<tr>
                		<td><b>作者</b></td>
                		<td>kiddel</td>
                	</tr>
                	<tr>
                		<td><b>作者博客</b></td>
                		<td><a href="http://blog.iharuka.tk" target="_blank">blog.iharuka.tk</a></td>
                	</tr>
                	<tr>
                		<td><b>版权信息</b></td>
                		<td>Code by kiddel. Framework by <a href="http://thinkphp.cn" target="_blank">TopThink</a>. Theme by <a href="http://www.hostker.com" target="_blank">HostKer</a></td>
                	</tr>
                	<tr>
                		<td><b>遵循协议</b></td>
                		<td><a href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.zh" target="_blank"><img src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"></a></td>
                	</tr>
                	<tr>
                		<td><b>还有一件事...</b></td>
                		<td>感谢所有使用本程序的用户！谢谢你们的支持！</td>
                	</tr>
                </table>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
        <div style="clear:both;"></div>

        <div id="footer">
        	&copy; Copyright <?php echo date("Y");?> <a href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>" target="_blank"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>. All rights reserved.
        	&nbsp; Powered by HostSakura Ver.<?php echo (C("SAKURA_VERSION")); ?> Rel.<?php echo (C("SAKURA_RELEASE")); ?>
        </div>
    </div>
    
    <div id="leftside">
    	<div class="user">
        	<img src="https://www.gravatar.com/avatar/<?php echo (md5(C("SAKURA_ADMIN.email"))); ?>.png?s=44" width="44" height="44" class="hoverimg" alt="Avatar" />
                        <p>欢迎光临</p>
            <p class="username"><?php echo (C("SAKURA_ADMIN.name")); ?></p>
            <p class="userbtn"><a href="__APP__/Index" title="">后台首页</a></p>
            <p class="userbtn"><a href="__APP__/Index/logout" title="">退出登录</a></p>
        </div>
        
        <ul id="nav">
        	<li>
                <ul class="navigation">
                    <li class="heading selected" onclick="javascript:location.href='__ROOT__/index.php'">返回网站首页</li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">系统管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Setting/basic">基本参数设置</a></li>
                    <li><a href="__APP__/Setting/hostker">HostKer参数设置</a></li>
                    <li><a href="__APP__/Setting/mailsend">邮件参数设置</a></li>
                    <li><a href="__APP__/Setting/systemupdate">系统升级</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">用户管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/User">用户列表</a></li>
                    <li><a href="__APP__/User/sendmail">发送邮件</a></li>
                    <!-- <li><a href="__APP__/User/sendmails">群发邮件</a></li> -->
                </ul>
            </li>
            <li>
                <a class="expanded heading">主机管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Host">主机列表</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">订单管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Order">订单列表</a></li>
                </ul>
            </li>
                </ul>
            </li>
        </ul>
    </div>
</body>
</html>