<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>用户登录 - Powered by HostSakura</title>
<link href="__ROOT__/Admin/Public/style/styles.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/layout.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/login.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='http://lib.sinaapp.com/js/jquery/1.4.2/jquery.min.js'></script>
</head>
<body>
<script>
$(function(){
	$('#username').focus();
});
</script>
<div id="logincontainer">
	<div id="loginbox">
    	<div id="loginheader">
        	<img src="__ROOT__/Admin/Public/image/cp_logo_login.png" alt="Control Panel Login" />
        </div>
        <div id="innerlogin">
            <form method="post" action="__APP__/Index/dologin">
            	<p>E-Mail:</p>
            	<input type="text" name="email" id="username" class="logininput" value="" />
                <p>密码:</p>
            	<input type="password" name="password" class="logininput" />
               	<input type="submit" class="loginbtn" value="登录" /><br />
            </form>
        </div>
    </div>
    <img src="__ROOT__/Admin/Public/image/login_fade.png" alt="Fade" />
</div>
</body>
</html>