<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理中心 - Powered by HostSakura</title>
<link href="__ROOT__/Admin/Public/style/layout.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/wysiwyg.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/styles.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.min.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery-ui.min.js'></script>
<script type="text/javascript" src="__ROOT__/Admin/Public/js/enhance.js"></script>	
<script type='text/javascript' src='__ROOT__/Admin/Public/js/excanvas.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.wysiwyg.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/visualize.jQuery.js'></script>
<script type="text/javascript" src='__ROOT__/Admin/Public/js/functions.js'></script>
</head>
<body id="homepage">
	<div id="header">
    	<a href="__ROOT__/admin.php"><img src="__ROOT__/Public/image/logo.png" alt="" class="logo" /></a>
    	<div id="searcharea">
        </div>
    </div>
    
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>订单管理</li><li>/</li>
            <li>订单列表</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>订单列表</h2>
            </div>
            <div class="contentbox">
            	<b>说明：</b><br/><br/>
	   			状态：0[未支付],1[已支付]
	   			<div class="spacer"></div>
                <table width="100%">
                	<tr>
                		<th>ID</th>
                		<th>用户E-Mail</th>
                		<th>订单标题</th>
                		<th>订单金额</th>
                		<th>时间</th>
                		<th>状态</th>
                		<th>HostKer中ID</th>
                		<th>管理</th>
                	</tr>
                	<?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><tr>
                			<td><?php echo ($val["id"]); ?></td>
                			<td><?php echo ($val["email"]); ?></td>
                			<td><?php echo ($val["title"]); ?></td>
                			<td><?php echo ($val["money"]); ?></td>
                			<td><?php echo (date("Y-m-d H:i:s",$val["timestamp"])); ?></td>
                			<td><?php echo ($val["status"]); ?></td>
                			<td><?php echo ($val["hkid"]); ?></td>
                			<td>
                				<a href="__APP__/Host/detail/hid/<?php echo ($val["id"]); ?>">管理</a>&nbsp;|&nbsp;
                				<a href="__APP__/Host/hostdel/hid/<?php echo ($val["id"]); ?>">删除</a>
                			</td>
                		</tr><?php endforeach; endif; else: echo "" ;endif; ?>
                </table><br>
                <div align="right"><?php echo ($page_nav); ?></div>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
        <div style="clear:both;"></div>

        <div id="footer">
        	&copy; Copyright <?php echo date("Y");?> <a href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>" target="_blank"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>. All rights reserved.
        	&nbsp; Powered by HostSakura Ver.<?php echo (C("SAKURA_VERSION")); ?> Rel.<?php echo (C("SAKURA_RELEASE")); ?>
        </div>
    </div>
    
    <div id="leftside">
    	<div class="user">
        	<img src="https://www.gravatar.com/avatar/<?php echo (md5(C("SAKURA_ADMIN.email"))); ?>.png?s=44" width="44" height="44" class="hoverimg" alt="Avatar" />
                        <p>欢迎光临</p>
            <p class="username"><?php echo (C("SAKURA_ADMIN.name")); ?></p>
            <p class="userbtn"><a href="__APP__/Index" title="">后台首页</a></p>
            <p class="userbtn"><a href="__APP__/Index/logout" title="">退出登录</a></p>
        </div>
        
        <ul id="nav">
        	<li>
                <ul class="navigation">
                    <li class="heading selected" onclick="javascript:location.href='__ROOT__/index.php'">返回网站首页</li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">系统管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Setting/basic">基本参数设置</a></li>
                    <li><a href="__APP__/Setting/hostker">HostKer参数设置</a></li>
                    <li><a href="__APP__/Setting/mailsend">邮件参数设置</a></li>
                    <li><a href="__APP__/Setting/systemupdate">系统升级</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">用户管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/User">用户列表</a></li>
                    <li><a href="__APP__/User/sendmail">发送邮件</a></li>
                    <!-- <li><a href="__APP__/User/sendmails">群发邮件</a></li> -->
                </ul>
            </li>
            <li>
                <a class="expanded heading">主机管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Host">主机列表</a></li>
                    <li><a href="__APP__/Host/buyhost">添加主机</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">订单管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Order">订单列表</a></li>
                </ul>
            </li>
                </ul>
            </li>
        </ul>
    </div>
</body>
</html>