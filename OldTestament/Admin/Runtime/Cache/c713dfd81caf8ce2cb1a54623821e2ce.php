<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>管理中心 - Powered by HostSakura</title>
<link href="__ROOT__/Admin/Public/style/layout.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/wysiwyg.css" rel="stylesheet" type="text/css" />
<link href="__ROOT__/Admin/Public/style/styles.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.min.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery-ui.min.js'></script>
<script type="text/javascript" src="__ROOT__/Admin/Public/js/enhance.js"></script>	
<script type='text/javascript' src='__ROOT__/Admin/Public/js/excanvas.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/jquery.wysiwyg.js'></script>
<script type='text/javascript' src='__ROOT__/Admin/Public/js/visualize.jQuery.js'></script>
<script type="text/javascript" src='__ROOT__/Admin/Public/js/functions.js'></script>
</head>
<body id="homepage">
	<div id="header">
    	<a href="__ROOT__/admin.php"><img src="__ROOT__/Public/image/logo.png" alt="" class="logo" /></a>
    	<div id="searcharea">
        </div>
    </div>
    
    <div id="breadcrumb">
    	<ul>	
        	<li><img src="__ROOT__/Admin/Public/image/icon_breadcrumb.png" alt="Location" /></li>
        	<li><strong>您现在的位置:</strong></li>
            <li>管理中心</li><li>/</li>
            <li>系统管理</li><li>/</li>
            <li>HostKer参数设置</li>
        </ul>
    </div>

    <div id="rightside">
        <div class="contentcontainer">
            <div class="headings">
                <h2>提示信息</h2>
            </div>
            <div class="contentbox">
                <p>请在下面的表单中修改您需要的配置。</p>
                <div style="clear: both;"></div>
            </div>
        </div>
        <div class="contentcontainer">
            <div class="headings">
                <h2>HostKer参数设置</h2>
            </div>
            <div class="contentbox">
                <form method='post' action="__APP__/Setting/dohostker">
             		<p>
                        <label for="domain"><strong>代理商编号: </strong></label>
                        <input type="text" name="apfid"  class="inputbox" value="<?php echo ($setlist["apf_id"]); ?>" /> <br />
                    </p>
             		<p>
                        <label for="domain"><strong>代理接口Key: </strong></label>
                        <input type="text" name="apfkey"  class="inputbox" value="<?php echo ($setlist["apf_key"]); ?>" /> <br />
                    </p>
					<input type="submit" value="提交" class="btn" />
				</form>
                <div style="clear: both;"></div>
            </div>
        </div>
        
        
        <div style="clear:both;"></div>

        <div id="footer">
        	&copy; Copyright <?php echo date("Y");?> <a href="<?php echo (C("SAKURA_CONFIG.siteurl")); ?>" target="_blank"><?php echo (C("SAKURA_CONFIG.sitename")); ?></a>. All rights reserved.
        	&nbsp; Powered by HostSakura Ver.<?php echo (C("SAKURA_VERSION")); ?> Rel.<?php echo (C("SAKURA_RELEASE")); ?>
        </div>
    </div>
    
    <div id="leftside">
    	<div class="user">
        	<img src="https://www.gravatar.com/avatar/<?php echo (md5(C("SAKURA_ADMIN.email"))); ?>.png?s=44" width="44" height="44" class="hoverimg" alt="Avatar" />
                        <p>欢迎光临</p>
            <p class="username"><?php echo (C("SAKURA_ADMIN.name")); ?></p>
            <p class="userbtn"><a href="__APP__/Index" title="">后台首页</a></p>
            <p class="userbtn"><a href="__APP__/Index/logout" title="">退出登录</a></p>
        </div>
        
        <ul id="nav">
        	<li>
                <ul class="navigation">
                    <li class="heading selected" onclick="javascript:location.href='__ROOT__/index.php'">返回网站首页</li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">系统管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Setting/basic">基本参数设置</a></li>
                    <li><a href="__APP__/Setting/hostker">HostKer参数设置</a></li>
                    <li><a href="__APP__/Setting/mailsend">邮件参数设置</a></li>
                    <li><a href="__APP__/Setting/systemupdate">系统升级</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">用户管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/User">用户列表</a></li>
                    <li><a href="__APP__/User/sendmail">发送邮件</a></li>
                    <!-- <li><a href="__APP__/User/sendmails">群发邮件</a></li> -->
                </ul>
            </li>
            <li>
                <a class="expanded heading">主机管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Host">主机列表</a></li>
                </ul>
            </li>
            <li>
                <a class="expanded heading">订单管理</a>
                 <ul class="navigation">
                    <li><a href="__APP__/Order">订单列表</a></li>
                </ul>
            </li>
                </ul>
            </li>
        </ul>
    </div>
</body>
</html>