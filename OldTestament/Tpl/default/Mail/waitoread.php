<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Mail/waitoread">未读邮件</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>未读邮件</h3></div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>标题</th>
      <th>时间</th>
    </tr>
  </thead>
  <tbody>
  <volist name="list" id="lst">
    <tr>
      <td>{$lst.id}</td>
      <td><a href="__APP__/Mail/read/mid/{$lst.id}">{$lst.title}</a></td>
      <td>{$lst.timestamp|date="Y-m-d H:i:s",###}</td>
    </tr>
  </volist>
  </tbody>
</table>
{$page_nav}

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />