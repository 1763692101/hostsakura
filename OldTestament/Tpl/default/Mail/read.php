<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Mail">邮件列表</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Mail/read/mid/{$data.id}">邮件列表</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3><a href="__APP__/Mail">[返回]</a>&nbsp;&nbsp;{$data.title}</h3></div>
  <div>{$data.context}</div>
  <div align="right" style="">{$data.timestamp|date="Y-m-d H:i:s",###}</div>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />