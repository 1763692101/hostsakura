<include file="Public:header" />

<link href="__ROOT__{$Think.config.PUBLIC_URL}/style/forum.css" rel="stylesheet">
<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Forum">讨论区</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Forum/thread/tid/{$thread_data.tid}">{$thread_data.title}</a>
	</li>
</ul>

<div class="row-fluid">
	<div class="span12">
		<div class="hero-unit">

<h3>帖子内容</h3>
<div class="forum_thread">
  <div class="thread">
    <div class="left">
      <img src="http://1.gravatar.com/avatar/{$thread_data.author|md5}?s=128" height="128" width="128" />
    </div>
    <div class="right">
      <div class="info">
        <div class="l">主题：{$thread_data.title}&nbsp;|&nbsp;作者：{:A('Member')->GetName($thread_data['author'])}</div>
        <div class="r">#1</div>
      </div>
      <div class="context">{$thread_data.context|nl2br}</div>
      <div class="tool">
      	<div class="le">发表时间：{$thread_data.timestamp|date="Y-m-d H:i",###}</div>
      	<div class="re"></div>
      </div>
    </div>
  </div>
</div>
<h3>回复列表</h3>
<div class="forum_thread">
<present name="replys">
<php>$num=2+($page*10);</php>
<volist name="replys" id="reply">
  <div class="thread">
    <div class="left">
      <img src="http://1.gravatar.com/avatar/{$reply.author|md5}?s=128" height="128" width="128" />
    </div>
    <div class="right">
      <div class="info">
        <div class="l">主题：{$reply.title}&nbsp;|&nbsp;作者：{:A('Member')->GetName($reply['author'])}</div>
        <div class="r">#{$num}</div>
      </div>
      <div class="context">{$reply.context}</div>
      <div class="tool">
      	<div class="le">发表时间：{$reply.timestamp|date="Y-m-d H:i",###}</div>
      	<div class="re"></div>
      </div>
    </div>
  </div>
<php>$num++;</php>
</volist>
<else />
还没有人回复，抢沙发↓
</present>
</div>
<div align="right">{$page_nav}</div>

<hr>
<h3>回复此贴</h3>
<form action="__APP__/Forum/newreply" method="post" id="form">
<table class="table table-bordered">
    <tr>
      <th>标题</th>
      <td><input type="text" name="title" style="width:400px" value="Re:{$thread_data.title}"></td>
    </tr>
    <tr>
      <th>内容</th>
      <td><textarea name="context" style="width:400px;height:100px;"></textarea></td>
    </tr>
    <tr>
      <th>发表</th>
      <td>
      	<input type="hidden" name="tid" value="{$thread_data.tid}">
      	<a class="btn" href="javascript:void(0);" onclick="form.submit()">发表</a>
      </td>
    </tr>
</table>
</form>

		</div>
	</div>
</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />