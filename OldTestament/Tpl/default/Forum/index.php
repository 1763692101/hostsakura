<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Forum">讨论区</a>
	</li>
</ul>

<div class="row-fluid">
	<div class="span12">
		<div class="hero-unit">

<h3>帖子列表</h3>
<table class="table table-bordered">
  <thead>
    <tr>
      <th width="45%">标题</th>
      <th width="15%">作者</th>
      <th width="15%">时间</th>
      <th width="10%">回复</th>
      <th width="15%">最后回复</th>
    </tr>
  </thead>
  <tbody>
  <volist name="thread_list" id="thread">
    <tr>
      <td><a href="__APP__/Forum/thread/tid/{$thread.tid}">{$thread.title}</a></td>
	  <td>
	  	<img src="http://1.gravatar.com/avatar/{$thread.author|md5}?s=16" width="16" height="16">
	  	{:A('Member')->GetName($thread['author'])}
	  </td>
	  <td style="color:gray;font-size:12px;">{:SakuraAction::friendlyDate($thread['timestamp'])}</td>
	  <td>{:A('Forum')->GetReplyNum($thread['tid'])}</td>
	  <td>{:A('Forum')->GetLastReplyName($thread['tid'])}</td>
    </tr>
  </volist>
  </tbody>
</table>
<div align="right">{$page_nav}</div>
<hr>
<h3>发新贴</h3>
<form action="__APP__/Forum/newpost" method="post" id="form">
<table class="table table-bordered">
    <tr>
      <th>标题</th>
      <td><input type="text" name="title" style="width:400px"></td>
    </tr>
    <tr>
      <th>内容</th>
      <td><textarea name="context" style="width:400px;height:100px;"></textarea></td>
    </tr>
    <tr>
      <th>发表</th>
      <td><a class="btn" href="javascript:void(0);" onclick="form.submit()">发表</a></td>
    </tr>
</table>
</form>

		</div>
	</div>
</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />