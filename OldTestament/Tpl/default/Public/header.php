<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{$Think.config.SAKURA_CONFIG.sitename} - Powered by HostSakura</title>
	<meta name="keywords" content="{$Think.config.SAKURA_CONFIG.keywords}">
    <meta name="description" content="{$Think.config.SAKURA_CONFIG.description}">
    <meta name="generator" content="HostSakura Ver.{$Think.config.SAKURA_VERSION} Rel.{$Think.config.SAKURA_RELEASE} Dev.{$Think.const.SAKURA_DEVREL}" />
	<meta name="author" content="Kiddel[kiddel.mail@gmail.com]">
    <link href="__ROOT__{$Think.config.PUBLIC_URL}/style/main.css" rel="stylesheet">
	<link href="__ROOT__{$Think.config.PUBLIC_URL}/style/bootstrap.css" rel="stylesheet">
	<link href="__ROOT__{$Think.config.PUBLIC_URL}/style/bootstrap-responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div id="is">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
   <div class="container">
	  <a class="brand" href="{$Think.config.SAKURA_CONFIG.siteurl}">{$Think.config.SAKURA_CONFIG.sitename}</a>
		<ul class="nav">
		  <li><a href="__ROOT__/index.php">主页</a></li>
		  <li><a href="__APP__/Member/index">用户中心</a></li>
		  <li><a href="__APP__/Page/about">关于我们</a></li>
		  <li><a href="__APP__/Forum">讨论区</a></li>
		</ul>
        <ul class="nav pull-right">
        	<php>if(!C('SAKURA_MEMBER')){</php>
 				<li><a href="__APP__/Member/login">登录</a></li>
          		<li><a href="__APP__/Member/reg">注册</a></li>
          	<php>}else{</php>
 				<li><a href="javascript:void(0);">欢迎您，{$Think.config.SAKURA_MEMBER.name}</a></li>
          		<li><a href="__APP__/Member/info">资料</a></li>
          		<li><a href="__APP__/Member/logout">退出</a></li>
 			<php>}</php>
        </ul>
	 </div>
	</div>
</div>
<php>if(C('SAKURA_MEMBER')){</php>
<php>if(A('Mail')->CheckWaitMail() == true){</php>
<div class="alert alert-info"><b>您有新邮件，点击[<a href="__APP__/Mail">这里</a>]查看</b></div>
<php>}</php>
<php>}</php>
