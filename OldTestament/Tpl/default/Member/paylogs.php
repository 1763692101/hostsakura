<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member/paylogs">交易记录</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>交易记录</h3></div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>交易单号</th>
      <th>交易金额</th>
      <th>交易时间</th>
      <th>交易状态</th>
    </tr>
  </thead>
  <tbody>
  <volist name="list" id="order">
    <tr>
      <td>{$order.id}</td>
      <td>{$order['money']<0?'<font color="red">支出</font>':'<font color="green">收入</font>'} <php>echo abs($order['money']);</php> 元</td>
      <td>{$order.timestamp|date="Y-m-d H:i:s",###}</td>
      <td>{$order['status']=='1'?'<font color="green">已支付</font>':'<font color="red">等待支付</font>'}</td>
    </tr>
  </volist>
  <tr><td colspan="4">&nbsp;</td></tr>
  <tr>
  <php>
  $all = array();
  foreach($list as $val) $all[] = $val['money'];
  $all = array_sum($all);
  </php>
    <td>合计</td>
    <td>{$all} 元</td>
    <td>-</td>
    <td>-</td>
  </tr>
  </tbody>
</table>
<div align="right">{$page_nav}</div>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />