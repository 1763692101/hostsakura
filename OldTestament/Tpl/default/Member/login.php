<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member/login">登录</a>
	</li>
</ul>

<div class="row-fluid">
	<div class="span12">
		<div class="hero-unit" style="text-align:center;">
			<h2>用户登录</h2>
			<form action="__APP__/Member/do_login" method="post" id="form">
			<table border="0" align="center">
				<tr>
					<th style="width:100px;">E-Mail</th>
					<td style="width:250px;"><input type="text" name="email"></td>
				</tr>
				<tr>
					<th>密码</th>
					<td><input type="password" name="password"></td>
				</tr>
				<tr>
					<td colspan="2">
						<a class="btn" href="javascript:void(0);" onclick="form.submit();">登录</a>&nbsp;
						<a class="btn" href="__APP__/Member/reg">没有账号？注册</a>
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />