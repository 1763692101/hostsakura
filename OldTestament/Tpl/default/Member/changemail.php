<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member/changemail">修改E-Mail</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>修改E-Mail</h3></div>
<form action="__APP__/Member/post_changemail" method="post" id="form">
<table class="table table-bordered">
    <tr>
      <th>原E-Mail</th>
      <td>{$Think.config.SAKURA_MEMBER.email}</td>
    </tr>
    <tr>
      <th>新E-Mail</th>
	  <td><input type="text" name="neweml"></td>
    </tr>
    <tr>
      <th>确认密码</th>
	  <td><input type="password" name="pwd"></td>
    </tr>
    <tr>
      <th>提交</th>
      <td><a class="btn" href="javascript:void(0);" onclick="form.submit()">修改E-Mail</a></td>
    </tr>
</table>
</form>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />