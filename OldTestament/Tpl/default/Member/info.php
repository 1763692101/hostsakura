<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member/info">基础资料修改</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>基础资料修改</h3></div>
<form action="__APP__/Member/post_info" method="post" id="form">
<table class="table table-bordered">
    <tr>
      <th>用户UID</th>
      <td>{$Think.config.SAKURA_MEMBER.uid}</td>
    </tr>
    <tr>
      <th>用户E-Mail</th>
	  <td>{$Think.config.SAKURA_MEMBER.email}&nbsp;[<a href="__APP__/Member/changemail">修改</a>]</td>
    </tr>
    <tr>
      <th>真实姓名</th>
      <td><input type="text" name="name" value="{$Think.config.SAKURA_MEMBER.name}"></td>
    </tr>
    <tr>
      <th>电话/手机</th>
	  <td><input type="text" name="phone" value="{$Think.config.SAKURA_MEMBER.phone}"></td>
    </tr>
    <tr>
      <th>验证登陆密码</th>
	  <td><input type="password" name="password"></td>
    </tr>
    <tr>
      <th>提交</th>
      <td><a class="btn" href="javascript:void(0);" onclick="form.submit()">修改</a></td>
    </tr>
</table>
</form>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />