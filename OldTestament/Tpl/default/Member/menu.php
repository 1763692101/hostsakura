<ul class="nav nav-tabs nav-stacked">
<li class="nav-header">用户中心</li>
<li><a href="__APP__/Member">用户中心首页</a></li>
<li><a href="__APP__/Member/logout">退出登录</a></li>

<li class="nav-header">资料管理</li>
<li><a href="__APP__/Member/info">基础资料修改</a></li>
<li><a href="__APP__/Member/changepwd">用户密码修改</a></li>
<li><a href="__APP__/Member/changemail">用户E-Mail修改</a></li>

<li class="nav-header">财务管理</li>
<li><a href="__APP__/Member/paylogs">交易记录</a></li>
<li><a href="__APP__/Member/recharge">在线充值</a></li>

<li class="nav-header">产品管理</li>
<li><a href="__APP__/Host">我的主机列表</a></li>
<li><a href="__APP__/Host/buy">购买主机</a></li>

<li class="nav-header">站内邮件管理</li>
<li><a href="__APP__/Mail">邮件列表</a></li>
<li><a href="__APP__/Mail/waitoread">未读邮件</a></li>
</ul>