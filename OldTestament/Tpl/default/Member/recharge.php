<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member/recharge">在线充值</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>在线充值</h3></div>
<form action="__APP__/Member/do_recharge" method="post" id="form">
<table class="table table-bordered">
    <tr>
      <th>用户UID</th>
      <td>{$Think.config.SAKURA_MEMBER.uid}</td>
    </tr>
    <tr>
      <th>用户E-Mail</th>
	  <td>{$Think.config.SAKURA_MEMBER.email}</td>
    </tr>
    <tr>
      <th>余额</th>
      <td>{$Think.config.SAKURA_MEMBER.money} 元</td>
    </tr>
    <tr>
      <th>充值数额</th>
	  <td><input type="text" name="money"></td>
    </tr>
    <tr>
      <th>充值方式</th>
	  <td><input type="radio" name="type" value="netpay" checked>网银支付</td>
    </tr>
    <tr>
      <th>确认登录密码</th>
	  <td><input type="password" name="password"></td>
    </tr>
    <tr>
      <th>提交</th>
      <td><a class="btn" href="javascript:void(0);" onclick="form.submit()">充值</a></td>
    </tr>
</table>
</form>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />