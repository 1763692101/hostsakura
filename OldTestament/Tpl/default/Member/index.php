<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>用户中心</h3></div>
<h4>我的资料[<a href="__APP__/Member/info">修改</a>]</h4>
<table class="table table-bordered">
    <tr>
      <th>用户UID</th>
      <td>{$Think.config.SAKURA_MEMBER.uid}</td>
      <th>用户E-Mail</th>
	  <td>{$Think.config.SAKURA_MEMBER.email}</td>
    </tr>
    <tr>
      <th>真实姓名</th>
      <td>{$Think.config.SAKURA_MEMBER.name}</td>
      <th>电话/手机</th>
	  <td>{$Think.config.SAKURA_MEMBER.phone}</td>
    </tr>
    <tr>
      <th>注册IP</th>
      <td>{$Think.config.SAKURA_MEMBER.regip}</td>
      <th>注册时间</th>
	  <td>{$Think.config.SAKURA_MEMBER.regtime|date="Y-m-d H:i:s",###}</td>
    </tr>
    <tr>
      <th>上次登录IP</th>
      <td>{$Think.config.SAKURA_MEMBER.lastip}</td>
      <th>上次登录时间</th>
	  <td>{$Think.config.SAKURA_MEMBER.lasttime|date="Y-m-d H:i:s",###}</td>
    </tr>
</table>
<hr>
<h4>财务信息[<a href="__APP__/Member/recharge">充值</a>]</h4>
<table class="table table-bordered">
    <tr>
      <th>账号状态</th>
      <td>正常</td>
      <th>账号余额</th>
	  <td>{$Think.config.SAKURA_MEMBER.money} 元</td>
    </tr>
</table>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />