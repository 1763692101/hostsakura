<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Host/buy">购买主机</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Host/buyguide/pid/{$plandata.id}">{$plandata.name}</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>购买主机 - {$plandata.name}</h3></div>
<div class="alert alert-info">注意：所有空间开通后均<b>不予退款</b>！购买前请三思！</div>
<form action="__APP__/Host/do_buy" method="post" id="form">
<table class="table table-bordered">
  <tr>
    <th>资料确认<br><a href="__APP__/Host/buy">[返回修改]</a></th>
    <td>
    	<input type="hidden" name="pid" value="{$plandata.id}">
		<p>计划名称：<font color="green">{$plandata.name}</font></p>
		<p>文件硬盘：<font color="green">{$plandata.quota}</font> MB</p>
		<p>数据库硬盘：<font color="green">{$plandata.mysql}</font> MB</p>
		<p>香港/亚洲CDN流量：<font color="green">{$plandata.hkbw}</font> MB</p>
		<p>大陆/欧美CDN流量：<font color="green">{$plandata.cnbw}</font> MB</p>
		<p>CPU时间：<font color="green">{$plandata.cputime}</font> min</p>
		<p>数据库查询时间：<font color="green">{$plandata.querytime}</font> min</p>
		<p>价格：<font color="green">{$plandata.fee}</font> 元/年 | <font color="green"><php>echo $plandata['fee']/10;</php></font> 元/月</p>
    </td>
  </tr>
  <tr>
    <th>控制面板用户名</th>
    <td><input type="text" name="panelusername"></td>
  </tr>
  <tr>
    <th>控制面板密码</th>
    <td>
      <input type="text" name="panelpassword">
      <font color="gray">我们不会存储您的密码，所以请您注意保管</font>
    </td>
  </tr>
  <tr>
    <th>付款周期</th>
    <td>
      <input type="radio" name="time" value="month" checked>1个月[<php>echo $plandata['fee']/10;</php>元]&nbsp;&nbsp;
      <input type="radio" name="time" value="year">1年[{$plandata.fee}元]&nbsp;&nbsp;
      <!--<input type="radio" name="time" value="test">1天试用[免费]-->
    </td>
  </tr>
  <tr>
    <th>确认</th>
    <td><a class="btn" href="javascript:void(0);" onclick="form.submit();">开通</a></td>
  </tr>
</table>
</form>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />