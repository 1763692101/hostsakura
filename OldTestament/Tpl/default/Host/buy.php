<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Host/buy">购买主机</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>购买主机</h3></div>
<div class="alert alert-info">注：所有空间均支持月付，月付价格为<b>年付价格 / 10</b></div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>计划<br>名称</th>
      <th>文件硬盘<br>(MB)</th>
      <th>数据库硬盘<br>(MB)</th>
      <th>香港/亚洲CDN<br>流量(MB)</th>
      <th>大陆/欧美CDN<br>流量(MB)</th>
      <th>CPU时间<br>(min)</th>
      <th>数据库查询<br>时间(min)</th>
      <th>价格<br>(元/年)</th>
      <th>购买</th>
    </tr>
  </thead>
  <tbody>
  <volist name="list" id="val">
    <tr>
      <td>{$val.name}</td>
      <td>{$val.quota}</td>
      <td>{$val.mysql}</td>
      <td>{$val.hkbw}</td>
      <td>{$val.cnbw}</td>
      <td>{$val.cputime}</td>
      <td>{$val.querytime}</td>
      <td>{$val.fee}</td>
      <td><a class="btn btn-mini" href="__APP__/Host/buyguide/pid/{$val.id}">购买</a></td>
    </tr>
  </volist>
  </tbody>
</table>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />