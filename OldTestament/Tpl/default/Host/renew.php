<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<span class="breadcrumb-separator"><span class="divider">续费主机</span></span>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>续费主机 - ID:{$hostinfo.id}</h3></div>
<form action="__APP__/Host/do_renew" method="post" id="form">
<table class="table table-bordered">
  <tr>
    <th>资料确认</th>
    <td>
    	<input type="hidden" name="hid" value="{$hostinfo.id}">
    	<input type="hidden" name="panelusername" value="{$hostinfo.panelusername}">
		<p>主机ID：<font color="green">{$hostinfo.id}</font></p>
		<p>控制面板用户名：<font color="green">{$hostinfo.panelusername}</font></p>
		<p>到期时间：<font color="green">{$hostinfo.exptime}</font></p>
    </td>
  </tr>
  <tr>
    <th>付款周期</th>
    <td>
      <input type="radio" name="time" value="month" checked>1个月[<php>echo $plandata['fee']/10;</php>元]&nbsp;&nbsp;
      <input type="radio" name="time" value="year">1年[{$plandata.fee}元]
    </td>
  </tr>
  <tr>
    <th>确认</th>
    <td><a class="btn" href="javascript:void(0);" onclick="form.submit();">续费</a></td>
  </tr>
</table>
</form>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />