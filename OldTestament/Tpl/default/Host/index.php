<include file="Public:header" />

<ul class="breadcrumb">
	<li class="active">
		<a href="__ROOT__/index.php">主页</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Member">用户中心</a><span class="breadcrumb-separator"><span class="divider">/</span></span>
		<a href="__APP__/Host">我的主机列表</a>
	</li>
</ul>

<div class="row-fluid">
  <div class="span12">
  <div class="row-fluid">
  <div class="span3">
  <include file="Member:menu" />
  </div>
  <div class="span9 well">
  <div class="page-header"><h3>我的主机列表</h3></div>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>ID</th>
      <th>控制面板用户名</th>
      <th>主机类型</th>
      <th>到期时间</th>
      <th>操作</th>
    </tr>
  </thead>
  <tbody>
  <volist name="list" id="val">
    <tr>
      <td>{$val.id}</td>
      <td>{$val.panelusername}</td>
      <td>{$val.type}</td>
      <td>{$val.exptime}</td>
      <td>
        <a class="btn btn-mini" href="javascript:void(0);" onclick="window.open('__APP__/Host/panel/hid/{$val.id}');">控制面板</a>
        <a class="btn btn-mini" href="__APP__/Host/renew/hid/{$val.id}">续费</a>
      </td>
    </tr>
  </volist>
  </tbody>
</table>

  </div>
    </div>
  </div>


</div>
	
  </div> <!-- is end -->

<include file="Public:footer" />