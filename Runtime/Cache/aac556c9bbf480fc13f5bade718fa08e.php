<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo C('SAKURA_SETTING.sitename');?> - Powered by HostSakura</title>
    <link href="__ROOT__/Public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="keywords" content="<?php echo C('SAKURA_SETTING.keywords');?>" />
    <meta name="description" content="<?php echo C('SAKURA_SETTING.descripton');?>" />
    <meta name="generator" content="HostSakura <?php echo SAKURA_VERSION;?> Release <?php echo SAKURA_RELEASE;?>">
    <meta name="author" content="kiddel[kiddel@qq.com]">
    <meta name="copyright" content="<?php echo date('Y');?> <?php echo C('SAKURA_SETTING.sitename');?>.">
    <script src="__ROOT__/Public/jquery/jquery-1.10.1.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="navbar navbar-static-top"><!-- navbar-inverse -->
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a>
          <ul class="nav">
            <li id="nav_index"><a href="__ROOT__/index.php">首页</a></li>
            <li id="nav_member"><a href="__APP__/Member">会员中心</a></li>
            <li id="nav_product" class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">产品中心<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li id="productnav_cloudhost"><a href="__APP__/Product/cloudhost">云虚拟主机</a></li>
                <li id="productnav_hosting"><a href="__APP__/Product/hosting">普通虚拟主机</a></li>
              </ul>
            </li>
            <li id="nav_forum"><a href="__APP__/Forum">社区</a></li>
          </ul>
          <ul class="nav pull-right">
            <?php if(C('SAKURA_MEMBER')){ ?>
              <li id="nav_username"><a><img src="<?php echo A('Member')->getAvatar(C('SAKURA_MEMBER.email'));?>">&nbsp;<?php echo C('SAKURA_MEMBER.name');?></a></li>
              <?php if(C('SAKURA_MEMBER.admin')=='1'){ ?>
                <li id="nav_admin"><a href="__APP__/Admin"><i class="icon-cog"></i></a></li>
              <?php } ?>
              <li id="nav_logout"><a href="__APP__/Member/logout"><i class="icon-off"></i></a></li>
            <?php }else{ ?>
              <li id="nav_login"><a href="__APP__/Member/login">登录</a></li>
              <li id="nav_register"><a href="__APP__/Member/register">注册</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div><br>
    
    <script type="text/javascript">document.getElementById("nav_product").className="active dropdown";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Product">产品中心</a> <span class="divider">/</span></li>
        <li class="active">云虚拟主机</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <h4>产品中心 - 云虚拟主机</h4>
          <hr>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>&nbsp;</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><th><?php echo ($val["name"]); ?></th><?php endforeach; endif; ?>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>空间大小</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["quota"]); ?> MB</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>MySQL大小</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["mysql"]); ?> MB</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>香港/亚洲流量</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["hkbw"]); ?> GB</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>大陆/欧美流量</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["cnbw"]); ?> GB</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>CPU时间</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["cputime"]); ?> 小时</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>查询时间</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["querytime"]); ?> 小时</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>价格</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><?php echo ($val["fee"]); ?> 元/年<br><?php echo ($val['fee']/10);?> 元/月</td><?php endforeach; endif; ?>
              </tr>
              <tr>
                <th>购买</th>
                <?php if(is_array($data)): foreach($data as $key=>$val): ?><td><a href="__APP__/Hostker/buy/pid/<?php echo ($val["id"]); ?>" class="btn btn-mini btn-primary">购买</a></td><?php endforeach; endif; ?>
              </tr>
            </tbody>
          </table>
        </div>
	  </div>
	</div>
	
	<footer>
      <div class="form-actions" id="footer">
        <div class="container">
          <div class="pull-left text-left">
            <p>
              <small>Powered by <strong><a href="http://www.hostsakura.tk" target="_blank">HostSakura</a></strong> <?php echo SAKURA_VERSION;?></small><br>
              <small>&copy; <?php echo date("Y");?> <a href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a></small>
            </p>
          </div>
          <div class="pull-right text-right">
            <p>
              <small><?php echo C('SAKURA_SETTING.foot');?></small><br>
              <small>GMT+8, <?php echo date("Y-m-d H:i");?></small>
            </p>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="__ROOT__/Public/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>