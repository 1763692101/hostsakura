<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo C('SAKURA_SETTING.sitename');?> - Powered by HostSakura</title>
    <link href="__ROOT__/Public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="keywords" content="<?php echo C('SAKURA_SETTING.keywords');?>" />
    <meta name="description" content="<?php echo C('SAKURA_SETTING.descripton');?>" />
    <meta name="generator" content="HostSakura <?php echo SAKURA_VERSION;?> Release <?php echo SAKURA_RELEASE;?>">
    <meta name="author" content="kiddel[kiddel@qq.com]">
    <meta name="copyright" content="<?php echo date('Y');?> <?php echo C('SAKURA_SETTING.sitename');?>.">
    <script src="__ROOT__/Public/jquery/jquery-1.10.1.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="navbar navbar-static-top"><!-- navbar-inverse -->
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a>
          <ul class="nav">
            <li id="nav_index"><a href="__ROOT__/index.php">首页</a></li>
            <li id="nav_member"><a href="__APP__/Member">会员中心</a></li>
            <li id="nav_product" class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">产品中心<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li id="productnav_cloudhost"><a href="__APP__/Product/cloudhost">云虚拟主机</a></li>
                <li id="productnav_hosting"><a href="__APP__/Product/hosting">普通虚拟主机</a></li>
              </ul>
            </li>
            <li id="nav_forum"><a href="__APP__/Forum">社区</a></li>
          </ul>
          <ul class="nav pull-right">
            <?php if(C('SAKURA_MEMBER')){ ?>
              <li id="nav_username"><a><img src="<?php echo A('Member')->getAvatar(C('SAKURA_MEMBER.email'));?>">&nbsp;<?php echo C('SAKURA_MEMBER.name');?></a></li>
              <?php if(C('SAKURA_MEMBER.admin')=='1'){ ?>
                <li id="nav_admin"><a href="__APP__/Admin"><i class="icon-cog"></i></a></li>
              <?php } ?>
              <li id="nav_logout"><a href="__APP__/Member/logout"><i class="icon-off"></i></a></li>
            <?php }else{ ?>
              <li id="nav_login"><a href="__APP__/Member/login">登录</a></li>
              <li id="nav_register"><a href="__APP__/Member/register">注册</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div><br>
    
    <script type="text/javascript">document.getElementById("nav_forum").className="active";</script>
    <style>.pagenav a{margin:0 2px 0 2px;}</style>
    <style>
    .threadHeader{border:1px gray;border-style:none none dashed none;overflow:hidden;}
    .threadBody{margin:15px 20px 30px 20px;}
    .threadFooter{border:1px gray;border-style:dashed none none none;}
    </style>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum">社区</a> <span class="divider">/</span></li>
        <li><a href="__APP__/Forum/view/tid/<?php echo ($threadData['id']); ?>"><?php echo ($threadData['title']); ?></a> <span class="divider">/</span></li>
        <li class="active">第 <?php echo (I('get.p')?I('get.p'):'1');?> 页</li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <h4></h4>
          <div class="span12">
            <div class="threadata inline">
              <div class="pull-left span2 text-center">
                <img src="<?php echo A('Member')->getAvatar(A('Member')->getInfoByUID($threadData['uid'],'email'),'128');?>" width="128" height="128">
              </div>
              <div class="pull-right span9" style="margin-right:50px;">
                <div class="threadHeader inline">
                  <div class="pull-left"><strong><?php echo ($threadData['title']); ?></strong></div>
                  <div class="pull-right"><strong>楼主</strong></div>
                </div>
                <div class="threadBody"><?php echo (nl2br($threadData['context'])); ?></div>
                <div class="threadFooter inline">
                  <div class="pull-left"><small><?php echo (date("Y-m-d H:i",$threadData['timestamp'])); ?></small></div>
                  <div class="pull-right"><small>
                    <a href="__APP__/Forum/reply/tid/<?php echo ($threadData['id']); ?>">回复</a>
                    <?php if(C('SAKURA_MEMBER.admin') == '1' || C('SAKURA_MEMBER.uid') == $threadData['uid']){ ?>
                      <a href="__APP__/Forum/edit/tid/<?php echo ($threadData['id']); ?>">编辑</a>
                    <?php } ?>
                    <?php if(C('SAKURA_MEMBER.admin') == '1'){ ?>
                      <a href="__APP__/Forum/del/tid/<?php echo ($threadData['id']); ?>">删除</a>
                    <?php } ?>
                  </small></div>
                </div>
              </div>
            </div>
          </div>
          <h1>&nbsp;</h1>
          <?php $page = I('get.p') ? I('get.p') : '1'; ?>
          <?php $p = $page - 1; ?>
          <?php if($replyData){ ?>
            <?php $i = 1; ?>
            <?php if(is_array($replyData)): foreach($replyData as $key=>$reply): ?><div class="span12">
                <div class="threadata inline">
                  <div class="pull-left span2 text-center">
                    <img src="<?php echo A('Member')->getAvatar(A('Member')->getInfoByUID($reply['uid'],'email'),'128');?>" width="128" height="128">
                  </div>
                  <div class="pull-right span9" style="margin-right:50px;">
                    <div class="threadHeader inline">
                      <div class="pull-left"><strong><?php echo ($reply['title']); ?></strong></div>
                      <div class="pull-right"><strong><?php echo $i+$p*10;?>#</strong></div>
                    </div>
                    <div class="threadBody">
                      <?php echo (nl2br($reply['context'])); ?>
                      <?php $pReply = A('Forum')->getReply($reply['id']); ?>
                      <?php if($pReply){ ?>
                        <div class="well" style="padding:10px!important;margin-top:10px;margin-left:20px;">
                          <?php if(is_array($pReply)): foreach($pReply as $key=>$pr): ?><div class="inline" style="margin:3px 5px 5px 10px!important"><nobr>
                              <img src="<?php echo A('Member')->getAvatar(A('Member')->getInfoByUID($pr['uid'],'email'),'24');?>" width="24" height="24">
                              <small><strong><?php echo A('Member')->getInfoByUID($pr['uid'],'name');?>: </strong><?php echo ($pr['context']); ?></small>
                              <div class="text-right"><small><?php echo (date("Y-m-d H:i",$pr['timestamp'])); ?></small></div>
                            </nobr></div>
                            <hr style="margin:0!important"><?php endforeach; endif; ?>
                        </div>
                      <?php } ?>
                    </div>
                    <div class="threadFooter inline">
                      <div class="pull-left"><small><?php echo (date("Y-m-d H:i",$reply['timestamp'])); ?></small></div>
                      <div class="pull-right"><small>
                          <a href="__APP__/Forum/reply/tid/<?php echo ($reply['id']); ?>">回复</a>
                        <?php if(C('SAKURA_MEMBER.admin') == '1' || C('SAKURA_MEMBER.uid') == $threadData['uid']){ ?>
                          <a href="__APP__/Forum/edit/tid/<?php echo ($reply['id']); ?>">编辑</a>
                        <?php } ?>
                        <?php if(C('SAKURA_MEMBER.admin') == '1'){ ?>
                          <a href="__APP__/Forum/del/tid/<?php echo ($reply['id']); ?>">删除</a>
                        <?php } ?>
                      </small></div>
                    </div>
                  </div>
                </div>
              </div>
              <h1>&nbsp;</h1>
              <?php $i++; endforeach; endif; ?>
          <?php }else{ ?>
            <p>无回复</p>
          <?php } ?>
          <div class="text-right pagenav"><small><?php echo ($page_nav); ?></small></div>
          <h1>&nbsp;</h1>
          <hr>
          <h4>发表回复</h4>
          <?php if(C('SAKURA_MEMBER')){ ?>
            <form class="form-horizontal" action="__APP__/Forum/doreply" method="post">
              <input type="hidden" name="tid" value="<?php echo ($threadData['id']); ?>">
              <div class="control-group">
                <label class="control-label" for="inputTitle">标题</label>
                <div class="controls">
                  <input class="span8" type="text" id="inputTitle" name="title" value="Re:<?php echo ($threadData['title']); ?>">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="inputContext">内容</label>
                <div class="controls">
                  <textarea class="span8" rows="10" id="inputContext" name="context" placeholder="请输入回复内容"></textarea>
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <button type="submit" class="btn btn-primary btn-large">回复</button>
                </div>
              </div>
            </form>
          <?php }else{ ?>
            <p class="lead">您还没有登录，不能发表回复</p>
          <?php } ?>
        </div>
	  </div>
	</div>
	
	<footer>
      <div class="form-actions" id="footer">
        <div class="container">
          <div class="pull-left text-left">
            <p>
              <small>Powered by <strong><a href="http://www.hostsakura.tk" target="_blank">HostSakura</a></strong> <?php echo SAKURA_VERSION;?></small><br>
              <small>&copy; <?php echo date("Y");?> <a href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a></small>
            </p>
          </div>
          <div class="pull-right text-right">
            <p>
              <small><?php echo C('SAKURA_SETTING.foot');?></small><br>
              <small>GMT+8, <?php echo date("Y-m-d H:i");?></small>
            </p>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="__ROOT__/Public/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>