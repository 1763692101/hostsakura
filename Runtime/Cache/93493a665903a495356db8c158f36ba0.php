<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo C('SAKURA_SETTING.sitename');?> - Powered by HostSakura</title>
    <link href="__ROOT__/Public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="keywords" content="<?php echo C('SAKURA_SETTING.keywords');?>" />
    <meta name="description" content="<?php echo C('SAKURA_SETTING.descripton');?>" />
    <meta name="generator" content="HostSakura <?php echo A('Sakura')->getVersion();?>">
    <meta name="author" content="kiddel[kiddel@qq.com]">
    <meta name="copyright" content="<?php echo date('Y');?> <?php echo C('SAKURA_SETTING.sitename');?>.">
    <script src="http://lib.sinaapp.com/js/jquery/1.10.1/jquery-1.10.1.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="navbar navbar-static-top"><!-- navbar-inverse -->
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a>
          <ul class="nav">
            <li id="nav_index"><a href="__ROOT__/index.php">首页</a></li>
            <li id="nav_member"><a href="__APP__/Member">会员中心</a></li>
          </ul>
          <ul class="nav pull-right">
            <?php if(C('SAKURA_MEMBER')){ ?>
              <li id="nav_username"><a><img src="https://www.gravatar.com/avatar/<?php echo md5(C('SAKURA_MEMBER.email'));?>?s=18">&nbsp;<?php echo C('SAKURA_MEMBER.name');?></a></li>
              <?php if(C('MOE_MEMBER.admin')=='1'){ ?>
                <li id="nav_admin"><a href="__APP__/Admin"><i class="icon-cog icon-white"></i></a></li>
              <?php } ?>
              <li id="nav_logout"><a href="__APP__/Member/logout"><i class="icon-off icon-white"></i></a></li>
            <?php }else{ ?>
              <li id="nav_login"><a href="__APP__/Member/login">登录</a></li>
              <li id="nav_register"><a href="__APP__/Member/register">注册</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div><br>
    
    <script type="text/javascript">document.getElementById("nav_register").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">注册</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span12">
  	      <h4>用户注册</h4>
  	      <hr>
  	      <form class="form-horizontal" action="__APP__/Member/doregister" method="post">
            <div class="control-group">
              <label class="control-label" for="inputEmail">E-Mail</label>
              <div class="controls">
                <input type="text" id="inputEmail" name="email" placeholder="请输入E-Mail">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword">密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPassword2">确认密码</label>
              <div class="controls">
                <input type="password" id="inputPassword2" name="password2" placeholder="请再次输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputName">姓名</label>
              <div class="controls">
                <input type="text" id="inputName" name="name" placeholder="请输入姓名">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPhone">手机</label>
              <div class="controls">
                <input type="text" id="inputPhone" name="phone" placeholder="请输入手机">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputInvite">邀请码</label>
              <div class="controls">
                <?php if(C('SAKURA_SETTING.open')=='1'){ ?>
                  <input type="text" id="inputInvite" name="invite" placeholder="请输入邀请码，没有可以不填">
                <?php }elseif(C('SAKURA_SETTING.open')=='2'){ ?>
                  <input type="text" id="inputInvite" name="invite" placeholder="请输入邀请码">
                <?php } ?>
              </div>
            </div>
            <div class="control-group">
              <div class="controls">
                <button type="submit" class="btn">注册</button>
              </div>
            </div>
          </form>
        </div>
	  </div>
	</div>
	
	<footer>
      <div class="form-actions" id="footer">
        <div class="container">
          <div class="pull-left">
            &copy; Copyright <?php echo date("Y");?> <?php echo C('SAKURA_SETTING.sitename');?>
          </div>
          <div class="pull-right">
            Powered by HostSakura
          </div>
        </div>
      </div>
    </footer>
    
    <script src="__ROOT__/Public/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>