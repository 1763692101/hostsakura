<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo C('SAKURA_SETTING.sitename');?> - Powered by HostSakura</title>
    <link href="__ROOT__/Public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="keywords" content="<?php echo C('SAKURA_SETTING.keywords');?>" />
    <meta name="description" content="<?php echo C('SAKURA_SETTING.descripton');?>" />
    <meta name="generator" content="HostSakura <?php echo SAKURA_VERSION;?> Release <?php echo SAKURA_RELEASE;?>">
    <meta name="author" content="kiddel[kiddel@qq.com]">
    <meta name="copyright" content="<?php echo date('Y');?> <?php echo C('SAKURA_SETTING.sitename');?>.">
    <script src="__ROOT__/Public/jquery/jquery-1.10.1.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="navbar navbar-static-top"><!-- navbar-inverse -->
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a>
          <ul class="nav">
            <li id="nav_index"><a href="__ROOT__/index.php">首页</a></li>
            <li id="nav_member"><a href="__APP__/Member">会员中心</a></li>
            <li id="nav_product" class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">产品中心<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li id="productnav_cloudhost"><a href="__APP__/Product/cloudhost">云虚拟主机</a></li>
                <li id="productnav_hosting"><a href="__APP__/Product/hosting">普通虚拟主机</a></li>
              </ul>
            </li>
            <li id="nav_forum"><a href="__APP__/Forum">社区</a></li>
          </ul>
          <ul class="nav pull-right">
            <?php if(C('SAKURA_MEMBER')){ ?>
              <li id="nav_username"><a><img src="<?php echo A('Member')->getAvatar(C('SAKURA_MEMBER.email'));?>">&nbsp;<?php echo C('SAKURA_MEMBER.name');?></a></li>
              <?php if(C('SAKURA_MEMBER.admin')=='1'){ ?>
                <li id="nav_admin"><a href="__APP__/Admin"><i class="icon-cog"></i></a></li>
              <?php } ?>
              <li id="nav_logout"><a href="__APP__/Member/logout"><i class="icon-off"></i></a></li>
            <?php }else{ ?>
              <li id="nav_login"><a href="__APP__/Member/login">登录</a></li>
              <li id="nav_register"><a href="__APP__/Member/register">注册</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div><br>
    
    <script type="text/javascript">document.getElementById("nav_member").className="active";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Member">会员中心</a> <span class="divider">/</span></li>
        <li class="active">基本资料修改</li>
      </ul>
      <div class="row-fluid">
  	    <div class="span3">
  	      <ul class="nav nav-tabs nav-stacked">
            <li class="nav-header">系统</li>
            <li id="sidenav_index"><a href="__APP__/Member">会员中心首页</a></li>
            <li class="nav-header">用户</li>
            <li id="sidenav_basic_info"><a href="__APP__/Member/basic_info">基本资料修改</a></li>
            <li id="sidenav_basic_pwd"><a href="__APP__/Member/basic_pwd">登录密码修改</a></li>
            <li id="sidenav_basic_email"><a href="__APP__/Member/basic_email">用户邮箱修改</a></li>
            <li class="nav-header">主机</li>
            <li id="sidenav_host_mycloud"><a href="__APP__/Member/host_mycloud">我的云虚拟主机</a></li>
            <li id="sidenav_host_my"><a href="__APP__/Member/host_my">我的普通虚拟主机</a></li>
            <li><a href="__APP__/Product">购买新主机</a></li>
            <li class="nav-header">财务</li>
            <li id="sidenav_money_info"><a href="__APP__/Member/money_info">我的财务信息</a></li>
            <li id="sidenav_money_log"><a href="__APP__/Member/money_log">交易记录</a></li>
            <li id="sidenav_money_recharge"><a href="__APP__/Member/money_recharge">在线充值</a></li>
            <li class="nav-header">邮件</li>
            <li id="sidenav_mail_list"><a href="__APP__/Member/mail_list">邮件列表</a></li>
            <li id="sidenav_mail_send"><a href="__APP__/Member/mail_send">发送邮件</a></li>
          </ul>
  	      <script type="text/javascript">document.getElementById("sidenav_basic_info").className="active";</script>
        </div>
        <div class="span9">
          <h4>基本资料修改</h4>
          <hr>
          <form class="form-horizontal" action="__APP__/Member/post_basic_info" method="post">
            <div class="control-group">
              <label class="control-label" for="inputPassword">确认密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputName">姓名</label>
              <div class="controls">
                <input type="text" id="inputName" name="name" value="<?php echo C('SAKURA_MEMBER.name');?>" placeholder="请输入姓名">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPhone">手机</label>
              <div class="controls">
                <input type="text" id="inputPhone" name="phone" value="<?php echo C('SAKURA_MEMBER.phone');?>" placeholder="请输入手机">
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-primary">提交</button>
              <button type="button" class="btn" onclick="reset();">重置</button>
            </div>
          </form>
          <script>
          function reset(){
              document.getElementById('inputName').value="<?php echo C('SAKURA_MEMBER.name');?>";
              document.getElementById('inputPhone').value="<?php echo C('SAKURA_MEMBER.phone');?>";
          }
          </script>
        </div>
	  </div>
	</div>
	
	<footer>
      <div class="form-actions" id="footer">
        <div class="container">
          <div class="pull-left text-left">
            <p>
              <small>Powered by <strong><a href="http://www.hostsakura.tk" target="_blank">HostSakura</a></strong> <?php echo SAKURA_VERSION;?></small><br>
              <small>&copy; <?php echo date("Y");?> <a href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a></small>
            </p>
          </div>
          <div class="pull-right text-right">
            <p>
              <small><?php echo C('SAKURA_SETTING.foot');?></small><br>
              <small>GMT+8, <?php echo date("Y-m-d H:i");?></small>
            </p>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="__ROOT__/Public/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>