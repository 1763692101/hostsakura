<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo C('SAKURA_SETTING.sitename');?> - Powered by HostSakura</title>
    <link href="__ROOT__/Public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <meta name="keywords" content="<?php echo C('SAKURA_SETTING.keywords');?>" />
    <meta name="description" content="<?php echo C('SAKURA_SETTING.descripton');?>" />
    <meta name="generator" content="HostSakura <?php echo SAKURA_VERSION;?> Release <?php echo SAKURA_RELEASE;?>">
    <meta name="author" content="kiddel[kiddel@qq.com]">
    <meta name="copyright" content="<?php echo date('Y');?> <?php echo C('SAKURA_SETTING.sitename');?>.">
    <script src="__ROOT__/Public/jquery/jquery-1.10.1.min.js" type="text/javascript"></script>
  </head>
  <body>
    <div class="navbar navbar-static-top"><!-- navbar-inverse -->
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a>
          <ul class="nav">
            <li id="nav_index"><a href="__ROOT__/index.php">首页</a></li>
            <li id="nav_member"><a href="__APP__/Member">会员中心</a></li>
            <li id="nav_product" class="dropdown">
              <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">产品中心<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li id="productnav_cloudhost"><a href="__APP__/Product/cloudhost">云虚拟主机</a></li>
                <li id="productnav_hosting"><a href="__APP__/Product/hosting">普通虚拟主机</a></li>
              </ul>
            </li>
            <li id="nav_forum"><a href="__APP__/Forum">社区</a></li>
          </ul>
          <ul class="nav pull-right">
            <?php if(C('SAKURA_MEMBER')){ ?>
              <li id="nav_username"><a><img src="<?php echo A('Member')->getAvatar(C('SAKURA_MEMBER.email'));?>">&nbsp;<?php echo C('SAKURA_MEMBER.name');?></a></li>
              <?php if(C('SAKURA_MEMBER.admin')=='1'){ ?>
                <li id="nav_admin"><a href="__APP__/Admin"><i class="icon-cog"></i></a></li>
              <?php } ?>
              <li id="nav_logout"><a href="__APP__/Member/logout"><i class="icon-off"></i></a></li>
            <?php }else{ ?>
              <li id="nav_login"><a href="__APP__/Member/login">登录</a></li>
              <li id="nav_register"><a href="__APP__/Member/register">注册</a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div><br>
    
    <script type="text/javascript">document.getElementById("nav_product").className="active dropdown";</script>
    <div class="container">
      <ul class="breadcrumb">
        <li><a href="__ROOT__/index.php"><i class="icon-home"></i></a> <span class="divider">/</span></li>
        <li><a href="__APP__/Product">产品中心</a> <span class="divider">/</span></li>
        <li class="active">购买云虚拟主机 <?php echo ($plan["name"]); ?></li>
      </ul>
      <div class="row-fluid">
        <div class="span12">
          <h4>产品中心 - 云虚拟主机 - 购买 <?php echo ($plan["name"]); ?></h4>
          <hr>
          <form class="form-horizontal" action="__APP__/Hostker/dobuy" method="post">
            <div class="control-group">
              <label class="control-label" for="inputPassword">确认登录密码</label>
              <div class="controls">
                <input type="password" id="inputPassword" name="password" placeholder="请输入密码">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">购买型号</label>
              <div class="controls">
                <?php echo ($plan["name"]); ?>&nbsp;[<a href="__APP__/Product/cloudhost">更换</a>]
                <input type="hidden" name="pid" value="<?php echo ($plan["id"]); ?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">购买时间</label>
              <div class="controls">
                <label class="radio inline">
                  <input type="radio" name="time" id="time_radio_year" value="year" onclick="time_year();" checked>1 年
                </label>
                <label class="radio inline">
                  <input type="radio" name="time" id="time_radio_month" onclick="time_month();" value="month">1 月
                </label>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">所需金额</label>
              <div class="controls">
                <div id="money_need"><?php echo ($plan["fee"]); ?> 元</div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">您的余额</label>
              <div class="controls">
                <div id="money_need"><?php echo C('SAKURA_MEMBER.money');?> 元</div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPanelusername">控制台用户名</label>
              <div class="controls">
                <input type="text" id="inputPanelusername" name="panelusername" placeholder="请输入控制台用户名">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="inputPanelpassword">控制台密码</label>
              <div class="controls">
                <input type="text" id="inputPanelpassword" name="panelpassword" placeholder="请输入控制台密码">
              </div>
            </div>
            <div class="control-group">
              <div class="controls" id="post_button">
                <?php if(C('SAKURA_MEMBER.money') >= $plan['fee']){ ?>
                  <button type="submit" class="btn btn-primary">购买</button>
                <?php }else{ ?>
                  <button type="button" class="btn btn-danger">您的余额不足</button>
                <?php } ?>
              </div>
            </div>
          </form>
          <script>
          var year = <?php echo ($plan["fee"]); ?>;
          var month = <?php echo ($plan['fee']/10); ?>;
          var my = <?php echo C('SAKURA_MEMBER.money');?>;
		  function time_year(){
		      document.getElementById('money_need').innerHTML = '<?php echo ($plan["fee"]); ?> 元';
		      if(my >= year){
			      document.getElementById('post_button').innerHTML = '<button type="submit" class="btn btn-primary">购买</button>';
		      }else{
		    	  document.getElementById('post_button').innerHTML = '<button type="button" class="btn btn-danger">您的余额不足</button>';
		      }
		  }
		  function time_month(){
		      document.getElementById('money_need').innerHTML = '<?php echo ($plan["fee"]/10); ?> 元';
		      if(my >= month){
			      document.getElementById('post_button').innerHTML = '<button type="submit" class="btn btn-primary">购买</button>';
		      }else{
		    	  document.getElementById('post_button').innerHTML = '<button type="button" class="btn btn-danger">您的余额不足</button>';
		      }
		  }
          </script>
        </div>
	  </div>
	</div>
	
	<footer>
      <div class="form-actions" id="footer">
        <div class="container">
          <div class="pull-left text-left">
            <p>
              <small>Powered by <strong><a href="http://www.hostsakura.tk" target="_blank">HostSakura</a></strong> <?php echo SAKURA_VERSION;?></small><br>
              <small>&copy; <?php echo date("Y");?> <a href="<?php echo C('SAKURA_SETTING.siteurl');?>"><?php echo C('SAKURA_SETTING.sitename');?></a></small>
            </p>
          </div>
          <div class="pull-right text-right">
            <p>
              <small><?php echo C('SAKURA_SETTING.foot');?></small><br>
              <small>GMT+8, <?php echo date("Y-m-d H:i");?></small>
            </p>
          </div>
        </div>
      </div>
    </footer>
    
    <script src="__ROOT__/Public/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>